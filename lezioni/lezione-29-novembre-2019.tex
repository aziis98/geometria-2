\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 28 Novembre 2019}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

In generale $\P^n(\K) = \{ x_0 = 0 \} \cup \{ x_0 \neq 0 \} \simeq \P^{n-1}(\K) \cup U_0 \simeq \P^{n-1}(\K) \cup \K^n$. Notiamo anche che la restrizione $\pi : \CC^{n+1} \setminus \{ 0 \} \to \P(\CC)$ a $S^{2n+1} = \{ v \in \CC^{n+1} = \R^{2n+2}, \norm{v} = 1 \}$ è surgettiva perché $\pi(v) = \pi\lrpa{\sfrac{v}{\norm{v}}}$, ciò implica subito $\P^n(\CC)$ compatto.

Tuttavia $\forall \theta \in \R \; \forall v \in \R^{n+1} \setminus \{ 0 \} \; \pi(e^{i\theta} v) = \pi(v)$. Consideriamo la mappa $f : S^{2n+2} \to \P^1(\CC)$ ottenuta restringendo $\pi$, $\forall P \in \P^n(\CC), f^{-1}(P) \subseteq S^{2n+1}$ è omeomorfo a $S^1$ infatti se $v \in f^{-1}(P)$ scelto a caso $f^{-1}(P) = \{ \lambda v, \lambda \in \CC, |\lambda| = 1 \}$ per cui la mappa $i : S^1 = \{ \lambda \in \CC \mid |\lambda| = 1 \} \to f^{-1}(P)$ che manda $\lambda \mapsto \lambda v$ è bigettiva ed è perciò un omeomorfismo in quanto va da compatto a T2. 


Perciò, se $n = 1$ otteniamo che $\exists f : S^3 \to S^2$ surgettiva tale che $\forall P \in S^2 \; f^{-1}(P) \simeq S^1$. A questo punto potremmo chiederci se si può decomporre $S^3$ in un qualche prodotto topologico usando la $f$ che abbiamo costruito ma vedremo che $S^3 \not\simeq S^2 \times S^1$ (studiandone i $\pi_1$).

In particolare questa $f$ si chiama \textit{fibrazione di Lefschetz} ed è un esempio di fibrazione non banale ($\R^3$ come unione di $S^1$ ed una copia di $\R$ -- visualizzazione trovata in giro \url{https://www.youtube.com/watch?v=AKotMPGFJYk}).

\section{Spazi di Baire}

\begin{definition}
    Sia $X$ uno spazio topologico e $Y \subseteq X$ allora

    \begin{itemize}
        \item $Y$ è detto \textbf{raro} in $X$ se $(\overline Y)^\circ = \varnothing$
        \item $Y$ è detto \textbf{magro} se $Y$ è unione numerabile di \textit{rari} (detti di I categoria, i non magri invece sono detti di II categoria)
    \end{itemize}
\end{definition}

\begin{observation}
    Un chiuso $Z \subseteq X$ è raro $\iff Z^\circ = \varnothing \iff X \setminus Z$ è un aperto denso.
\end{observation}

\begin{definition}
    $X$ è detto uno \textbf{spazio di Baire} se per ogni magro $Y \subseteq X$ si ha $Y^\circ = \varnothing$.
\end{definition}

\begin{prop}
    I seguenti fatti sono equivalenti

    \begin{enumerate}
        \item $X$ è uno spazio di Baire
        \item Unione di numerabile chiusi rari ha sempre parte interna vuota
        \item Intersezione numerabile di aperti densi è densa
    \end{enumerate}


    \begin{proof} \ 
        \begin{itemize}
            \item[$\boxed{\text{ii)}\Rightarrow\text{iii)}}$]
            E' il passaggio al complementare. 
            
            \item[$\boxed{\text{i)}\Rightarrow\text{ii)}}$] 
            Segue dalla definizione.

            \item[$\boxed{\text{ii)}\Rightarrow\text{i)}}$] 
            Sia $Y \subseteq X$ magro, $Y = \bigcup_{n \in \N} Y_n$ con $Y_n$ raro, allora $Y^\circ = \lrpa{\bigcup_{n \in \N} Y_n}^\circ \subseteq \lrpa{\bigcup_{n \in \N} \overline{Y_n}}^\circ = \varnothing$. $\overline{Y_n}$ è chiuso raro.
        \end{itemize}
    \end{proof}
\end{prop}

\begin{theorem}
    $X$ spazio metrico completo $\implies X$ spazio di Baire.
\end{theorem}

\begin{observation}
    % Spesso si indica con Teorema di Baire anche un altro risultato che però non mostreremo.
    Il teorema è detto teorema di Baire, che contiene anche un altro enunciato che però non mostreremo ovvero

    $$
    X \text{ localmente compatto e T2} \implies X \text{ spazio di Baire}
    $$
\end{observation}

\newcommand{\ball}[1]{B\lrpa{#1}}

\textit{Dimostrazione Teorema di Baire.}
Sia $\{ F_n \}_{n \in \N}$ una famiglia di chiusi rari. Supponiamo per assurdo che $\lrpa{\bigcup_{n \in \N} F_n}^\circ \neq \varnothing$ e siano

$$
\text{0)} x_0 \in X, r_0 > 0 \text{ tale che } B(x_0, r_0) \subset \bigcup_{n \in \N} F_n
$$

$\lrpa{F_0}^\circ = \varnothing \implies \ball{x_0, \frac{r_0}{3}} \not\subset F_0$. $F_0$ è chiuso $\implies$

$$
\exists x_1 \in \ball{x_0, \frac{r_0}{3}} \setminus F_0 \; \exists r_0 < \frac{r_0}{3} \text{ tale che } \ball{x_1, r_1} \cap F_0 = \varnothing
$$

Ripeto il ragionamento con $x_1, r_1, F_1$ quindi

$$
\exists x_2 \in \ball{x_1, \frac{r_1}{3}} \setminus F_1 \; \exists r_2 < \frac{r_1}{3} \text{ tale che } \ball{x_2, r_2} \cap F_1 = \varnothing
$$

In questo modo costruisco una successione $\{ x_n \} \subseteq X$ e $\{ r_n \} \subseteq \R^+$ con le seguenti proprietà

\begin{itemize}
    \item $r_{n+1} < \frac{r_n}{3} \leq \frac{r_0}{3^{n+1}}$
    \item $x_{n+1} \in \ball{x_n, \frac{r_n}{3}}$ ed in particolare $d(x_n, x_{n+1}) < \frac{r_n}{3} \leq \frac{r_0}{3^{n+1}}$
    \item $\ball{x_{n+1}, r_{n+1}} \cap F_n = \varnothing$
\end{itemize}

I successivi passi della dimostrazione sono i seguenti

\begin{enumerate}[label={\arabic*)}]
    \item $\{ x_n \}$ di Cauchy, dunque $\exists x_\infty = \lim_n x_n$
    \item $\forall n \in \N \; x_\infty \notin F_n$
    \item $d(x_\infty, x_0) < r_0$
\end{enumerate}

Per ipotesi assurda avevamo $\ball{x_0, r_0} \subset \bigcup_{n \in \N} F_n \; \lightning$

\textit{Dimostriamo i tre passi.}

\begin{enumerate}[label={\arabic*)}]
    
    \item $\{ x_n \}$ è di Cauchy

        Siano $m > n$ naturali allora abbiamo [TODO: Ehm... Blob di stime]

        $$
        d(x_n, x_m) 
        \leq \sum_{i=n}^{m-1} d(x_i, x_{i+1}) 
        < \sum_{i=n}^{m-1} \frac{r_i}{3} 
        \leq \sum_{i=1}^{m} \frac{r_n}{3^i} 
        = \frac{r_n}{3} \sum_{i=0}^{m-1} \frac{1}{3^i} 
        < \frac{r_n}{3} \sum_{i=0}^\infty \frac{1}{3^i} 
        = \frac{r_n}{3} \cdot \frac{3}{2} 
        = \frac{r_n}{2}
        \leq \frac{r_0}{2 \cdot 3^n}
        $$

        dunque $\{ x_n \}$ è di Cauchy.
    
    \item $\forall n \in \N, x_\infty \notin F_n$

        Ponendo $m \to \infty$ in $\displaystyle d(x_{n+1}, x_m) < \frac{r_{n+1}}{2}$ troviamo $\displaystyle d(x_{n+1}, x_m) \leq \frac{r_{n+1}}{2} \implies x_\infty \in \ball{x_{n+1}, r_{n+1}}$ che non interseca $F_n \implies x_\infty \notin F_n$.
    
    \item $d(x_\infty, x_0) < r_0$

        Ponendo $m \to \infty$ in $\displaystyle d(x_0, x_m) < \frac{r_0}{2}$ otteniamo $\displaystyle d(x_0, x_\infty) \leq \frac{r_0}{2} < r_0 \Rightarrow x_\infty \in \ball{x_0, r_0} \subset \bigcup_{n \in \N} F_n$.

\end{enumerate}

\hfill $\square$

\begin{definition}
    $Y \subset X$ allora $Y$ è \textbf{perfetto} se è chiuso e privo di punti isolati (dove un punto isolato è un $y \in Y$ tale che $\{ y \} \subset Y$ è aperto)
\end{definition}

\begin{prop}
    $Y \subset \R$ è perfetto $\implies Y$ è più che numerabile.

    \begin{proof}
        
        Supponiamo per assurdo che sia numerabile, allora vogliamo trovare un punto isolato. Consideriamo quindi $Y = \{ y_n \}_{n \in \N}$, vogliamo trovare un $n$ tale che $y_n \in Y$ è isolato.

        $Y$ chiuso in $\R \implies Y$ metrico completo $\implies Y$ spaizo di Baire. $\displaystyle Y = \bigcup_{n \in \N} \{ y_n \}$ unione numerabile di chiusi. $Y^\circ = Y \neq \varnothing$. Per il teorema non può esistere $\forall n \in \N \; \lrpa{\overline{\{ y_n \}}}^\circ = \varnothing \implies \exists n$ tale che $y_n \in Y$ è isolato. 

    \end{proof}
\end{prop}

\begin{theorem}
    $f : [0, 1] \to \R$ continua e non derivabile in nessun punto.

    \begin{proof}
        
        Sia $X = \{ f : [0, 1] \to \R \mid f \text{ continua} \}$ con la distanza

        $$
        d_\infty(f, g) = \max_{x \in [0, 1]} |f(x) - g(x)|
        $$

        In generale abbiamo visto che  dato $Y$ spazio topologico e $X$ spazio metrico completo allora $C_\text{Lim}(Y, Z) = \{ f : Y \to Z \mid f \text{ continua e limitata} \}$ è uno spazio metrico completo con $d_\infty$. Inoltre $X$ è anche uno spazio di Baire.

        Ora consideriamo % l'insieme della funzioni a derivata limitata (in valore assoluto da $n$)

        $$
        C_n = \lrbr{ 
        f : [0, 1] \to \R \;\middle|\; f \text{ continua }
        \exists x_0 \in [0, 1] \text{ con } 
        \forall x \in [0, 1], x \neq x_0 \; \left| \frac{f(x) - f(x_0)}{x - x_0} \right| \leq n
        }
        $$

        Mostriamo le seguenti cose

        \begin{enumerate}[label={\arabic*)}]
            
            \item $f \in X$ derivabili in un punto $x_0 \in [0, 1]$, allora $\exists n \in \N$ per cui

                $$
                \forall x \in [0, 1], x \neq x_0 \; \abs[\bigg]{\frac{f(x) - f(x_0)}{x - x_0}} \leq n \implies f \in C_n
                $$

                Dunque $\displaystyle \{ f : [0, 1] \to \R \mid f \text{ continua e derivabile}\} \subset \bigcup_{n \in \N} C_n$

            \item $C_n$ è chiuso e raro. Dunque $\{ f : [0, 1] \to \R \text{ derivabile} \} \subset \bigcup C_n$ è un magro di $X$, dunque ha parte interna vuota e quindi $\{ f : [0, 1] \to \R \text{ continua e non derivabile in un punto} \} \subset X$ è denso.

        \end{enumerate}

        \textit{Mostriamo questi fatti.}

        \begin{enumerate}[label={\arabic*)}]
            
            \item Sia $f : [0, 1] \to \R$ derivabile $\implies \exists n \; f \in C_n$. Supponiamo $f$ derivabile in $x_0 \in [0, 1]$. Poniamo 

                $$
                \begin{gathered}
                    \ell = \lim_{x \to x_0} \abs[\bigg]{\frac{f(x) - f(x_0)}{x - x_0}} \\
                    \implies \exists \delta > 0 \text{ tale che } \forall x \in (x_0 - \delta, x_0 + \delta) \; \abs[\bigg]{\frac{f(x) - f(x_0)}{x - x_0}} \leq \ell + 1
                \end{gathered}
                $$

                D'altra parte $\abs[\big]{\frac{f(x) - f(x_0)}{x - x_0}}$ è continua su $[0, x_0 - \delta] \cup [x_0 + \delta, 1] \implies$ Per Weierstrass $\abs[\big]{\frac{f(x) - f(x_0)}{x - x_0}}$ è limitata fuori da cui ricaviamo che

                $$
                \exists n \in \N \text{ con } \forall x \neq x_0 \; \abs[\bigg]{\frac{f(x) - f(x_0)}{x - x_0}} \leq n
                $$

            \item $C_n$ è un chiuso raro.

                \begin{itemize}
                    \item $C_n$ è chiuso.

                        Vediamo che $C_n$ è chiuso per successioni. Sia $\{ f_k \} \subset C_n$. Supponiamo $f_k \to f$ in $X$. Vediamo che $f \in C_n$. Per ogni $k \; \exists x_k \in [0, 1]$ con $\forall x \neq x_k \; \abs[\big]{\frac{f_k(x) - f_k(x_k)}{x - x_k}} \leq n$ e $\{ x_k \} \subset [0, 1] \implies$ esiste una sottosuccessione di $x_k$ convergente. Dunque guardando la sottosuccessione corrispondente di $\{ f_k \}$ posso avere $x_k \to x_0$. 

                        Vediamo che $\forall x \neq x_k \; \abs[\big]{\frac{f(x) - f(x_k)}{x - x_k}} \leq n$. Supponiamo $\forall x \neq x_k \; \abs[\big]{\frac{f_k(x) - f_k(x_k)}{x - x_k}} \leq n$ e portando al limite per $k \to \infty$ otteniamo

                        $$
                        \forall x \neq x_0 \; \abs[\bigg]{\frac{f(x) - f(x_0)}{x - x_0}} \leq n
                        $$

                    \item $C_n$ è raro.

                        Vediamo che $C_n^\circ = \varnothing$. Supponiamo che non lo sia $\implies \exists f \in C_n$ e un $\varepsilon > 0$ tale che $\ball{f, \varepsilon} \subseteq C_n$. $\ball{f, \varepsilon} = \{ g : [0, 1] \to \R \text{ con } d_\infty(f, g) < \varepsilon \}$. $\forall \varepsilon > 0$ posso trovare $\ball{f, \varepsilon}$ una funzione in cui i rapporti incrementali sono tutti abbastanza grandi. [TODO: Probabilmente Frigerio riprenderà la dimostrazione la prossima volta]

                \end{itemize}
        \end{enumerate}
    \end{proof}
\end{theorem}











% [BM - Bookmark: Ultima cosa di teoria]

\end{document}
