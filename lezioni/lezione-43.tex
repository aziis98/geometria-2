\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 18 Marzo 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

Iniziamo ora a studiare $\CC$ e le funzioni da $\CC$ a $\CC$. Vedremo come è possibile definire il concetto di continuità utilizzando che $\CC \simeq \R^2$ e poi passeremo ad aggiungere il concetto di derivabilità e differenziabilità e vedremo come questi passano su $\CC$.

\section{Funzioni complesse}

$U \subseteq \CC$ un aperto, $f : U \to \CC$. Possiamo scrivere $\forall z \in U \quad f(z) = u(z) + iv(z)$

Chiamiamo

\begin{itemize}
    \item $u(z)$ la parte reale della di $f$, $u : U \to \R$
    \item $v(z)$ la parte immaginaria della di $f$, $v : U \to \R$
\end{itemize}

\begin{fact}
    $f : U \to \CC$ è continua in $z_0 \in U$ (e rispettivamente su tutto $U$) $\iff u$ e $v$ sono continue in $z_0 \in U$ (rispettivamente in tutto $U$)
\end{fact}

Da un punto di vista della continuità le funzioni da $U$ a valori complessi possono essere semplicemente viste come funzioni da $U \subseteq \R^2$ a valori reali.

\textbf{Domanda.} Vale la stessa cosa quando parliamo di derivabilità?

\begin{definition}
    La funzione $f : U \to \CC$ è differenziabile in $z_0 \in U$ se $\exists A : \CC \to \CC$ applicazione $\R$-lineare tale che

    \begin{itemize}
        \item $f(z) = f(z_0) + A (z - z_0) + r(z)$
        \item $\displaystyle \frac{|r(z)|}{|z - z_0|} \xrightarrow{z \to z_0} 0$
    \end{itemize}
\end{definition}

In questo caso, chiamiamo l'applicazione $A$ lo \textbf{Jacobiano} di $f$

$$
J f_{z_0} := A
$$

\begin{observation}
    $f$ è differenziabile in $z_0 \implies f$ è continua in $z_0$
\end{observation}

Fissiamo la base $\{1, i\}$ di $\CC$, come spazio vettoriale reale:

$$
\begin{array}{rccc}
    f : & U & \longrightarrow & \CC \\
    & z = x + iy & \longmapsto & u(x, y) + i v(x, y)
\end{array}
$$

\begin{facts} \ 
    \begin{itemize}
        \item Se $f$ è differenziabile in $z_0 \in U \implies \exists$ le derivate parziali di $f$ in $z_0$.

            $$
            J f_{z_0} (z_0) = 
            \begin{pmatrix}
                \dfrac{\pd u}{\pd x} (x_0, y_0)
                & \dfrac{\pd u}{\pd y} (x_0, y_0) \\
                \dfrac{\pd v}{\pd x} (x_0, y_0)
                & \dfrac{\pd v}{\pd y} (x_0, y_0) \\
            \end{pmatrix}
            $$

            dove $z_0 = x_0 + i y_0$

        \item \textbf{Teorema del Differenziale Totale.} Se $\exists \dfrac{\pd u}{\pd x}, \dfrac{\pd u}{\pd y}, \dfrac{\pd v}{\pd x}$ e $\dfrac{\pd v}{\pd y}$ in un intorno di $z_0$ e sono continue in $z_0$, allora $f$ è differenziabile in $z_0$.

    \end{itemize}
\end{facts}

\textbf{Esercizio.} $f : \R^2 \to \R$ data da $f(x, y) = x$ se $y \neq x^2$ o $f(x, y) = 0$ se $y = x^2$, è $f$ differenziabile in $(0, 0)$?

\begin{definition}
    Sia $U \subseteq \CC$ aperto, $f : U \to \CC$ funzione continua, diciamo che $f$ è \textbf{olomorfa} in $z_0 \in \CC$ se esiste il limite

    $$
    \lim_{h \to 0} \frac{f(z_0 + h) - f(z_0)}{h}
    $$

    Se tale limite esiste, lo chiamiamo derivata di $f$ in $z_0$ e lo denotiamo con $f'(z_0)$.

    Diciamo che $f$ è \textbf{olomorfa} in $U$ se è olomorfa $\forall z_0 \in U$.

    Diciamo che $f$ è intera se $f$ è definita su tutto $\CC$ ed è olomorfa in $\CC$
\end{definition}

\begin{prop} \ 
    \begin{enumerate}[label={\arabic*)}]
        \item $f + g$ è olomorfa in $z_0$ e

            $$
            (f + g)'(z_0) = f'(z_0) + g'(z_0)
            $$

        \item $f \cdot g$ è olomorfa in $z_0$ e

            $$
            (f \cdot g)'(z_0) = f'(z_0) g(z_0) + f(z_0) g'(z_0)
            $$

        \item Se $g(z_0) \neq 0$, allora $\frac{f}{g}$ è olomorfa in $z_0$ e

            $$
            \lrpa{\frac{f}{g}}'(z_0) = \frac{f'(z_0)g(z_0) - f(z_0)g'(z_0)}{g(z_0)^2}
            $$

    \end{enumerate}
\end{prop}

\begin{prop}
    Siano $U, V$ aperti di $\CC$. Siano $f : U \to V$ e $g : U \to \CC$ due funzioni, assumiamo che $f$ è olomorfa in $z_0 \in U$ e $g$ è olomorfa in $f(z_0)$, allora $g \compose f$ è olomorfa in $z_0$ e $(g \compose f)'(z_0) = g'(f(z_0)) f'(z_0)$
\end{prop}

\begin{examples}
    Vediamo ora degli esempi concreti di funzioni olomorfe

    \begin{itemize}
        \item $f(z) = z$ è una funzione intera, infatti

            $$
            \frac{f(z + h) - f(z)}{h} = \frac{z + h - z}{h} = 1
            $$

            quindi $f$ è olomorfa $\forall z \in \CC$ e $f'(z) = 1$

        \item Sia $n \geq 1$ allora $f(z) = z^n$ è intera infatti

            $$
            \begin{aligned}
                & \frac{f(z + h) - f(z)}{h} 
                = \frac{(z + h)^n - n^n}{h} \\
                &= \frac{\sum_{k=0}^n \binom{n}{k} z^k h^{n - k} - z^n }{h} 
                = \sum_{k=0}^{n-1} \binom{n}{k} z^k h^{n - k - 1} \\
                & \xrightarrow{h \to 0} \binom{n}{n - 1} z^{n-1}
                = n z^{n-1} =: f'(z)
            \end{aligned}
            $$

        \item Provare che $n < 0, f(z) = z^n$ è olomorfa in $\CC \setminus \{ 0 \}$ 
    \end{itemize}
\end{examples}

\begin{observations} \ 
    \begin{itemize}
        \item I polinomi sono funzioni intere. 
        \item Le funzioni razionali, ovvero i quozienti della forma $\frac{P(z)}{Q(z)}$ con $P, Q$ polinomi sono olomorfi in $\CC \setminus \{ z \in \CC \mid Q(z) = 0 \}$.
    \end{itemize}
\end{observations}

\begin{example}
    Una funzione non olomorfa è $f(z) = \bar z$, dove con $\bar z$ indichiamo il coniugio in $\CC$. 

    \begin{proof}
        Infatti abbiamo che

        $$
        \frac{f(z + h) - f(z)}{h} = \frac{\bar z + \bar h - \bar z}{h} = \frac{\bar h}{h}
        $$

        per $h \in \CC, h \neq 0$.

        Se $\exists f'(z)$, allora il limite dovrebbe esistere per ogni possibile direzione per cui $h \to 0, h \neq 0$. Ma se ad esempio ci avviciniamo a $z$ lungo la retta reale (ovvero $\Imm h = 0$) e lungo la retta immaginaria (ovvero con $\Re h = 0$) dovremmo ottenere sempre lo stesso limite ma abbiamo

        $$
        \lim_{\substack{h \to 0 \\ \Imm h = 0}} \frac{\bar h}{h} = 1 
        \qquad
        \lim_{\substack{h \to 0 \\ \Re h = 0}} \frac{\bar h}{h} = -1
        $$

        $\implies$ non può esistere il limite.
    \end{proof}
\end{example}

\textbf{Domanda.} Qual è il legame tra l'olomorficità di $f : U \to \CC$ e la differenziabilità di $f$ vista come funzione da $U \subseteq \R^2$ in $\R^2$?

\begin{observation}
    Come nel caso della differenziabilità in ambito reale, $f$ olomorfa in $z_0 \implies f$ continua in $z_0$.

    \begin{proof}
        $$
        f(z) - f(z_0) = \lrpa{\frac{f(z) - f(z_0)}{z - z_0}} (z - z_0)
        $$

        al limite per $z \to z_0$, abbiamo che viene $\displaystyle f'(z_0) \lim_{z \to z_0} (z - z_0) = 0$
    \end{proof}
\end{observation}

\begin{example}
    $g(z) = \bar z$ non è olomorfa ma nella base $\{1, i\}$ di $\CC$

    $$
    \begin{array}{rccc}
        g : & \R^2 & \longrightarrow & \R^2 \\
        & (x, y) & \longmapsto & (x, -y)
    \end{array}
    $$

    è differenziabile.
\end{example}

\begin{theorem}
    Sia $U \subseteq \CC$ un aperto e $f : U \to \CC$ una funzione continua. $f$ è olomorfa in $z_0 \in U \iff$ le seguenti due condizioni sono soddisfatte

    \begin{enumerate}
        \item $f$ è differenziabile nel punto $z_0 \in U$
        \item $J f_{z_0} : \CC \to \CC$ corrisponde alla moltiplicazione per $a \in \CC$
    \end{enumerate}

    \begin{proof} \
        \begin{itemize}
            \itemRarr La condizione di olomorficità implica che

                $$
                f(z_0 + h) = f(z_0) + \underbrace{f'(z_0)}_a h + r(h)
                $$

                con $\dfrac{|r(h)|}{|h|} \xrightarrow{h \to 0} 0$. Nella base $\{1, i\}$ possiamo riscrivere l'equazione come

                $$
                f(x_0 + \alpha, y_0 + \beta) = f(x_0, y_0) + f'(z_0)(\alpha + i\beta) + r(\alpha, \beta) \quad \text{ con } \quad \frac{|r(\alpha, \beta)|}{\sqrt{\alpha^2 + \beta^2}} \longrightarrow 0
                $$

                Posto $a := f'(z_0)$, la mappa

                $$
                \begin{array}{ccc}
                    \CC & \longrightarrow & \CC \\
                    z & \longmapsto & az
                \end{array}
                $$

                è $\R$-lineare $\implies f$ è differenziabile in $z_0$ e $J f_{z_0} = $ moltiplicazione per $a$.

            \itemLarr La dimostrazione è analoga. Si parte della definizione di differenziabilità (nelle variabili $x, y$), si riscrive in termini della variabile $z$. Si nota che per ii. $J f_{z_0} = $ moltiplicazione per un numero complesso $a \in \CC \implies f'(z_0) = a$.

        \end{itemize}
    \end{proof}
\end{theorem}

Vorremmo ora sostituire la condizione ii. con le cosiddette condizioni di Cauchy-Riemann

\begin{lemma}
    Sia $A : \CC \to \CC$ una mappa $\R$-lineare allora le seguenti affermazioni sono equivalenti.

    \begin{itemize}
        \item $A$ è indotta dalla moltiplicazione per un numero complesso, cioè $A(z) = a \cdot z$ per un certo $a \in \CC$.
        \item $A$ è $\CC$-lineare
        \item $A(i) = i A(1)$
        \item $A =$ moltiplicazione per la matrice $\displaystyle \begin{pmatrix} \alpha & -\beta \\ \beta & \alpha \end{pmatrix}$ dove $\alpha, \beta \in \R$ (nella base $\{1, i\}$ di $\CC$)
    \end{itemize}
\end{lemma}

\begin{observation}
    $a = \alpha + i \beta$
\end{observation}

\begin{definition}

    Sia $U \subseteq \CC$ un aperto e $f : U \to \CC$ continua. Denotiamo $f(x, y) = u(x, y) + i v(x, y)$ nella base $\{ 1, i \}$ di $\CC$ allora 

    $f$ è olomorfa in $z_0 \in U \iff$ le seguenti due condizioni sono soddisfatte:

    \begin{enumerate}
        \item $f$ è differenziabile in $z_0 \in U$
        \item[ii')] Valgono le condizioni di Cauchy-Riemann

            $$
            \frac{\pd u}{\pd x}(x_0, y_0) = \frac{\pd v}{\pd y}(x_0, y_0) 
            \quad \text{ e } \quad
            \frac{\pd u}{\pd y}(x_0, y_0) = -\frac{\pd v}{\pd x}(x_0, y_0) 
            $$

            dove $z_0 = x_0 + i y_0$. Nel caso $f$ sia olomorfa 

            $$
            \implies f'(z_0) 
            = \frac{\pd u}{\pd x}(z_0) + i \frac{\pd v}{\pd x}(x_0, y_0) 
            = \frac{\pd v}{\pd y}(z_0) - i \frac{\pd u}{\pd y}(x_0, y_0)
            $$
    \end{enumerate}    
\end{definition}

\begin{proof}
    Dobbiamo semplicemente provare che ii) del teorema precedente equivale a ii'). Per la differenziabilità di $f$ abbiamo

    $$
    J f_{z_0} (z_0) = 
    \begin{pmatrix}
        \dfrac{\pd u}{\pd x}(z_0) & \dfrac{\pd u}{\pd y}(z_0) \\
        \dfrac{\pd v}{\pd x}(z_0) & \dfrac{\pd v}{\pd y}(z_0) 
    \end{pmatrix}
    = \begin{pmatrix}
        \alpha & -\beta \\
        \beta & \alpha \\
    \end{pmatrix}
    $$

    usando il lemma per i termini delle diagonali, per le antidiagonali invece [...].
\end{proof}

\subsection{Come usare il teorema di Cauchy-Riemann}

\begin{definition}
    Per ogni $z = x + iy \in \CC$, chiamiamo \textbf{esponenziale} \textit{del numero complesso $z$} la quantità

    $$
    e^z := e^x (\cos(y) + i \sin(y))
    $$
\end{definition}

\begin{prop}
    $f(z) = e^z$ è intera con $f'(z) = e^z$.

    \begin{proof}
        Nella base $\{ 1, i \}$ di $\CC$ scriviamo $f(x, y) = u(x, y) + i v(x, y)$ con $u(x, y) = e^x \cos(y)$ e $v(x, y) = e^x \sin(y)$

        \begin{enumerate}
            \item $f$ è differenziabile
            \item Verifichiamo le condizioni di Cauchy-Riemann

                $$
                \begin{aligned}
                    \frac{\pd u}{\pd x} = e^x \cos(y) 
                    & \frac{\pd u}{\pd y} = -e^x \sin(y) \\ 
                    \frac{\pd v}{\pd x} = e^x \sin(y) 
                    & \frac{\pd v}{\pd y} = e^x \cos(y)
                \end{aligned}
                $$

                $\implies f(z)$ soddisfa le condizioni di Cauchy-Riemann.
        \end{enumerate}
    \end{proof}
\end{prop}

\begin{examples}
    Altre funzioni interse sono le seguenti

    $$
    \begin{aligned}
        \sin(z) &:= \frac{e^{iz} - e^{-iz}}{2i} \quad
        & \cos(z) &:= \frac{e^{iz} + e^{-iz}}{2} \\
        \sinh(z) &:= \frac{e^{z} - e^{-z}}{2} \quad
        & \cosh(z) &:= \frac{e^{z} + e^{-z}}{2} \\
    \end{aligned}
    $$
\end{examples}

\end{document}
