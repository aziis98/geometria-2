\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezioni \& aggiornamenti del 22, 23 e 27 Aprile}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}


\textbf{Teorema.}
Sia $a \in \CC$ e sia $f$ una funzione olomorfa nella corona circolare $A = \{ z \in \CC \mid 0 < \rho_2 < |z - a| < \rho_1 < \infty \}$, allora $f$ ha un'espansione di Laurent cioè esplicitamente $\forall z \in A$

$$
f(z) = \sum_{n \in \Z} a_n (z - a)^n \text{ con } \forall n \; a_n := \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{(w - a)^{n+1}} \dd w  
$$ 

\textit{Dimostrazione.}
Senza prendere di generalità prendiamo $a = 0$, scegliamo $r_1, r_2, r_1', r_2'$ tali che

$$
\rho_1 < r_2' < r_2 < r_1 < r_1' < \rho_2
$$

siano $\gamma_1 = t \mapsto r_1' e^{2 \pi i t}$, $\gamma_2 = t \mapsto r_2' e^{2 \pi i t}$.

$B := \{ z \in \CC \mid r_2' \leq |z| \leq r_1' \}$, sia $z \in \CC$ tale che $r_2' < r_2 \leq |z| \leq r_1 < r_1'$. Sia $r > 0$ tale che

$$
\{ w \in \CC \mid |w - z| \leq r \} \subset \{ w \in \CC \mid r_2' < |w| < r_1' \}
$$

$\alpha$ è una parametrizzazione del bordo di $\overline{B(z, r)}$, col verso in senso antiorario.

[TODO: \textit{Addendum} e disegnino]

\begin{observation}
    $\forall \omega$ forma chiusa in $A \setminus B(z, r)$

    $$
    \int_{\gamma_1} \omega - \int_{\gamma_2} \omega - \int_{\alpha} \omega = 0
    $$
\end{observation}

In particolare $\dfrac{f(w)}{w - z}$ è olomorfa in $A \setminus B(z, r) \implies \omega = \dfrac{f(w)}{w - z} \dd w$ è chiusa, sostituendo ad $\omega$ sopra l'espressione

$$
\frac{1}{2 \pi i} \int_{\gamma_1} \frac{f(w)}{w - z} \dd w - \frac{1}{2 \pi i} \int_{\gamma_2} \frac{f(w)}{w - z} \dd w = \frac{1}{2 \pi i} \int_{\alpha} \frac{f(w)}{w - z} \dd w = \underbrace{I(\alpha, z)}_{=1} f(z)  
$$

\begin{wrapfigure}[10]{o}{0.325\textwidth}
    \raisebox{0pt}[9\baselineskip]{
        \def\svgwidth{0.3\textwidth}
        \input{./images/laurent-series.pdf_tex}
    }
\end{wrapfigure}

Abbiamo visto che le funzioni olomorfe su un disco ammettono uno sviluppo in serie sul disco stesso (cioè sono analitiche); ora concludiamo la dimostrazione che funzioni olomorfe su corone circolari ammettono uno sviluppo di Laurent.

Sia $A = \{ z \mid \rho_2 < |z| < \rho_1 \}$ e sia $z \in A$. Siano $r_1, r_2$ scelti in modo che $0 < \rho_2 < r_2 < |z| < r_1 < \rho_1$. Sia $B = A \setminus C$, $C$ piccola palla centrata in $z$. Sia $\alpha = \alpha_1 * \alpha_2$ una parametrizzazione in senso antiorario del bordo di questa palla come in figura. Il cammino $\beta = l_2 * \alpha_1 * l_1 * \gamma_2 * \bar l_1 * \alpha_2 * \bar l_2 * \bar \gamma_1$

% [TODO: Disegno di tutte le curve]

borda un disco (più precisamente $\hat \beta : S^1 \to B$ si estende a $D^2 \to B$) in $B$, perciò è omotopicamente banale in $B$. Dunque, se $f : A \to \CC$ è olomorfa, allora $w \mapsto \dfrac{f(w)}{z - w}$ è olomorfa su $B$ per cui $\omega = \dfrac{f(w)}{z - w} \dd w$ è chiusa in $B$ e

$$
\begin{aligned}
    0 &= \int_{\beta} \omega = \int_{l_2} \omega + \int_{\alpha_1} \omega + \int_{l_1} \omega + \int_{\gamma_2} \omega + \int_{\bar l_1} \omega + \int_{\alpha_2} \omega + \int_{\bar l_2} \omega + \int_{\bar\gamma_1} \omega \\
    &= \int_{\gamma_2} \omega - \int_{\gamma_1} \omega + \int_{\alpha_1} \omega + \int_{\alpha_2} \omega = \int_{\gamma_2} \omega - \int_{\gamma_2} \omega - \int_{\gamma_1} \omega + \int_{\alpha} \omega \\
\end{aligned}
$$

Perciò $\ds \int_{\alpha} \omega = \int_{\gamma_1} \omega - \int_{\gamma_2} \omega$. Analizziamo perciò $\ds\int_{\gamma_1} \omega$ e $\ds\int_{\gamma_2} \omega$

Per il primo dei due si fa esattamente lo stesso conto fatto per la formula di Cauchy. Lungo $\gamma_1$, abbiamo $|\gamma_1(t)| = r_1 > |z|$ perciò se $w = \gamma_1(t)$ abbiamo

$$
\frac{f(w)}{w - z} = \frac{f(w)}{w \lrpa{a - \frac{z}{w}}} = \frac{f(w)}{w} \cdot \frac{1}{1 - \frac{z}{w}} = \frac{f(w)}{w} \cdot \sum_{n=0}^\infty \lrpa{\frac{z}{w}}^n = \sum_{n=0}^\infty = \sum_{n=0}^\infty \frac{z^n f(w)}{w^{n+1}}
$$ 

Poiché la convergenza di questa serie nella corona $\{ r_2 \leq |w| \leq r_1 \}$ è normale, dunque uniforme, possiamo scambiare serie e integrale nella seguente catena di uguaglianze

$$
\int_{\gamma_1} \omega = \int_{\gamma_1} \frac{f(w)}{z - w} \dd w = \int_{\gamma_1} \lrpa{\sum_{n \geq 0} \frac{z^n f(w)}{w^{n+1}}} \dd w = \sum_{n \geq 0} z^n \int_{\gamma_1} \frac{f(w)}{w^{n+1}} \dd w = \sum_{n \geq 0} b_n z^n
$$

dove $b_n = \ds \int_{\gamma_1} \frac{f(w)}{w^{n+1}} \dd w$.

Notiamo subito che, se $\gamma$ è un qualsiasi cammino liberamente omotopo a $\gamma_1$, in $\CC \setminus \{ 0 \}$, cioè se $I(\gamma, 0) = 1$ e $\gamma : [0, 1] \to A$, allora poiché $\dfrac{f(w)}{w^{n+1}} \dd w$ è chiusa in $A$, e essere liberamente omotopi in $A$ equivale a esserlo in $\CC \setminus \{ 0 \}$ ($\CC \setminus \{ 0 \}$ si retrae per deformazione su $A$), allora $\ds b_n = \int_{\gamma} \frac{f(w)}{w^{n+1}} \dd w, n \geq 0$. Passiamo ora a $\ds \int_{\gamma_2} \frac{f(w)}{w - z} \dd w$. Ora se $w = \gamma_2(t), |w| < |z|$, per cui

$$
\begin{aligned}
    \frac{f(w)}{w - z} &= - \frac{f(w)}{z - w} = -\frac{f(w)}{z - w} = -\frac{f(w)}{z \lrpa{1 - \frac{w}{z}}} = - \frac{f(w)}{z} \sum_{n \geq 0} \lrpa{\frac{w}{z}}^n \\
    &= - \frac{f(w)}{z} \sum_{n \leq 0} \lrpa{\frac{z}{w}}^n = -\frac{f(w)}{z} \cdot \frac{z}{w} \cdot \sum_{n < 0} \lrpa{\frac{z}{w}}^n = -\frac{f(w)}{w} \cdot \sum_{n < 0} \frac{z^n}{w^n} \\
    &= -\sum_{n < 0} \frac{f(w) z^n}{w^{n+1}} \\
\end{aligned}
$$

ed esattamente come prima,

$$
- \int_{\gamma_2} \omega = - \int_{\gamma_2} \frac{f(w)}{w - z} \dd w = \int_{\gamma_2} \lrpa{\sum_{n < 0} \frac{z^n f(w)}{w^{n+1}}} \dd w = 
= \sum_{n < 0} z^n \int_{\gamma_2} \frac{f(w)}{w^{n+1}} \dd w = \sum_{n < 0} b_n z^n 
$$

dove $\ds b_n = \int_{\gamma_2} \frac{f(w)}{w^{n+1}} \dd w = \int_{\gamma} \frac{f(w)}{w^{n+1}} \dd w$, $\forall \gamma : [0, 1] \to A$ con $I(\gamma, 0) = 1$. Dunque

$$
f(z) = \frac{1}{2\pi i} \int_{\alpha} \frac{f(w)}{w - z} \dd w = \frac{1}{2\pi i} \lrbt{\int_{\gamma_1} \omega - \int_{\gamma_2} \omega} = \frac{1}{2\pi i} \lrpa{\sum_{n \geq 0} b_n z^n + \sum_{n < 0} b_n z^n} = \frac{1}{2\pi i} \sum_{n=-\infty}^{+\infty} b_n z^n
$$

(la serie si può riordinare per convergenza assoluta ma $\sum_{-\infty}^\infty k_n$ è per definizione $\sum_{0}^\infty k_n + \sum_{1}^\infty k_{-n}$, per cui l'uguaglianza finale vale per definizione). Dunque $f$ ammette lo sviluppo di Laurent

$\ds f(z) = \sum_{n \in \Z} a_n z^n$ con $a_n = \ds \frac{b_n}{2\pi i} = \frac{1}{2 \pi i} \int_{\gamma} \frac{f(w)}{w^{n+1}} \dd w$, per qualsiasi $\gamma$ in $A$ con $I(\gamma, 0) = 1$. (la formula per gli $a_n$ è infatti indipendente da $z$). $\hfill \square$

\begin{observation}
    Poiché $\ds a_n = \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w^{n+1}} \dd w$, si ha (come nel caso già visto, cioè $n \geq 0$), $\forall n \in \Z \; |a_n| \leq \dfrac{M(R)}{R^n}, M(R) = \sup \{ |f(w)| \mid |w| = R \}$
\end{observation}

\begin{corollary}
    $A = \{ \rho_2 < |z| < \rho_1 \}, f : A \to \CC$ olomorfa. Allora $f = f_1 + f_2$, dove $f_1 : B(0, \rho_1) \to \CC, f_2 : \CC \setminus \overline{B(0, \rho_2)} \to \CC$ sono olomorfe. Richiedendo $\lim_{|z| \to +\infty} |f_2(z)| = 0$ questa scrittura è unica.

    \begin{proof}
        $\ds f(z) = \sum_{-\infty}^\infty a_n z^n$. Per l'esistenza basta porre $\ds f_1 = \sum_{n \geq 0} a_n z^n$ e $\ds f_2 = \sum_{n < 0} a_n z^n$

        [TODO: Disegno di $A$ e delle tre regioni]

        Per l'unicità, se $f = g_1 + g_2$, possiamo definire $F : \CC \to \CC$ ponendo

        $$
        F(z) =
        \begin{cases}
            f_1(z) - g_1(z) & |z| < \rho_1 \\
            g_2(z) - f_2(z) & |z| > \rho_2 \\
        \end{cases}
        $$

        Se ben definita, $F$ è ovviamente olomorfa. Nell'intersezione $\rho_2 < |z| < \rho_1, f_1(z) + f_2(z) = g_1(z) + g_2(z)$ in quanto entrambi coincidono con $f(z)$, per cui $f_1(z) - g_1(z) = g_2(z) - f_2(z)$ come voluto.

        Infine $\lim_{|z| \to \infty} F(z) = \lim_{|z| \to \infty} g_2(z) - f_2(z) = 0 - 0 = 0$ per cui $F : \CC \to \CC$ è olomorfa e limitata, e per Liouville è costante. Avendo limite nullo $F \equiv 0$ e $f_1 = g_1$ e $f_2 = g_2$.
    \end{proof} 
\end{corollary}

\section{Singolarità}

Ci interessa studiare il comportamento di funzioni olomorfe su \textqt{dischi puntati}, cioè corone circolari della forma $0 < |z - a| < R$, cioè $B(a, R) \setminus \{ a \}$.

\begin{theorem}
    Sia $D \subseteq \CC$ un aperto, $f : D \setminus \{ z_0 \} \to \CC$ olomorfa, dove $z_0 \in D$. Allora $f$ si estende a una funzione olomorfa $g : D \to \CC \iff$ è limitata in un intorno di $z_0$, cioè $\exists \varepsilon > 0$ tale che $f {|}_{B(z_0, \varepsilon) \setminus \{ z_0 \}}$ sia limitata.

    \begin{proof}
        \begin{itemize}
            \itemRarr Se $f$ si estende a $g$, $g$ in quanto olomorfa è continua, e dunque localmente limitata.

            \itemLarr Sia $\varepsilon$ tale che $B = B(z_0, \varepsilon) \subseteq D$ e $\forall z \in B \setminus \{ z_0 \} \; |f(z)| \leq M$. $f$ ammette uno sviluppo di Laurent $\ds f(z) = \sum_{-\infty}^\infty a_n (z - z_0)^n$ su $B \setminus \{ z_0 \}$, con

            $$
            \forall R < \varepsilon \quad |a_n| \leq \frac{M(R)}{R^n} \leq \frac{M}{R^n}
            $$

            dove $M(R) = \sup \{ |f(z)| \mid |z - z_0| = R \}$. Se $n < 0, \lim_{R \to 0^+} \dfrac{M}{R^n} = 0$, per cui $a_n = 0$. Dunque la serie di Laurent ha solo termini con esponente positivo $\implies f$ si estende a una funzione $\bar f : B(z_0, \varepsilon) \to \CC$ olomorfa, e possiamo porre $g(z) = \bar f(z)$ se $z \in B(z_0, \varepsilon), g(z) = f(z)$ se $z \neq z_0$.
        \end{itemize}
    \end{proof}
\end{theorem}

Nel caso descritto del teorema precedente, $z_0$ si chiama \textbf{singolarità eliminabile} o \textbf{rimuovibile} di $f$. (Nota: $f : \R \setminus \{ 0 \} \to \R, f(x) = \sim \frac{1}{x}$ è $C^\infty$ e limitata ma non si estende con continuità a $0$)

Se invece $f : D \setminus \{ z_0 \} \to \CC$ non si estende in $z_0$ a una funzione olomorfa, $z_0$ si chiama \textbf{singolarità isolata}.

\begin{definition}
    Sia $z_0$ una singolarità isolata di $f : D \setminus \{ z_0 \} \to \CC$ olomorfa con sviluppo di Laurent $\ds f(z) = \sum_{-\infty}^\infty a_n (z - z_0)^n$ in un piccolo disco puntato centrato in $z_0$. Allora $z_0$ si dice

    \begin{itemize}
        \item \textbf{polo} di ordine $n_0$ se $\{ a_n \mid n < 0, a_n \neq 0 \}$ è finito e $n_0 = -\min \{ a_n \mid n < 0, a_n \neq 0 \}$
        \item \textbf{singolarità essenziale} se $\{ a_n \mid n < 0, a_n \neq 0 \}$ è infinito.
    \end{itemize}
\end{definition}

Il fatto che se $a_n \neq 0$ per qualche $n < 0$ allora la singolarità non è eliminabile può essere visto così: se $a_{n_0} \neq 0, n_0 < 0$ sia $g(z) = f(z) (z - z_0)^{|n_0| - 1}$. Se $f$ fosse estendibile, lo sarebbe anche $g$. Se $b_n$ è il coeficiente $n$-esimo dello sviluppo di Laurent di $g$, avremmo $b_{-1} = a_{n_0} \neq 0$.

Ora $\ds b_{-1} = \frac{1}{2\pi i} \int_{\gamma} \frac{g(w)}{(w - z_0)^{-1+1}} \dd w = \frac{1}{2\pi i} \int_{\gamma} g(w) \dd w = 0$ perché $g$ olomorfa anche in $z_0$.

Questa uguaglianza si può dimostrare senza usare l'unicità, osservando che se $\ds g(z) = \sum_{-\infty}^\infty b_n (z - z_0)^n$, allora $\ds g(z) = \lrpa{\sum_{n \neq -1} b_n (z - z_0)^n} + \frac{b_{-1}}{z - z_0}$

$$
\int_{\gamma} g(z) = \int_{\gamma} \frac{b_{-1}}{z - z_0} \dd z = \frac{1}{2\pi i} b_{-1}
$$

\subsection{Unicità dello sviluppo in serie di Laurent}

Per semplicità sia $z_0 = 0$ e sia $f : D \to \CC$ olomorfa e $D = \{ z \mid \rho_2 < |z| < \rho_1 \}$ con sviluppo $\ds f(z) = \sum_{-\infty}^\infty a_n z^n$.

Sia $k \in \Z$ fissato e sia $\gamma : [0, 1] \to D$ con $I(\gamma, 0) = 1$ e calcoliamo

$$
\int_{\gamma} \frac{f(w)}{z^{k+1}} \dd z = \int_{\gamma} \frac{1}{z^{k+1}}\sum_{-\infty}^\infty a_n z^n \dd z = \int_{\gamma} \lrpa{\sum_{-\infty}^\infty a_n z^{n-k-1}} \dd z = \sum_{-\infty}^\infty a_n \int_{\gamma} z^{n-k-1} \dd z   
$$

Ora, $z^i \dd z$ è esatta su $\CC \setminus \{ 0 \}$ (dunque su $D$) per $i \neq -1$, con primitiva $\dfrac{z^{i+1}}{i+1}$, dunque se $n - k - 1 \neq -1$, cioè se $n \neq k$

$$
\int_{\gamma} z^{n-k-1} \dd z = 0 
$$

Perciò $\ds \int_{\gamma} \frac{f(z)}{z^{k+1}} \dd z = \sum_{-\infty}^\infty a_n \int_{\gamma} z^{n-k-1} \dd z = a_k \int_{\gamma} \frac{1}{z} \dd z = 2 \pi i a_k$. Dunque $\ds a_k = \frac{1}{2\pi i} \int_{\gamma} \frac{f(z)}{z^{k+1}}$, perciò gli $a_k$ sono univocamente determinati da $f$, cioè lo sviluppo di Laurent è unico.     

In particolare per $k = -1$

$$
\int_{\gamma} f(z) \dd z = 2 \pi i a_{-1}
$$

ed $a_{-1}$ è detto \textbf{residuo di $f$ in $0$}. In generale se $f : B \setminus \{ z_0 \} \to \CC$ è olomorfa dove $B$ è una palla di centro $z_0$, il \textbf{residuo} di $f$ in $z_0$ è

$$
\Res(f, z_0) = a_{-1}
$$

dove $\ds \sum_{n = -\infty}^\infty a_n (z - z_0)^n$ è lo sviluppo di Laurent di $f$ centrato in $z_0$.

\section{Singolarità Isolate}

Sia $z_0$ una singolarità isolata di $f : D \setminus \{ z_0 \} \to \CC$ con $D$ aperto, $z_0 \in D$ ed $f$ olomorfa.

\begin{prop}
    Sono fatti equivalenti

    \begin{enumerate}
        \item $z_0$ è un polo di ordine $n_0$ di $f$
        \item $f(z) = \dfrac{g(z)}{(z - z_0)^{n_0}}$ con $g$ olomorfa su $D$ e $g(z_0) \neq 0$
        \item $\dfrac{1}{f}$ si estende a una funzione olomorfa $k : U \to \CC$ in un intorno di $z_0$, con $z_0$ zero di ordine $n_0$ per $k$.
    \end{enumerate}

    \begin{proof}
        \item[$\boxed{\text{i)} \Rightarrow \text{ii)}}$]
        $\ds f(z) \sum_{n \geq -n_0} a_n(z - z_0)^n$, con $a_{-n_0} \neq 0$ dunque $\ds f(z) = (z - z_0)^{-n_0} \cdot \sum_{n \geq 0} a_{n - n_0} (z - z_0)^n = \frac{1}{(z - z_0)^{n_0}} \cdot g(z)$, con $g$ olomorfa, $g(z_0) = a_{-n_0} \neq 0$

        \item[$\boxed{\text{ii)} \Rightarrow \text{iii)}}$]
        Se $\ds f(z) = \frac{g(z)}{(z - z_0)^{n_0}}, g(z_0) \neq 0$, in un intorno di $z_0$ abbiamo che $g$ non si annulla per cui è ben definita e olomorfa la funzione $k(z) = \dfrac{1}{f(z)} = (z - z_0)^{n_0} \cdot \frac{1}{g(z)}$ che ha in $z_0$ uno zero di ordine $n_0$ (si ricordi una delle definizioni equivalenti di ordine di $0$). 

        \item[$\boxed{\text{iii)} \Rightarrow \text{i)}}$]
        Se $k(z) = h(z) \cdot (z - z_0)^{n_0}, h(z_0) \neq 0$ e $k = \dfrac{1}{f}$ in $B \setminus B \setminus \{ z_0 \}, B$ palla che contiene $z_0$ allora in $B \setminus \{ z_0 \}$ abbiamo $f(z) = \frac{1}{h(z)} \cdot \frac{1}{(z - z_0)^{n_0}} = \lrpa{\sum_{n = 0}^\infty a_n (z - z_0)^n} \cdot \frac{1}{(z - z_0)^{n_0}} = \sum_{n \geq -n_0} a_{n+n_0} (z - z_0)^n$, dunque $z_0$ è un polo di $f$ di ordine $n_0$ (in quanto $a_0 \neq 0$)
    \end{proof}
\end{prop}
    
\begin{definition}
    $D \subseteq \CC$ aperto. Una funzione \textbf{meromorfa} su $D$ è una funzione olomorfa $f : D \setminus S \to \CC$ dove $S$ è un insieme discreto (e chiuso in $D$) e $\forall z_0 \in S, f$ ha un polo in $z_0$.
\end{definition}

Grazie alla caratterizzazione dei poli appena vista, se $f, g : D \to \CC$ sono olomorfe e $g$ non è costantemente nullla, allora $\dfrac{f}{g}$ è una funzione meromorfa su $D$ (ricordiamo che gli zeri di una funzione olomorfa sono discreti). L'unica cosa non completamennte ovvia è capire cosa succede se $z_0$ è tale che $f(z_0) = g(z_0) = 0$. In tal caso, se $n$ è l'ordine di $z_0$ come zero di $f$, e $m$ è l'ordine di $z_0$ come zero di $g$,

\begin{itemize}
    \item $n \geq m$, $\dfrac{f}{g}$ si estende a una funzione olomorfa in $z_0$
    \item $n < m$, $\dfrac{f}{g}$ ha in $z_0$ un polo di ordine $|n - m|$
.\end{itemize}

[TODO: E' un esercizio]

\begin{corollary}
    Se $f$ ha un polo in $z_0$, $\lim_{z \to z_0} |f(z)| = +\infty$

    \begin{proof}
        Segue da $f(z) = \dfrac{g(z)}{(z - z_0)^n}, g(z_0) \neq 0, n > 0$ per cui $|f(z)| = \frac{|g(z)|}{|z - z_0|^n} \to +\infty$
    \end{proof}
\end{corollary}

Al contrario, vicino alle singolarità essenziali abbiamo

\textbf{Teorema di Weierstrass.} Sia $z_0$ una singolarità essenziale per $f : D \setminus \{ z_0 \} \to \CC$. Allora per ogni intorno $U$ di $z_0, U \subseteq D$ l'insieme $f(U \setminus \{ z_0 \})$ è denso in $\CC$.

\begin{proof}
    Per assurdo, se $f(U \setminus \{ z_0 \})$ non è denso, $\exists a \in \CC$ con $B(a, R) \cap f(U \setminus \{ z_0 \}) = \varnothing, R > 0$. Sia $g : U \setminus \{ z_0 \} \to \CC$ data da $g(z) = \dfrac{1}{f(z) - a}$.

    $$
    \forall z \in U, |g(z)| = \frac{1}{|f(z) - a|} \leq \frac{1}{R}
    $$

    Perciò $z_0$ è una singolarità eliminabile per $g$ dunque $g$ si estende a una funzione olomorfa che denotiamo ancora con $g$ su tutto $U$. Ma allora $f(z) = \dfrac{1}{g(z)} + a$ ha un polo in $z_0$ il che contraddice il fatto che $z_0$ sia una singolarità essenziale.
\end{proof}

\begin{example}
    $f(z) = e^{1/z}$ ha una singolarità essenziale in 0.
\end{example}

\begin{corollary}
    Se $z_0$ è una singolarità essenziale per $f$, $\lim_{z \to z_0} f(z)$ non esiste. Infatti, dal Teorema di Weierstrass segue che $\forall a \in \CC \; \exists z_n \to z_0$ con $\lim_{n \to \infty} f(z_n) = a$.
\end{corollary}

\section{Teorema dei residui}

Sia $D \subseteq \CC$ chiuso con $D^\circ \neq \varnothing, \Gamma = \pd D$. Diciamo che $D$ ha bordo $C^1$ a tratti se per ogni componente connessa $\Gamma_i$ di $\Gamma \; \exists \gamma_i : I_i \to \Gamma_i$ curva $C^1$ a tratti con $I_i \subseteq \R$ intervalli e $\gamma_i$ iniettiva eccetto al più che negli estremi di $I_i$ 

% (se $\Gamma_i \omeom S^1$)
\begin{example}
    $D = \R \cup \overline{B(0, 1)}$ ha $\pd D = (-\infty, -1] \cup S^1 \cup [1, +\infty)$ e perciò non ha bordo $C^1$ a tratti. $D = \overline{B(0, 1)}$ ha $\pd D = S^1$ ed ha perciò bordo $C^1$ a tratti.

    $D_R = \{ z \in \CC, |z| \leq R, \Imm(z) \geq 0\}, R > 0$ ha bordo $C^1$ a tratti.

    [TODO: Disegno]

\end{example}

Le componenti di bordo di un tale $D$ si possono orientare canonicamente come segue: diciamo $\gamma : I \to \Gamma_i$ è positiva se $\forall t_0 \in I$ tale che $\gamma'(t_0)$ sia definita, il vettore $-i\gamma'(t_0)$ punta all'estremo di $D$ in $\gamma(t_0)$.

[TODO: Disegno]

Un vettore $v$ \textqt{punta all'esterno di $D$} in $z_0 \in \pd D$ se $\exists \varepsilon > 0$ tale che $\{ t \in (-\varepsilon, \varepsilon), z_0 + t v \in D \} = (-\varepsilon, 0]$.

\begin{example}
    Le parametrizzazioni positive girano in senso antiorario (cioè lasciandosi a sinistra il dominio)

    [TODO: Tre disegnini]
\end{example}

In questi casi, se $R \subseteq D$ è un dominio con bordo $C^1, \Gamma = \pd R = \Gamma_1 \cup \dots \cup \Gamma_k$ con parametrizzazioni \textit{positive} $\gamma_i : I_i \to \Gamma_i$, e $\omega$ è una 1-forma su $D$, poniamo

$$
\int_{\gamma R} \omega = \sum_{i=1}^k \int_{\gamma_i} \omega 
$$

\textbf{Teorema dei Residui.} Sia $D \subseteq \CC$ un aperto, $f : D \setminus S \to \CC$ olomorfa, $S$ chiuso e discreto in $D$. Sia $R \subseteq D$ compatto e con bordo $C^1$ a tratti, per cui $R \cap S = \{ z_1, \dots, z_k \}$ è finito $\pd R \cap S = \varnothing$. Allora

$$
\int_{\pd R} f(z) \dd z = 2 \pi i \sum_{i=1}^k \Res(f, z_i) 
$$

\textit{Dimostrazione.}
Dimostriamo il teorema dei residui nel caso in cui $R \subseteq D$ sia una regione omeomorfa ad un disco. Perciò abbia $f$ olomorfa su $D \setminus S$, con singolarità isolate date dall'insieme $S = \{ z_1, \dots, z_k \}$

\begin{wrapfigure}[10]{o}{0.65\textwidth}
    \raisebox{0pt}[9.5\baselineskip]{
        \def\svgwidth{0.65\textwidth}
        \input{./images/residual-theorem.pdf_tex}
    }
\end{wrapfigure}

Sia $\beta = \gamma * l_k * \bar\alpha_k * \bar l_k * l_{k-1} * \bar\alpha_{k-1} * \bar l_{k-1} * \dots * l_1 * \bar\alpha_1 * \bar l_1$ dove $\alpha_i$ è una piccola circonferenza intorno a $z_i$ percorsa in senso antiorario. $\beta$ è omotopicamente banale in $R \setminus \{ z_1, \dots, z_k \}$

Poiché $f$ è olomorfa su $R \setminus \{ z_1, \dots, z_k \}$, $f(z) \dd z$ è chiusa su $R \setminus \{ z_1, \dots, z_k \}$ e poiché $\beta$ è banale 

$$
0 = \int_{\beta} f(z) \dd z = \int_{\gamma} f(z) \dd z + \sum_{i=1}^k \lrpa{ \int_{\bar{l_{i}}} f(z) \dd z + \int_{\bar{\alpha_i}} f(z) \dd z  + \int_{l_i} f(z) \dd z }
$$

dunque $\ds \int_{\gamma} f(z) \dd z = - \sum_{i=1}^k \int_{\bar{\alpha_i}} f(z) \dd z =  \sum_{i=1}^k \int_{\alpha_i} f(z) \dd z = 2 \pi i \sum_{i=1}^k \Res(f, z_i)$. Il caso in cui $R$ non sia un disco è simile, ma non verrà mai usato nelle applicazioni, per cui non lo dimostriamo. Idea: consideriamo una regione qualsiasi (disco meno dischi)

[TODO: Disegno]

$\beta$ è ancora omotopicamente banale e si ragiona come sopra. La convenzione sull'orientazione delle componenti di bordo fa sì che quella esterna sia percorsa in senso antiorario e quelle interne in senso orario così che $\beta$ sia davvero banale. $\hfill \square$

\section{Calcolo dei Residui}

Se $z_0$ è un polo semplice di $f$, allora $\ds f(z) = \frac{a_{-1}}{z - z_0} + \sum_{n \geq 0} a_n (z - z_0)^n$, per cui $(z - z_0) f(z) = a_{-1} + \sum_{n \geq 0} a_n (z - z_0)^{n+1}$ e $\Res(f, z_0) = a_{-1} = \lim_{z \to z_0} (z - z_0) f(z)$.

Dunque se $f = \dfrac{p}{q}$, $p, q$ olomorfe, $p(z_0) \neq 0$ e $z_0$ è uno zero semplice di $q$ si ha

$$
\Res(f, z_0) = \lim_{z \to z_0} \frac{(z - z_0) p(z)}{q(z)} = \frac{p(z_0)}{q'(z_0)}
$$ 

in quanto $q(z) = q(z) - q(z_0)$.

\begin{center}
    \textbf{Polo Semplice} $\ds \Rightarrow \Res\lrpa{\frac{p}{q}, z_0} = \frac{p(z_0)}{q'(z_0)}$ 
\end{center}  

\begin{example}
    $f(z) = \dfrac{e^{iz}}{z^2 + 1}$ ha singolarità in $z = \pm i$, $z_0 = i, z_1 = -i$. Tali singolarità sono poli semplici, perché $\pm i$ sono zeri semplici di $z^2 + 1 = (z + i)(z - i)$ dunque 

    $$
    \begin{aligned}
        \Res(f, z_0) &= \Res(f, i) = \frac{e^{iz}}{2z} {\bigg|}_{z=i} = \frac{e^{i\cdot i}}{2i} = - \frac{i}{2e} \\
        \Res(f, z_1) &= \Res(f, -i) = \frac{e^{i\cdot (-i)}}{2(-i)} = \frac{i \cdot e}{2}
    \end{aligned}
    $$
\end{example}

Per i poli di ordine superiore una strategia possibile è la seguente: se $f$ ha un polo di ordine $k$ in $z_0$, si pone $g(z) = (z - z_0)^k f(z)$ che si estende a una funzione olomorfa anche in $z_0$ (sempre denotata con $g$)

$$
\begin{aligned}
    f(z) &= a_{-k} (z - z_0)^k + \dots a_{-1}(z - z_0)^{-1} + \sum_{n \geq 0} a_n (z - z_0)^n \\
    g(z) &= a_{-k} + a_{-k+1}(z - z_0) + a_{-k+2}(z - z_0)^2 + \cdots \\
\end{aligned}
$$

dunque $a_{-1}$ + il coefficiente di $(z - z_0)^{k-1}$ dello sviluppo di $g$, che è olomorfa. Dunque $\Res(f, z_0) = a_{-1} = \frac{g^{(k-1)}(z_0)}{(k-1)!}$

\begin{example}
    $\ds f(z) = \frac{e^{iz}}{z(z^2+1)^2}$ ha un polo semplice in $0$ e due poli doppi (cioè di ordine $2$) in $\pm i$. Per calcolare il residuo in $i$, prendo

    $$
    \begin{aligned}
        g(z) &= (z - i)^2 f(z) = (z - i)^2 \cdot \frac{e^{iz}}{z(z + i)^2 (z - i)^2} = \frac{e^{iz}}{z(z+i)^2} \\  
        g^{(k-1)}(z) &= g'(z) = \frac{i e^{iz} \cdot z \cdot (z + i)^2 - e^{iz} [(z + i)^2] + 2z (z + i)}{z^2 (z + i)^4} \\
        &=\frac{ize^{iz}(z + i) - e^{iz} [(z + i) + 2z]}{z^2(z+i)^3}
    \end{aligned}
    $$

    che dà $\ds g'(i) = -\frac{3}{4e}$, dunque $\ds \Res(f, i) = \frac{g'(i)}{1!} = g'(i) = -\frac{3}{4e}$.
\end{example}

\subsection{Applicazioni}

\begin{examples}
    Calcolare

    \begin{itemize}
        \item $\ds \int_{-\infty}^{\infty} \frac{\cos x}{x^2 + 1} \dd x = \lim_{R \to \infty} \frac{\cos x}{x^2 + 1} \dd x$

            Per $\ds x \in \R, \frac{\cos x}{x^2 + 1} = \Re \lrpa{\frac{e^{ix}}{x^2 + 1}}$. La funzione $\ds \frac{e^{iz}}{z^2 + 1}$ ha poli semplici in $\pm i$. sia $\Omega_R$ la regione $\Omega_R = \{ z, |z| \leq R, \Imm(z) \geq 0 \}$ con $R > 1$. 

            [TODO: Disegno]

            Una parametrizzazione positiva di $\pd \Omega_R$ è $\gamma_R = \alpha_R * \beta_R$, per il teorema dei residui se $\ds f(z) = \frac{e^{iz}}{z^2 + 1}$

            $$
            \int_{\alpha_R} f(z) \dd z + \int_{\beta_R} f(z) \dd z = \int_{\pd \Omega_R} f(z) \dd z = 2 \pi i \Res(f, i) = 2\pi i \cdot \lrpa{-\frac{i}{2e}} = \frac{\pi}{e}   
            $$ 

            con $\alpha_R : [-R, R] \to \CC$ e $\alpha_R(t) = t$, $\beta_R : [0, \pi] \to \CC$ data da $\beta_R(t) = R e^{it}$

            $$
            \int_{\alpha_R} f(z) \dd z = \int_{-R}^{R} f(t) \cdot \alpha'(t) \dd t = \int_{-R}^{R} f(t) \dd t = \int_{-R}^{R} \frac{\cos t + i \sin t}{t^2 + 1} \dd t 
            $$

            la cui parte reale è l'integrale richiesto (prima di passare al limite). Dunque $\ds \lim_{R \to \infty} \int_{-R}^{R} \frac{\cos t}{1 + t^2} \dd t = \Re\lrpa{\int_{\alpha_R} f(z) \dd z} = \Re\lrpa{\int_{\pd \Omega_R} f(z) \dd z - \int_{\beta_R} f(z) \dd z} = \frac{\pi}{e} - \Re \int_{\beta_R} f(z) \dd z$

            Per concludere, basta vedere che $\ds\lim_{R \to \infty} \int_{\beta_R} f(z) \dd z = 0$ da cui

            $$
            \int_{-\infty}^{\infty} \frac{\cos t}{1 + t^2} \dd t = \frac{\pi}{e}
            $$

            Ora $\beta_R(t) = Re^{it} = R(\cos t + i \sin t)$, per cui $e^{i \beta_R(t)} = e^{iR(\cos t + i \sin t)} = e^{iR\cos t} \cdot e^{-R\sin t}$, il cui modulo è $e^{-R \sin t}$, dunque

            $$
            |f(\beta_R(t)) \beta_R'(t)| = \abs[\bigg]{\frac{e^{i \beta_R(t)}}{\beta_R(t)^2 + 1} \cdot iRe^{it}} = \frac{R e^{-R \sin t}}{R^2 e^{2 i t} + 1} \leq \frac{R}{|R^2 e^{2 i t}| + 1} \leq \frac{R}{R^2 + 1} 
            $$

            per cui

            $$
            \abs[\bigg]{\int_{\beta_R} f(z) \dd z} = \abs[\bigg]{\int_{0}^{\pi} f(\beta_R(t)) \beta'(t) \dd t} \leq \int_{0}^{\pi} \frac{R}{R^2 + 1} \dd t = \frac{R \pi}{R^2 + 1} \xrightarrow{R \to \infty} 0 
            $$

            in definitiva, $\ds \int_{-\infty}^{\infty} \frac{\cos t}{t^2 + 1} \dd t = \frac{\pi}{e}$.

        \item $\ds \int_{-\infty}^{\infty} \frac{P(x)}{Q(x)} \dd x$ dove $P, Q$ sono polinomi, $\forall x \in \R \; Q(x) \neq 0$ e $\deg Q \geq \deg P + 2$ allora possiamo usare la stessa regione $\Omega_R$ di prima, ad esempio consideriamo

            $$
            \int_{-\infty}^{\infty} \frac{1}{1 + x^4} \dd x 
            $$

            Sia $f(z) = \frac{1}{1 + z^4}$ i cui poli sono le radici quarte di $-1$ cioè

            $$
            z_0 = e^{i \frac{\pi}{4}} \qquad 
            z_1 = e^{\frac{3}{4}i \pi} \qquad 
            z_2 = e^{\frac{5}{4}i \pi} \qquad 
            z_3 = e^{\frac{7}{4}i \pi} 
            $$

            ci interessano solo $z_0$ e $z_1$ (gli unici nella regione che mi interessa), tutti i polinomi sono semplici, perciò

            $$
            \Res(f, z_i) = \frac{1}{4z_i^3} = \frac{z_i}{4 z_i^4} = - \frac{z_i}{4}
            $$

            Ragionando come sopra,

            $$
            \int_{\pd \Omega_R} f(z) \dd z = 2 \pi i (\Res(f, z_0) + \Res(f, z_1)) = 
            = 2 \pi i \lrpa{-\frac{z_0}{4} - \frac{z_1}{4}} = \text{... [TODO] ...} = \frac{\sqrt{2}}{2} \pi 
            $$

            dunque $\ds \int_{-R}^{R} \frac{1}{1 + t^4} \dd t = \int_{\alpha_R} f(z) \dd z = \frac{\sqrt{2}}{2} \pi - \int_{\beta_R} f(z) \dd z$ e come prima $\ds \lim_{R \to \infty} \int_{\beta_R} f(z) \dd z = 0$ per cui il risultato è $\ds \frac{\sqrt 2}{2} \pi$.
    \end{itemize}
\end{examples}

\textbf{Attenzione.} Nel primo esempio per \textqt{complessificare} $\ds \frac{\cos x}{x^2 + 1}$ si sarebbe potuto prendere $\ds \frac{\cos z}{z^2 + 1}$. Il problema è che $\ds\abs[\bigg]{\frac{\cos z}{z^2 + 1}}$ non è stimabile facilmente lungo $\beta_R$, per cui $\ds\int_{\beta_R} \frac{\cos z}{z^2 + 1} \dd z \not\to 0$ per $R \to \infty$ (notando che $\cos(i \cdot t) = \cosh t$). Dunque è meglio prendere $\cos t = \Re(e^{it})$.

\begin{example}
    Consideriamo $\ds \int_{0}^{\infty} \frac{1}{1 + x^n} \dd x, n \geq 2$. Se $n$ è pari si può fare come sopra. Se $n$ è dispari ho un polo in $z_0 = -1$ che dà fastidio.

    Prendiamo questa volta $D_R = \{ z \in \CC, |z| \leq R, 0 \leq \arg(z) \leq \frac{2\pi}{n} \}$. Se $\ds \frac{1}{1 + z^n}$, i poli di $f$ sono tutti semplici e sono le radici $n$-esime di $-1$, perciò $z_k = e^{i\lrpa{\frac{\pi}{n} + k \frac{2\pi}{n}}}, k = 0, \dots, n - 1$.

    [TODO: Disegno regione per $n = 8$]

    Nella regione $D_R$ cade solo il polo $z_0 = e^{\frac{i\pi}{n}}$. $\ds \Res(f, z_0) = \Res\lrpa{\frac{1}{1 + z^n}, z_0} = \frac{1}{n z_0^{n-1}} = \frac{z_0}{n z_0^n} = - \frac{z_0}{n} = -\frac{e^{\frac{i\pi}{n}}}{n}$. 

    Consideriamo le seguenti

    $$
    \begin{aligned}
        \alpha_R : [0, R] \to \CC \text{ data da } & \alpha_R(t) = t \\
        \gamma_R : \lrbt{0, \frac{2\pi}{n}} \to \CC \text{ data da } & \gamma_R(t) = R e^{it} \\
        \beta_R : [0, R] \to \CC \text{ data da } & \beta_R(t) = t e^{i \frac{2\pi}{n}} \\
    \end{aligned}
    $$

    allora una parametrizzazione positiva di $\pd D_R$ è $\alpha_R * \gamma_R * \bar\beta_R$

    [TODO: Altro disegno]

    dunque 

    $$
    \begin{aligned}
        \int_{\alpha_R} f(z) \dd z + \int_{\gamma_R} f(z) \dd z - \int_{\beta_R} f(z) \dd z &= 2 \pi i \lrpa{- \frac{e^{i\frac{\pi}{n}}}{n}} = \frac{2\pi i}{n} \lrpa{-\cos \frac{\pi}{n} - i \sin \frac{\pi}{n}} = \\
        & \frac{2\pi}{n} \lrpa{\sin \frac{\pi}{n} - i \cos \frac{\pi}{n}}
    \end{aligned}
    $$

\end{example}







\end{document}
