\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 24 Febbraio 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\section{Teoria dei Rivestimenti}

A primo impatto la teoria sui rivestimenti potrebbe non sembrare collegata alla teoria sull'omotopia. Vedremo più avanti alcuni teoremi che collegano la teoria sui rivestimenti a quella sull'omotopia e sul $\pi_1$.

\begin{definition}
    $f : X \to Y$ è un \textbf{omeomorfismo locale} se $\forall x \in X \; \exists U \ni x$ aperto in $X$ e $V \ni f(x)$ tale che $f(U) = V$ e $f {|}_{U} : U \to V$ è un omeomorfismo. 
\end{definition}

\begin{facts} \ 
    \begin{itemize}
        \item Un omeomorfismo locale è una mappa aperta. 

            Infatti se $\Omega \subseteq X$ è aperto, $\forall x \in X$ seleziono un aperto $U_x \ni x$ come nella definizione di omeomorfismo locale e

            $$
            f(\Omega) = f \left( \bigcup_{x \in X} (\Omega \cap U_x) \right) = \bigcup_{x \in X} f(\Omega \cap U_x)
            $$

            è aperto in $Y$ (in quanto $\Omega \cap U_x$ è aperto in aperto).

        \item $f$ omeomorfismo locale $\implies \forall y \in Y \; f^{-1}(y)$ è discreto (non necessariamente chiuso). 

            Dato $x \in f^{-1}(y)$ per definizione di omeomorfismo locale $\exists U \subseteq X$ aperto con $x \in U$ e $f {|}_U$ iniettiva $\implies f^{-1}(y) \cap U = \{ x \}$

    \end{itemize}
\end{facts}

\begin{definition}
    Una mappa continua $p : E \to X$ è un \textbf{rivestimento} se $X$ è connesso per archi e $\forall x \in X \; \exists U$ aperto di $X$, $x \in U$ tale che 

    $$
    p^{-1}(U) = \bigsqcup_{i \in I} V_i
    $$

    con $V_i$ aperto in $E$ e $\forall i \in I \; p {|}_{V_i} : V_i \to U$ omeomorfismo. ($I \neq \varnothing$)
\end{definition}

\begin{facts} \ 
    \begin{itemize}
        \item Un rivestimento è un omeomorfismo locale.
        \item Un rivestimento è surgettivo
        \item Un intorno $U$ come nella definizione si dice \textbf{ben rivestito}.        
    \end{itemize}    
\end{facts}

\begin{examples} \ 
    \begin{itemize}
        \item L'esempio archetipico di rivestimento è quello di $S^1$ con $\R$ ovvero $p : \R \to S^1, p(t) = (\cos 2 \pi t, \sin 2 \pi t)$ allora $p$ è un rivestimento.

            [TODO: Disegnino]

            Dato $x_0 \in S^1 \; x_0 = p(t_0)$ per qualche $t_0 \in \R$. Scelgo $U = S^1 \setminus \{ -x_0 \}$ e ho

                $$
                p^{-1}(U) = \bigsqcup_{k \in \Z} (t_0 - \frac{1}{2} + k, t_0 + \frac{1}{2} + k)
                $$

            e $\forall k \in \Z \; p{|}_{(t_0 - \frac{1}{2} + k, t_0 + \frac{1}{2} + k)} : (t_0 - \frac{1}{2} + k, t_0 + \frac{1}{2} + k) \to U$ è un omeomorfismo.

        \item $p : (-1, 1) \to S^1, p(t) = (\cos 2 \pi t, \sin 2 \pi t)$ non è un rivestimento poiché per $k \in \Z \; p(k) = (1, 0) \in S^1$ dunque la mappa è surgettiva, in particolare è un omeomorfismo locale surgettivo ma non è un rivestimento.

            Infatti $(1, 0) \in S^1$ non ha intorni ben rivestiti.

        \item $\pi : \sfrac{\R \times \{ -1, 1 \}}{\sim} \to \R$ con $(x, \varepsilon) \sim (y, \varepsilon') \iff (x, \varepsilon) = (y, \varepsilon) \lor (x = y \land x \neq 0)$ (l'esempio della retta reale con un punto doppio, abbiamo già visto in passato che è un esempio di spazio localmente euclideo ma non T2).

            $\pi([(x, \varepsilon)]) = x$, $\pi$ è un omeomorfismo localmente surgettivo e non è un rivestimento ($0 \in \R$ non ha intorni ben rivestiti).

        \item $\pi : S^n \to \P^n(\R), \pi(x) = [x]$ è un rivestimento.
    \end{itemize}
\end{examples}

Vediamo ora come se abbiamo un diagramma come segue

$$
\xymatrix{
    & E \ar[d]^p \\
    Y \ar@{~>}[ru]^{\tilde f} \ar[r]_f & X \\
}
$$

ci possiamo chiedere quando è possibile sollevare $Y$ a $E$ tramite una $\tilde f$. Per prima cosa vediamo che se tali sollevamenti esistono e che coincidono in un punto allora sono identici.

\begin{prop}
    $p : E \to X$ rivestimento, $Y$ connesso per archi, $f : Y \to X$. Siano $\tilde f, \tilde g : Y \to E$ (dette \textqt{sollevamenti} di $Y$ su $E$) con $p \compose \tilde f = p \compose \tilde g = f$. Se $\exists y_0 \in Y$ con $\tilde f(y_0) = \tilde g(y_0) \implies \tilde f = \tilde g$.

    \begin{proof} 
        Basta vedere che $\Omega = \{ y \in Y \mid \tilde f(y) = \tilde g(y) \} \subseteq E$ è aperto e chiuso.

        \begin{itemize}
            \item[$\boxed{\Omega \text{ aperto}}$] Sia $y \in \Omega$ allora $\tilde f(y) = \tilde g(y) = \tilde x_0$. Sia $x_0 = p(\tilde x_0) = f(y)$ e sia $U$ un intorno di $x_0$ ben rivestito. $p^{-1}(U) = \bigsqcup_{i \in I} V_i$ dunque consideriamo $i_0 \in I$ tale che $\tilde x_0 \in V_{i_0}$.

                $\tilde f, \tilde g$ continue $\implies \exists$ intorno $W$ di $y$ in $Y$ tale che $\tilde f(W) \subseteq V_{i_0}, \tilde g(W) \subseteq V_{i_0}$.

                Ora $p {|}_{V_{i_0}}$ è iniettiva dunque, da $p \compose \tilde f = p \compose \tilde g$, deduco $\tilde f {|}_W = \tilde g {|}_W$, perciò $W \subseteq \Omega$ e $\Omega$ è aperto.

            \item[$\boxed{\Omega \text{ chiuso}}$] Vediamo che $\Omega^\complement = Y \setminus \Omega$ è aperto. Sia $y \in \Omega^\complement$ per cui $\tilde f(y) \neq \tilde g(y)$. 

                Tuttavia $p \compose \tilde f(y) = p \compose \tilde g(y) = f(y) = x_0$. Se $U$ è un intorno ben rivestito di $x_0$, $p^{-1}(U) = \bigsqcup_{i \in I} V_i$ e da $\tilde f(y) = \tilde g(y)$ deduco $\tilde f(y) \in V_{i_0}, \tilde g(V_{i_1})$ con $i_0 \neq i_1$. 

                Dunque per continuità $\exists W$ intorno di $y$ con $\tilde f(W) \subseteq V_{i_0}, \tilde g(W) \subseteq V_{i_1}$ poiché $V_{i_0} \cap V_{i_1} = \varnothing$. $\forall z \in W \; \tilde f(z) \neq \tilde g(z) \implies W \subseteq \Omega^\complement$ e $\Omega^\complement$ è aperto.
        \end{itemize}
    \end{proof}
\end{prop}

\begin{theorem}
    $p : E \to X$ rivestimento, $\gamma : [0, 1] \to X$ cammino continuo, siano $x_0 = \gamma(0)$ e $\tilde x_0 \in p^{-1}(x_0) \implies \exists \gamma : [0, 1] \to E$ continuo con $\tilde\gamma(0) = \tilde x_0$ e $p \compose \tilde\gamma = \gamma$

    \begin{proof}
        L'unicità di $\tilde\gamma$ segue da quanto visto prima. Ricopriamo $X$ con aperti ben rivestiti $\{ U_i, i \in I \}$ e sia $\varepsilon > 0$ un numero di Lebesgue per il ricoprimento $\{ \gamma^{-1}(U_i), i \in I \}$ di $[0, 1]$. Perciò se $\frac{1}{n} < \varepsilon$ allora

        $$
        \forall k = 0, \dots, n - 1 \qquad \gamma \left( \left[ \frac{k}{n}, \frac{k+1}{n} \right] \right) \subseteq U_k
        $$

        è ben rivestito. 

        [TODO: Disegnino]

        Definisco induttivamente $\tilde\gamma$ su $\left[ \frac{k}{n}, \frac{k+1}{n} \right]$ come segue. Per definizione di rivestimento $\exists V_0$ aperto di $E$ tale che $\tilde x_0 \in V_0$ e $p_0 = p{|}_{V_0} : V_0 \to U_0$ è omeomorfismo.

        $\forall t \in \left[ 0, \frac{1}{n} \right]$ pongo $\tilde\gamma(t) = p_0^{-1}(\gamma(t))$ (ricordiamo che $\gamma\left(\left[0, \frac{1}{n}\right]\right) \subseteq U_0$). Una volta definito $\tilde\gamma$ continuo su $\left[ 0, \frac{k}{n} \right]$ trovo $V_k \subseteq E$ tale che $\tilde\gamma\left(\frac{k}{n}\right) \in V_k$ e $p_k = p{|}_{V_k} : V_k \to U_k$ omeomorfismo e pongo $\forall t \in \left[\frac{k}{n}, \frac{k+1}{n}\right] \; \tilde\gamma(t) = p_k^{-1}(\gamma(t))$. Essendo giunzione di cammini continui $\tilde\gamma$ è continuo e quando $k$ arriva a $n - 1$ ho finito.
    \end{proof}
\end{theorem}

\end{document}
