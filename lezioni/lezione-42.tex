\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 16 Marzo 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\begin{theorem}
    $p : E \to X$ regolare connesso $\implies \forall \tilde x \in E$ posto $x = p(\tilde x)$ vale

    $$
    \Aut(E) \simeq \frac{\pi_1(x, x)}{p_*(\pi_1(E, \tilde x))}
    $$

    \begin{proof}
        ricordiamo che $p_*(\pi_1(E, \tilde x)) \lhd \pi_1(X, x)$, dunque in effetti il quoziente è un gruppo. Sia $\theta : \pi_1(X, x) \to \Aut(E)$ tale che $\forall \alpha \in \pi_1(X, x) \quad \theta(\alpha)$ sia l'unico elemento di $\Aut(E)$ tale che $\theta(\alpha)(\tilde x) = \tilde x \cdot \alpha$. 

        Tale $\theta(\alpha)$ esiste poiché il rivestimento è regolare ed è unico perché $\Aut(E)$ agisce liberamente su $E$.

        \begin{itemize}
            \item $\theta$ è un omomorfismo

                Basta vedere quanto segue

                $$
                (\theta(\alpha_1) \compose \theta(\alpha_2)) (\tilde x) 
                = \theta(\alpha_1) (\theta(\alpha_2) (x)) 
                = \theta(\alpha_1) (\tilde x \cdot \alpha_2) 
                = (\theta(\alpha_1)(\tilde x)) \cdot \alpha_2
                = (\tilde x \cdot \alpha_1) \cdot \alpha_2 = \tilde x \cdot (\alpha_1 * \alpha_2)
                $$

                (usando che l'azione di monodromia e l'azione sugli automorfismi commutano per quanto visto in un lemma precedente) e dunque $\theta(\alpha_1) \theta(\alpha_2) = \theta(\alpha_1 * \alpha_2)$.

            \item $\theta$ è surgettivo

                Se $\varphi \in \Aut(E)$, poiché l'azione di monodromia è transitiva, $\exists \alpha \in \pi_1(X, x)$ con $\tilde \cdot \alpha = \varphi(\tilde x)$. Dunque $\theta(\alpha) = \varphi$ in quanto $\theta(\alpha)$ e $\varphi$ coincidono su $\tilde x$ e sono perciò uguali (segue perché $\Aut(E)$ agiscono liberamente). 

            \item $\Ker \theta = p_*(\pi_1(E, \tilde x))$. 

                Infatti, poiché $\Aut(E)$ agisce liberamente $\theta(\alpha) = \Id \iff \theta(\alpha)(\tilde x) \iff \tilde x \cdot \alpha = \tilde x \iff \alpha \in p_*(\pi_1(E, \tilde x))$ [...]
        \end{itemize}
    \end{proof}
\end{theorem}

\begin{corollary}
    $p : E \to X$ regolare $\implies X \simeq \sfrac{E}{\Aut(E)}$

    \begin{proof}
        Essendo aperta e surgettiva $p : E \to X$ è un'identificazione per cui $X \simeq \sfrac{E}{\sim}$, dove $\tilde x \sim \tilde y \iff p(\tilde x) = p(\tilde y) \iff \exists \varphi \in \Aut(E) \quad \tilde x = \varphi(\tilde y)$ [...] agisce transitivamente sulle fibre.
    \end{proof}
\end{corollary}

Vale anche una sorta di viceversa

\begin{prop}
    Se $G$ agisce in maniera propriamente discontinua su uno spazio connesso $E$, allora la proiezione $p : E \to \sfrac{E}{G}$ è un rivestimento regolare, e $G = \Aut(E, p)$.

    \begin{proof}
        Sia $x \in \sfrac{E}{G}$, ne devo costruire un intorno ben rivestito. Scelgo $\tilde x \in E$ con $p(\tilde x) = x$, per definizione di azione propriamente discontinua $\exists$ aperto $V$ di $E$ che contiene $\tilde x$ e tale che $\forall \gamma \in G \setminus \{ \Id \} \quad \gamma(V) \cap V = \varnothing$. Pongo $U = p(V)$, le proiezioni al quoziente per azioni di gruppo sono aperte, per cui $U$ è aperto. 

        Per costruzione, $p^{-1}(U) = \bigcup_{\gamma \in G} \gamma(V)$. Ogni $\gamma(V)$ è aperto ($\gamma$ è un omeomorfismo) e l'unione è disgiunta poiché $\gamma_1(V) \cap \gamma_2(V) \neq \varnothing \implies V \cap (\gamma_1^{-1} \gamma_2)(V) \neq \varnothing \implies \gamma_1^{-1} \gamma_2 = 1 \implies \gamma_1 = \gamma_2$. 

        Infine $\forall \gamma \in G \quad p{|}_{\gamma(V)}$ è un omeomorfismo su $U$ (è continua aperta, surgettiva, l'iniettività segue ancora dal fatto che $\forall \gamma \neq 1 \quad V \cap \gamma(V) = \varnothing$).
    \end{proof}
\end{prop}

\begin{observation}
    Un rivestimento universale è sempre regolare infatti $p_*(\{ 1 \}) = \{ 1 \} \lhd \pi_1(X, x)$
\end{observation}

\begin{corollary}
    Sia $p : E \to X$ un rivestimento universale $\implies \Aut(E) \simeq \pi_1(X, x)$
\end{corollary}

\begin{corollary}
    $E$ semplicemente connesso e $G$ agisce in maniera propriamente discontinua $\implies \pi_1 \left( \sfrac{E}{G} \right) \simeq G$ (dunque $G$ è proprio del rivestimento universale).
\end{corollary}

\begin{examples} \ 
    \begin{itemize}
        
        \item $S^1 \simeq \sfrac{\R}{\Z}$, e poiché $\R$ è semplicemente connesso $\implies \pi_1(S^1) = \Z$
        
        \item $\underbrace{S^1 \times \cdots \times S^1}_{ n \text{ volte}} = \sfrac{\R^n}{\Z^n} \implies \pi_1((S^1)^n) = \Z^n$ in quanto anche $\R^n$ è semplicemente connesso.

        \item $\P^n(\R) = \sfrac{S^n}{G}, G = \{ \pm \Id \}$ dunque se $n \geq 2$ allora $\pi_1(\P^n(\R)) \simeq G = \sfrac{\Z}{2\Z}$

    \end{itemize}
\end{examples}

\section{Rivestimenti Universali}

Possiamo chiederci, ci saranno sempre rivestimenti universali di uno spaizo? Per certi spazi la risposta è affermetiva, vediamo la seguente definizione

\begin{definition}
    Sia $X$ uno spazio topologico localmente connesso per archi allora $X$ si dice \textbf{semilocalmente semplicemente connesso} se $\forall x \in X \; \exists U \subseteq X$ aperto con $x \in U$ tale che l'inclusione $i : U \to X$ induce il morfismo banale $i_* : \pi_1(U, x) \to \pi_1(X, x)$.
    
    Ovvero $\forall x \in X \exists$ intorno $U$ di $x$ tale che tutti i lacci basati in $X$ e contenuti in $U$ sono banali in $X$. 

    Questa proprietà è verificata ad esempio se ogni punti ha un intorno semplicemente connesso (ad esempio le varietà)
\end{definition}

\begin{observation}
    Se $X$ ammette un rivestimento universale $\implies$ è semilocalmente semplicemente connesso. Infatti, dato $x \in X$ ne posso prendere un intorno connesso per archi ben rivestito $U$. Se $p : E \to X$ è il rivestimento universale e $V \subseteq p^{-1}(U)$ è un aperto con $p {|}_V : V \to U$ omeomorfismo

    $$
    \begin{tikzcd}
        V \arrow[hook]{r}{j} \arrow{d}{\omeom} & 
        E \arrow{d}{p} \\
        U \arrow[hook]{r}{i} \arrow[bend left=45, dashed]{u}{\exists s} & 
        X \\
    \end{tikzcd}
    $$

    $i = p \compose j \compose s \implies i_* = p_* \compose j_* \compose s_*$ ma $j_*$ è banale perché $\pi_1(E) \simeq \{ 1 \}$ dunque $i_* : \pi_1(U, x) \to \pi_1(X, x)$ è banale.
\end{observation}

\begin{lemma}
    Quando esiste il rivestimento universale, è unico a meno di isomorfismo.

    \begin{proof}
        Siano $p_1 : E_1 \to X$ e $p_2 : E_2 \to X$ rivestimenti universali di $X$. Consideriamo, poiché $E_1$ è semplicemente connesso, il diagramma

        $$
        \begin{tikzcd}
            E_1 \arrow[swap]{rd}{p_1} \arrow[dashed]{rr}{\varphi} && 
            E_2 \arrow{ld}{p_2} \\ 
            & X & \\
        \end{tikzcd}
        $$

        che si completa con la freccia $\varphi : E_1 \to E_2$ tale che $p_2 \compose \varphi = p_1$. Fissati $\tilde x_1 \in p_1^{-1}(x), \tilde x_2 \in p_2^{-1}(x)$, posso anche richiedere $\varphi(\tilde x_1) = \tilde x_2$. Analogamente $\exists \psi : E_2 \to E_1$ con $p_1 \compose \psi = p_2$ e $\psi(\tilde x_2) = \tilde x_1$. Segue facilmente che $\varphi, \psi$ sono isomorfismi uno inverso dell'altro.
    \end{proof}
\end{lemma}
    
\begin{theorem}
    Sia $X$ uno spazio topologico localmente connesso per archi e connesso. Allora $X$ ammette un rivestimento universale $\iff X$ è semilocalmente semplicemente connesso.

    Inoltre in tale caso il rivestimento universale è unico.

    \begin{proof}
        Non vediamo nel dettaglio questa dimostrazione però ricapitoliamo quanto già provato.

        \begin{itemize}
            \itemRarr Già vista
            \item[$\boxed{\text{\footnotesize Unicità}}$] Già vista
            \itemLarr E' una dimostrazione abbastanza tecnica però vediamone uno sketch:

                Fissiamo $x \in X$ e definiamo $E$ come segue

                $$
                E = \snfrac{\lrpa{\bigcup_{y \in X} \Omega(x, y)}}{\sim}
                $$

                dove $\gamma_1 \sim \gamma_2 \iff$ sono omotopi a estremi fissi.

                Si topologizza $\bigcup_{y \in X} \Omega(x, y)$ con la topologia compatta-aperta (se $X$ è metrico è la topologia della convergenza uniforme), si dota $E$ della topologia quoziente e si defnisce $p : E \to X$ con $p([\gamma]) = \gamma(1)$. Bisgona poi verificare che $E$ è semplicemente connesso e che $p$ è un rivestimento.
        \end{itemize}
    \end{proof}  
\end{theorem}

\newcommand\dtilde[1]{\tilde{\tilde{#1}}} 

\begin{theorem}
    Sia $X$ connesso e semilocalmente semplicemente connesso, $x \in X \implies \forall H < \pi_1(X, x) \; \exists$ rivestimento $p : E \to X$ tale che $p_*(\pi_1(E, \tilde x)) = H$, dove $\tilde x$ è un punto di $p^{-1}(x)$. Tale rivestimento è unico a meno di isomorfismo.

    \begin{proof}
        Sia $p : \tilde X \to X$ il rivestimento universale di $X$ e fissiamo $\dtilde x in p^{-1}(x)$. Abbiamo un isomorfismo $\pi_1(X, x) = \Aut(\tilde X)$ e denoto con $H$ anche il sottogruppo di $\Aut(\tilde X)$ corrispondente ad $H < \pi_1(X, x)$, pongo $E = \sfrac{\tilde X}{H}$

        $$
        \begin{tikzcd}
            \tilde X \ar{r} \ar[bend right=20, swap]{rr}{q} & \sfrac{\tilde X}{H} = E \ar{r}{p} & \snfrac{\tilde X}{\pi_1(X, x)} = X
        \end{tikzcd}
        $$

        si verifica facilmente che $p$ è un rivestimento (un aperto di $X$ ben rivestito rispetto a $q$ lo è anche rispetto a $p$). Inoltre, se $\tilde x = \pi_1(\dtilde x) \in E$ abbiamo $p_*(\pi_1(E, \tilde x)) = \opn{Stab}(\tilde x)$ rispetto all'azione di monodromia.

        [TODO: Disegnino?]

        Dato $\alpha \in \pi_1(X, x), \alpha = [\gamma]$, siano $\tilde \gamma, \dtilde \gamma$ i sollevamenti di $\gamma$ in $E$, $\tilde X$ a partire da $\tilde x, \dtilde \gamma$, così che $\tilde \gamma = \pi \compose \dtilde \gamma$. Ora $\tilde \cdot \alpha = \tilde \gamma(1) = \pi(\dtilde \gamma(1))$ che è uguale a $\tilde x \iff \dtilde \gamma(1)$ è equivalente a $\dtilde x$ tramite l'azione di $H$, che equivale alla tesi.
    \end{proof}
\end{theorem}

D'ora in poi ha senso parlare "del" rivestimento di uno spazio $X$ e lo indicheremo sempre con $\tilde X$

\textbf{Esempi.} 
$X = S^1 \lor S^1$, chi è $\tilde X = $ ?, disegniamolo: si parte portando nel rivestimento il punto di partenza $x_0$, poi avremo quattro segmenti che partono da esso (corrispondenti a fare $a, b, a^{-1}$ o $b^{-1}$), se consideriamo ora un singolo segmento questo, prima o poi, si ricongiungerà nuovamente in $x_0$ in $X$ dunque troviamo un'altro punto della fibra di $x_0$ nel rivestimento e proseguiamo iterativamente (alla fine verra un frattale come in figura).

\begin{center}
    \def\svgwidth{0.65\textwidth}
    \input{./images/riv-univ-s1s1.pdf_tex}
\end{center} 

E' un cosìdetto \textit{albero quadrivalente infinito}, i segmenti orizzontali si proiettano su $a$ e quelli verticali su $b$. Ci possiamo chiedere chi sia il rivestimento di $X$ associato a $H = \present{ a }$?

\begin{wrapfigure}[10]{l}{0.225\textwidth}
    \raisebox{0pt}[13\baselineskip]{
        \def\svgwidth{0.225\textwidth}
        \input{./images/riv-univ-s1s1-2.pdf_tex}
    }    
\end{wrapfigure}

Cerchiamo un rivestimento tale che $p : E \to X$ con $p_*(\pi_1(E, \tilde x)) = \present{ a }$, il grafo risultante non è omogeneo ovvero se $\varphi : E \to E$ è omeomorfismo allora vale $\varphi(\text{loop}) = \text{loop} \implies \varphi(\tilde x_0) = \tilde x_0 \implies \Aut(E) = \{ 1 \}$.

In effetti $\present{a}$ è molto lontano dall'essere normale in $\pi_1(X, x) \simeq \Z * \Z$. Se $N = N(a)$ è il sottogruppo normale generato in $\pi_1(X)$ il rivestimento associato a $N$ come sarà fatto?

\begin{wrapfigure}[13]{r}{0.225\textwidth}
    \raisebox{0pt}[15\baselineskip]{
        \def\svgwidth{0.275\textwidth}
        \input{./images/riv-univ-s1s1-3.pdf_tex}
    }
\end{wrapfigure}

$N$ normale $\implies$ il rivestimento associato $p : E \to X$ sarà regolare e quindi

$$
\Aut(E) = \snfrac{\Z * \Z}{N} = \snfrac{\pi_1(X, x)}{\pi_*(E, \tilde x)}
$$

$$
\implies \snfrac{\Z * \Z}{N} = \prerel{ a, b }{ a } \simeq \Z
$$

attraverso il morfismo $\varphi$ 

$$
\begin{array}{rccc}
    \varphi : & \Z * \Z & \longrightarrow & \Z \\
    & a & \longmapsto & 0 \\
    & b & \longmapsto & 1 \\
\end{array}
$$

\section{$\pi_1$ di grafi finiti (connessi)}


Ci teniamo un po' sul vago sulla definizione di grafo (per la formalizzazione si procede come per i wedge di circonferenze, solo che si considerano tante copie di $[0, 1]$ quanti sono i lati e le si quozienta opportunamente tra loro in $0, 1$).

Diciamo che $\Gamma$ è un \textbf{grafo} connesso finito, avrà un insieme di vertici $V$ ed un insieme $E$ di lati. Diciamo che un grafo è un \textbf{albero} se non contiene cicli ovvero loop iniettivi.

\begin{center}
    % \def\svgwidth{0.65\textwidth}
    \input{./images/graph-1.pdf_tex}
\end{center}

Elenchiamo ora alcuni fatti sui grafi

\begin{facts} \ 
    \begin{itemize}
        
        \item Un albero è contraibile (si mostra per induzione sul numero di vertici, se è un albero deve avere almeno un vertice libero e si può retrarre per deformazione il lato che lo contiene sul resto dell'albero, ragionando poi per induzione).

        \item Se $\Gamma$ è un albero $V - E = 1$ (sempre per induzione ogni retrazione toglie un vertice ed un lato e si finisce con un solo vertice e zero lati)
        
        \item $\Gamma$ connesso $\implies \Gamma$ contiene un albero massimale $\Gamma'$ e $\Gamma'$ contiene tutti i vertici di $\Gamma \implies \Gamma = \Gamma' \cup \{ \text{\textqt{qualche lato}} \}$, a livello algoritmico si parte da un vertice a caso e si aggiungono man mano lati senza creare cicli.

            [TODO: Disegnino estrazione di un albero massimale da un grafo finito]
        
        \item Sia $\chi(\Gamma) :=$ caratteristica di Eulero di $\Gamma = V - E$

            Se $\Gamma' \subseteq \Gamma$ è un albero massimale, $\chi(\Gamma') = 1 \implies \Gamma = \Gamma' \cup \{ (1 - \chi(\Gamma)) \text{ lati} \}$.

            Infatti se $V, E$ sono vertici e lati di $\Gamma$, $V', E'$ sono vertici di $\Gamma' \implies V' - E' = 1, V - E = \chi(\Gamma)$ e $V = V'$

            $$
            \implies E - E' = (V' - E') - (V - E) = 1 - \chi(\Gamma)
            $$
        
        \item Dunque $\Gamma$ è ottenuto da un albero massimale aggiungendo $1 - \chi(\Gamma)$ lati. Usando induttivamente Van Kampen otteniamo il seguente risultato.
    \end{itemize}
\end{facts}

\begin{theorem}
    $\pi_1(\Gamma) \simeq F_{1 - \chi(\Gamma)}$
\end{theorem}

\begin{theorem}
    $F$ gruppo libero su $n$ generatori $H < F$ sottogruppo di indice $k \implies H$ gruppo libero su $k(n - 1) + 1$ generatori (in particolare il rango di $H$ è spesso maggiore di quello di $F$).

    \begin{proof}
        Per questa dimostrazione si sfrutta la teoria sui grafi che abbiamo appena finito di enunciare.

        $F = \pi_1(\Gamma), \chi(\Gamma) = 1 - n$. Per il teorema appena visto sia $\tilde \Gamma$ il rivestimento di $\Gamma$ associato ad $H$. Poiché vertici e lati sono semplicemente connessi, se $V, E$ sono vertici e lati di $\Gamma$ allora i vertici di $\tilde \Gamma$ sono $k V, k E$ in quanto $\deg(\text{rivestimento}) = [F : H] = k$.

        $$
        \implies H = \pi_1(\tilde\Gamma) = 1 - \chi(\tilde\Gamma) = 1 - k \chi(\Gamma) = 1 - k(1 - n) = 1 + k(n - 1)
        $$
    \end{proof}
\end{theorem}

\end{document}
