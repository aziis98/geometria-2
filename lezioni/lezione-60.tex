\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 4 Maggio 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

Sia $J$ un sottoinsieme non vuoto di $\P(V)$ e sia $\mc F = \{ \P(W) \subset \P(V) \mid J \subseteq \P(W) \}$ (famiglia di tutti i sottospazi proiettivi che contengono $J$). Allora definiamo il sottospazio proiettivo associato a $J$:


$$
L(J) := \bigcap_{\P(W) \in \mc F} \P(W)
$$


cioè $L(J)$ è il più piccolo sottospazio proiettivo associato a $J$.

\begin{observation}
    Per $S \subseteq \P(V)$ si ha che: $L(S) = S \iff S = \P(T)$ per un qualche sottospazio vettoriale $T \subseteq V$
\end{observation}

\textbf{Notazione.} $J = \{ P_1, \dots, P_t \} =$ insieme finito di punti, per notazione poniamo

$$
L(J) = L(P_1, \dots, P_t)
$$

\begin{observation}
    Sia $P_i = [v_i]$ per $i = 1, \dots, t$. Allora $L(P_1, \dots, P_t) = \P(\langle v_1, \dots, v_t \rangle) \implies \dim \langle v_1, \dots, v_t \rangle \leq t \implies \dim L(P_1, \dots, P_t) \leq t - 1$.
\end{observation}

\begin{definition}
    Assumiamo che $P_i = [v_i]$ per $i = 1, \dots, t$, allora diciamo che i punti $P_1, \dots, P_t$ sono \textbf{linearmente indipendenti} se i vettori $v_1, \dots, v_t$ sono \textit{linearmente indipendenti} (cioè se $\dim L(P_1, \dots, P_t) = t - 1$). Altrimenti, diciamo che i punti $P_1, \dots, P_t$ sono \textbf{linearmente dipendenti}.
\end{definition}

\begin{observation}
    Siano $P = [v], Q = [w]$ due punti di $\P(V)$. Allora $P, Q$ sono linearmente indipendenti $\iff P$ e $Q$ sono distinti.

    Infatti, $P = [v] \neq Q = [w] \iff \nexists \lambda \in \K^*$ tale che $w = \lambda v \iff \{ v, w \}$ sono linearmente indipendenti.
\end{observation}

\begin{observation}
    Se $P \neq Q \implies L(P, Q)$ è una retta $\implies$ dati due punti distinti $P, Q$ per essi passa una e una sola retta, cioè $L(P, Q)$.
\end{observation}

\begin{observation}
    Siano ora $P, Q, R \in \P(V)$ distinti tra loro, allora $P, Q, R$ sono linearmente indipendenti $\iff P, Q, R$ non sono allineati (cioè non giacciono su una stessa retta).

    In questo caso $L(P, Q, R)$ è un piano ed è l'unico piano che contiene $P, Q, R$. 
\end{observation}

\begin{example}
    In $\P^2(\R)$ consideriamo i punti

    $$
    \textstyle
    P = [\frac{1}{2}, 1, 1] 
    \qquad Q = [1, \frac{1}{3}, \frac{4}{3}] 
    \qquad R = [2, -1, 2] 
    $$

    $\implies P, Q, R$ sono allineati.
\end{example}

\begin{observation}
    $P_1, \dots, P_t \in \P(V)$. Se $P_1, \dots, P_t$ sono linearmente indipendenti (per definizione $v_1, \dots, v_t$ sono linearmente indipendenti) $\implies t \leq \dim V = n + 1$.
\end{observation}

\begin{definition}
    Siano $P_1, \dots, P_t \in \P(V)$ dei punti, allora diciamo che essi sono in \textbf{posizione generale} se

    \begin{itemize}
        \item Se $t \leq n + 1$: se essi sono linearmente indipendenti.
        \item Se $t > n + 1$: se per ogni possibile scelta di $n + 1$ punti tra essi, otteniamo un sottoinsieme costituito da punti linearmente indipendenti.
    \end{itemize}
\end{definition}

\begin{observation}
    Se $P_1, \dots, P_t$ sono in \textit{posizione generale} e $t > n + 1 \implies L(P_1, \dots, P_t) = \P(V)$.
\end{observation}

\begin{example}
    Sia $e_0, \dots, e_n$ un riferimento proiettivo di $\P(V)$. Allora

    $$
    F_0 = [e_0], F_1 = [e_1], \dots, F_n = [e_n], U = [e_0 + \cdots + e_n]
    $$

    (in questo caso $t = n + 2$) sono in \textit{posizione generale}.
\end{example}

\begin{lemma}
    Sia $V$ uno spazio vettoriale di dimensione $n + 1$. Siano $P_0, \dots, P_{n+1} \in \P(V)$ dei punti in posizione generale. Allora $\{ P_0, \dots, P_{n+1} \}$ definisce un riferimento proiettivo $e_0, \dots, e_n$ per cui

    \begin{itemize}
        \item $P_0, \dots, P_n$ sono i suoi punti fondamentali.
        \item $P_{n+1}$ è il suo punto unità (ovvero $P_{n+1} = [e_0 + \dots + e_n]$).
    \end{itemize}

    \begin{proof}
        Assumiamo che $P_i = [v_i]$ per $i = 0, \dots, n+1$. Per ipotesi $v_0, \dots, v_n$ sono vettori linearmente indipendenti. di conseguenza $\exists a_0, \dots, a_n \in \K$ tali che $v_{n+1} = a_0 v_0 + \dots + a_n v_n$. Osserviamo che $\forall i = 0, \dots, n \tcc a_i \neq 0$ poiché i punti $P_0, \dots, P_{n+1}$ sono in posizione generale.

        Se ad esempio si ha $a_0 = 0$, allora avremmo che $\{ v_{n+1}, v_1, \dots, v_n \}$ è un insieme costituito da vettori linearmente dipendenti $\implies P_{n+1}, P_1, \dots, P_n$ sarebbero linearmente dipendenti contraddicendo l'ipotesi sui $P_0, \dots, P_{n+1}$.        

        Definiamo il riferimento proiettivo associato a $P_0, \dots, P_{n+1}$:

        $$
        e_0 = a_0 v_0, e_1 = a_1 v_1, \dots, e_n = a_n v_n
        $$

        $\implies v_{n+1} = e_0 + \dots + e_n$. $P_i = [v_i] = [a_i v_i] = [e_i]$ per $i = 0, \dots, n$ e $P_{n+1} = [v_{n+1}] = [e_0 + \dots + e_n]$.
    \end{proof}
\end{lemma}

\begin{example}
    Consideriamo i seguenti punti in $\P^3(\R)$: 

    $$
    P_1 = [1, 0, 1, 3]
    \qquad P_2 = [0, 1, 1, 1]
    \qquad P_3 = [2, 1, 2, 2]
    \qquad P_4 = [1, 1, 2, 3]
    $$

    Abbiamo che $t = \#\{ P_1, \dots, P_4 \} = 4 = \dim V$. Inoltre $P_1, \dots, P_4$ sono in posizione generale $\iff P_1, \dots, P_4$ sono linearmente indipendenti.

    \textbf{Esercizio.} Verificare che sono linearmente indipendenti (è un esercizio di Algebra Lineare).

    \textbf{Esercizio.} $\dim L(P_1, P_2, P_3, P_4) =$ ?
\end{example}

\begin{observation}
    Visto che ogni sottospazio vettoriale $W$ di $V$ possiede una base, abbiamo che $\P(W) = L(P_1, \dots, P_t)$ dove $P_i = [v_i]$ e $v_1, \dots, v_t$ è una base di $W$. $t = \dim W$ (dunque $P_1, \dots, P_t$ sono in posizione generale)
\end{observation}

Come prima, $\dim V = n + 1$. Fissiamo un sottospazio vettoriale $W$ di $V$ di dimensione $k + 1$ ($\dim W = k + 1$). Siano $P_0 = [v_0], \dots, P_k = [v_k]  \in \P(W)$ punti in posizione generale $\implies v_0, \dots, v_k$ formano una base di $W$. Di conseguenza, per ogni punto $P = [v] \in \P(W)$ abbiamo che

$$
v = \lambda_0 v_0 + \dots + \lambda_k v_k
$$ 

per certi $\lambda_0, \dots, \lambda_k \in \K$. Fissiamo adesso un riferimento proiettivo $e_0, \dots, e_n$ di $\P(V)$. Allora

$$
P = [x_0, \dots, x_n] 
\qquad P_i = [y_{i,0}, \dots, y_{i,n}] \quad i = 0, \dots, k
$$

Possiamo riscrivere $v$ nella seguente forma.

$$
\begin{aligned}
    x_0 =& \; \lambda_0 y_{0,0} + \lambda_1 y_{1,0} + \dots + \lambda_k y_{k,0} \\
    x_1 =& \; \lambda_0 y_{0,1} + \lambda_1 y_{1,1} + \dots + \lambda_k y_{k,1} \\
    &\vdots \\ 
    x_n =& \; \lambda_0 y_{0,n} + \lambda_1 y_{1,n} + \dots + \lambda_k y_{k,n} \\
\end{aligned}
$$

\begin{example}
    $\dim \P(W) = 1 = \K$ allora abbiamo

    $$
    \begin{aligned}
        x_0 &=& \lambda_0 y_{0,0} + \lambda_1 y_{1,0} \\
        x_1 &=& \lambda_0 y_{0,1} + \lambda_1 y_{1,1} \\
        & \vdots & \\
        x_n &=& \lambda_0 y_{0,n} + \lambda_1 y_{1,n} \\
    \end{aligned}
    $$

    che sono le \textbf{equazioni parametriche della retta proiettiva} $L(P_0, P_1)$ con $P_0 = [y_{0,0}, \dots, y_{0,n}]$ e $P_1 = [y_{1,0}, \dots, y_{1,n}]$.
\end{example}

Le \textbf{equazioni cartesiane della retta $L(P, Q)$} con $P = [p_0, p_1, p_2]$ e $Q = [q_0, q_1, q_2]$ in $\P^2(\K)$ per $P \neq Q$ sono

$$
\det \begin{pmatrix}
    x_0 & p_0 & q_0 \\
    x_1 & p_1 & q_1 \\
    x_2 & p_2 & q_2 \\
\end{pmatrix}
= 0
$$

Le \textbf{equazioni cartesiane del piano $L(P, Q, R)$} con $P, Q, R$ in posizione generale in $\P^3(\K)$ sono

$$
\det \begin{pmatrix}
    x_0 & p_0 & q_0 & r_0 \\
    x_1 & p_1 & q_1 & r_1 \\
    x_2 & p_2 & q_2 & r_2 \\
    x_3 & p_3 & q_3 & r_3 \\
\end{pmatrix}
= 0
$$

\subsection{Unione di sottospazi}

Siano ora $S_1, S_2$ due sottospazi proiettivi di $\P(V)$

\begin{definition}
    Chiamiamo \textbf{sottospazio somma di $S_1$ e $S_2$} il sottospazio proiettivo dato da

    $$
    L(S_1, S_2) := L(S_1 \cup S_2)
    $$

    generato dall'unione $S_1 \cup S_2$.
\end{definition}

\begin{lemma}
    Se $S_1 = \P(W_1), S_2 = \P(W_2)$. Allora $L(S_1, S_2) = L(S_1 \cup S_2) = \P(W_1 + W_2)$

    \begin{proof}
        Sia $L(S_1, S_2) = \P(W)$ per qualche $W$ sottospazio vettoriale di $V$. Per definizione, $L(S_1, S_2)$ è il più piccolo sottospazio proiettivo che contiene $S_1$ ed $S_2$.

        $$
        \left.
        \begin{gathered}
            S_1 \subset L(S_1, S_2) \implies W_1 \subset W \\
            S_2 \subset L(S_1, S_2) \implies W_2 \subset W \\
        \end{gathered}
        \right\}
        \implies W_1 + W_2 \subset W 
        \implies \P(W_1 + W_2) \subseteq \P(W) = L(S_1, S_2)
        $$

        $L(S_1, S_2)$ è il più piccolo sottospazio che contiene $S_1, S_2 \implies L(S_1, S_2) \subseteq \P(W_1 + W_2)$.
    \end{proof}
\end{lemma}

\subsubsection{Formula di Grassmann}
    
Ricordiamo che la formula di Grassmann nel caso degli spazi vettoriali è la seguente

$$
\dim(W_1 + W_2) = \dim W_1 + \dim W_2 - \dim W_1 \cap W_2
$$

e ricaviamo ora una analoga detta \textbf{formula di Grassmann proiettiva}.

$$
\begin{aligned}
    \dim L(S_1, S_2) &= \dim \P(W_1 + W_2) \\
    &= \dim(W_1 + W_2) - 1 \\
    &= \dim W_1 + \dim W_2 - \dim W_1 \cap W_2 - 1 \\
    &= (\dim W_1 - 1) + (\dim W_2 - 1) - (\dim W_1 \cap W_2 - 1) \\
    &= \dim S_1 + \dim S_2 - \dim S_1 \cap S_2 \\
\end{aligned}
$$

$$
\implies \dim L(S_1, S_2) = \dim S_1 + \dim S_2 - \dim S_1 \cap S_2
$$

e vale che $\dim S_1 \cap S_2 = \dim S_1 + \dim S_2 - \dim L(S_1, S_2) \geq \dim S_1 + \dim S_2 - \dim \P(V)$.

\begin{prop}
    In un piano proiettivo, due rette qualsiasi si incontrano.

    \begin{proof}
        Siano $S_1 = r_1, S_2 = r_2$ due rette proiettive in un piano proiettivo. $\dim S_1 \cap S_2 = \dim r_1 \cap r_2 \geq \dim r_1 + \dim r_2 - \dim \P(V) = 1 + 1 - 2 = 0 \implies \dim r_1 \cap r_2 \geq 0 \implies r_1 \cap r_2 \neq \varnothing$
    \end{proof}
\end{prop}

\begin{prop}
    In uno spazio proiettivo di dimensione $3$, una retta e un piano qualsiasi si incontrano, e due piani distinti hanno una retta in comune.

    \begin{proof}
        Sia $S_1 = r_1$ una retta e sia $S_2 = p_2$ un piano. $\dim r_1 \cap p_2 \geq 1 + 2 - 3 = 0 \implies \dim r_1 \cap p_2 \geq 0 \implies r_1 \cap p_2 \neq \varnothing$.

        Se invece prendiamo $S_1 = p_1, S_2 = p_2$ due piani proiettivi. Assumiamo che $S_1 \neq S_2 \implies \dim S_1 \cap S_2 < 2$, d'altra parte $\dim S_1 \cap S_2 \geq 2 + 2 - 3 = 1 \implies 2 > \dim S_1 \cap S_2 \geq 1 \implies \dim S_1 \cap S_2 = 1 \implies S_1 \cap S_2$ è una retta.
    \end{proof}
\end{prop}

% BM - Bookmark: Ultimo punto della teoria %

\end{document}
