\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 5 Dicembre 2019}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\section{Omotopia}

Siano $f$ e $g : X \to Y$ continue. Un omotopia tra $f$ e $g$ è una mappa continua $H : X \times [0, 1] \to Y$ tale che $H(\cdot, 0) = f$ e $H(\cdot, 1) = f$.

\begin{fact}
    $\forall t$ la mappa $H_t(x) = H(x, t)$ definita su $H_t : X \to Y$ è continua per cui definisce un'interpolazione continua tra $f$ e $g$
\end{fact}

\begin{definition}
    $f$ si dice \textbf{omotopa} a $g$ e si scrive $f \sim g$ se esiste un'omotopia tra $f$ e $g$.
\end{definition}

Mostriamo che è una relazione di equivalenza

\begin{enumerate}
    
    \item Vale $f \sim f$: Basta prendere $\forall x \in X \; t \in I \; H(x, t) = f(x)$

    \item $f \sim g \implies g \sim f$: Basta prendere $K(x, t) = H(x, 1 - t)$

    \item $f \sim g$ e $g \sim h \implies f \sim h$. Se $H$ è omotopia tra $f$ e $g$ e $K$ tra $g$ e $h$ allora definiamo

        $$
        \begin{array}{ccccl}
            J & : & X \times [0, 1] & \to & Y \\
            && (x, t) & \mapsto & 
            \begin{cases}
                H(x, 2t) & t \in \lrbt{0, \frac{1}{2}} \\
                H(x, 2t - 1) & t \in \lrbt{\frac{1}{2}, 1} \\
            \end{cases} \\
        \end{array}
        $$

        $J$ è continua in quanto ben definita e continua sui chiusi $X \times [0, \frac{1}{2}]$ e $[\frac{1}{2}, 1]$ che danno un ricoprimento fondamentale di $X \times [0, 1]$

        [TODO: Disegnino]

\end{enumerate}

\hfill$\square$

Dunque essere omotope è una relazione di equivalenza sull'insieme $C(X, Y)$ delle funzioni continue da $X$ a $Y$. L'insieme delle classi di omotopia di tali funzioni si denota con $[X, Y]$.

\begin{example}
    Se $Y$ è un convesso di $\R^n$ (per esempio $\R^n$ stesso) allora $|[X, Y]| = 1$, cioè tutte le mappe $f : X \to Y$ sono omotope tra loro. Infatti date $f, g : X \to Y$ basta porre $H(x, t) = t f(x) + (1 - t) g(x)$ che è ben definito poiché $Y$ è convesso.

    In realtà basta $Y$ stellato rispetto a $P \in Y$, in tal caso $H(x, t) = t f(x) + (1 - t) P$ è un'omotopia tra $f$ e la costante $P$, da cui la tesi per transitività di $\sim$. (Un insieme $Y$ si dice stellato rispetto a $P \in Y$ se $\forall Q \in Y$ il segmento $PQ$ è tutto contenuto in $Y$)
\end{example}

\begin{definition}
    Sia $X$ spazio topologico, allora $\pi_0(X)$ è l'insieme delle componenti connesse per archi di $X$.
\end{definition}

\begin{lemma}
    Sia $f : X \to Y$ continua, allora la funzione $f_* : \pi_0(X) \to \pi_0(Y)$ indotta da $f$ è ben definita. (Indotta dalla richiesta che $\forall C \in \pi_0(X) \; f(C) \subseteq f_*(C)$)

    La buona definizione segue dal fatto che $f$ manda connessi per archi in connessi per archi e le componenti connesse per archi sono disgiunte, per cui $f_*(C)$ esiste ed è unica. 

    Se $f \sim g \implies f_* = g_*$.

    \begin{proof}
        Sia $C \in \pi_0(X)$ e sia $x_0 \in C$ (per convenzione le c.c.p.a. sono non vuote). Se $H$ è un'omotopia tra $f$ e $g$ la mappa

        $$
        \begin{array}{rcccc}
            \gamma & : & [0, 1] & \to & Y \\
            && t & \mapsto & H(x_0, t) \\
        \end{array}
        $$

        è un arco continuo in $Y$ che congiunge $f(x_0)$ con $g(x_0)$. Dunque $f(x_0)$ e $g(x_0)$ giacciono nella stessa c.c.p.a. di Y, che è sia $f_*(C)$ (in quanto contiene $f(x_0)$) ed anche $g_*(C)$ (in quanto contiene $g(x_0)$) Dunque $f_*(C) = g_*(C)$

    \end{proof}
\end{lemma}

\begin{fact}
    Se $X \subseteq \R^n$ è stellato rispetto a $P$ allora c'è una bigezione tra $[X, Y]$ e $\pi_0(X)$

    \begin{proof}
        $X$ stellato $\implies X$ connesso per archi $\implies |\pi_0(X)| = 1$ dunque posso definire $\psi$

        $$
        \begin{array}{rcccc}
            \psi & : & C(X, Y) & \to & \pi_0(Y) \\
            && f & \mapsto & f_*(X) \\
        \end{array}
        $$

        per il lemma, $\psi$ induce una ben definita $\varphi : [X, Y] \to \pi_0(Y)$, basta vedere che $\varphi$ è bigettiva.

        \begin{itemize}
            \item Suriettvità: Dato $C \in \pi_0(Y)$ scelgo $x_0 \in C$ e pongo $\forall x \in X \; f(x) = y_0$, $f$ è continua e $\psi(f) = C \implies \varphi([f]) = C$.
            \item Iniettività: Data $f \in C(X, Y)$ ho che $f$ è omotopa alla costante $f(P)$, pongo $H(x, t) = f(\underbrace{t x + (1 - t) P}_{\mathclap{\in X \text{ poiché } X \text{ è stellato}}})$

                $$
                \begin{aligned}
                    \forall x & \quad & H(x, 0) = f(0 + P) = f(P) \\
                    \forall x & \quad & H(x, 1) = f(x + 0) = f(x) \\
                \end{aligned}
                $$

                Date $f, g$ con $\psi(f) = \psi(g)$ abbiamo che $f(P)$ e $g(P)$ vivono nella stessa c.c.p.a. di $Y$, dunque le costanti $f(P)$ e $g(P)$ sono omotope tramite $H(x, t) = \gamma(t)$, dove $\gamma$ è un arco che congiunge $f(P)$ a $g(P)$. Dunque $f \sim $ cost. $f(P) \sim$ cost. $g(P) \sim g \implies [f] = [g]$.

        \end{itemize}

    \end{proof}
\end{fact}

\begin{definition}
    $f : X \to Y$ è un'\textbf{equivalenza omotopica} se ammette un'inversa omotopica, cioè $g : Y \to X$ tale che ($f, g$ continue) $f \compose g \sim \Id_Y$ e $g \compose f \sim \Id_X$. $X$ e $Y$ si dicono \textbf{omotopicamente equivalenti} (o \textbf{omotopi}) se esite un'equivalenza omotopica tra loro.
\end{definition}

\textbf{Esercizio.} Essere omotopicamente equivalente è una relazione di equivalenza, la transitività si mostra usando il seguente lemma

\begin{lemma}
    Siano $f_0, f_1 : X \to Y$ e $g_0, g_1 : Y \to Z$ continue con $f_0 \sim f_1$ e $g_0 \sim g_1 \implies g_0 \compose f_0 \sim g_1 \compose f_1$.

    \begin{proof}
        Se $H$ è un'omotopia tra $f_0$ e $f_1$ e $K$ tra $g_0$ e $g_1$ allora $(x, t) \mapsto K(H(x, t), t)$ è un'omotopia tra $g_0 \compose f_0$ e $g_1 \compose f_1$
    \end{proof}
\end{lemma}

\begin{definition}
    $X$ è \textbf{contraibile} se è omotopicamente equivalente a un punto (considerato come spazio topologico).
\end{definition}

\begin{prop}
    $X \subseteq \R^n$ stellato $\implies X$ contraibile.

    \begin{proof}
        Sia $Y = \{ 0 \}$ un punto. Siano $f : X \to Y$ l'unica funzione costante e $g : Y \to X$ con $g(0) = x_0 \in X$ scelto a caso. $f, g$ sono continue $f \compose g = \Id_Y$ e $g \compose f : X \to X$ è omotopa a $\Id_X$ poiché $X$ è stellato per cui $|[X, X]| = 1$.

    \end{proof}
\end{prop}

\begin{observation}
    $X$ contraibile $\implies X$ connesso per archi. Infatti se $f : X \to Y$ è un'equivalenza omotopica $f_* : \pi_0(X) \to \pi_0(Y)$ è bigettiva (segue dal fatto che le mappe omotope inducono la stessa mappa sui $\pi_0$ e $(f \compose g)_* = f_* \compose g_*$)
\end{observation}

\begin{definition}
    $X$ spazio topologico allora $C \subseteq X$ si dice 

    \begin{itemize}
        \item \textbf{retratto} se $\exists r : X \to C$ continua tale che $\forall x \in C \; r(x) = x$
        \item \textbf{retratto di deformazione} se $\exists r : X \to C$ come sopra tale che se $i : C \to X$ è l'inclusione allora esiste omotopia $H$ tra $\Id_X$ e $i \compose r$ tale che $\forall x \in C, t \in [0, 1] \; H(x, t) = x$
    \end{itemize}

    Segue che se $Y$ è un retratto di deformazione di $X$, allora $X$ e $Y$ sono omotopicamente equivalenti ($i$ e $r$ sono equivalenze omotopiche)
\end{definition}

\begin{example} \ 
    \begin{itemize}
        
        \item Se $P \in X$ allora $\{ P \}$ è un retratto di $X$.
        
        \item $S^n$ è un retratto per deformazione di $\R^{n+1} \setminus \{ 0 \}$ con

            $$
            \begin{array}{rcccc}
                r & : & \R^{n+1} \setminus \{ 0 \} & \to & S^n \\
                && x & \mapsto & \displaystyle \frac{x}{\norm{x}} \\
            \end{array}
            $$

            e l'omotopia tra $\Id_{\R^{n+1} \setminus \{ 0 \}}$ e $i \compose r$ è $\displaystyle (x, t) \mapsto (1 - t) x + t \frac{x}{\norm{x}}$ (Attenzione: E' fondamentale che $\forall (x, t) \in \R^{n+1} \setminus \{ 0 \} \times [0, 1]$ valga $H(x, t) \in \R^{n+1} \setminus \{ 0 \}$!)

    \end{itemize}
\end{example}

\end{document}
