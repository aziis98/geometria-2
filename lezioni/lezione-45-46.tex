\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Aggiornamenti del 25 Marzo 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\begin{prop}
    La funzione $\opn{Arg} : D \to \CC$ è continua.

    \begin{proof}
        $\forall z \in \CC \quad \arg(z) = \arg\lrpa{\frac{z}{|z|}}$ ed abbiamo che il seguente diagramma commuta 

        [TODO: Aggiustare]
        $$
        \begin{tikzcd}
            D \ar[swap]{rdd}{\opn{Arg}(\,\cdot\,)} & & \{ u \in \CC \mid |u| = 1, \Re(u) > 0 \} = U \ar{ldd}{\opn{Arg}(\,\cdot\,)} \\
            z \ar[mapsto]{rr}{} & & \frac{z}{|z|} \\
            & \CC & \\
        \end{tikzcd}
        $$

        La mappa $z \mapsto \frac{z}{|z|}$ è continua. Ci rimane da provare che $\opn{Arg}(\,\cdot\,) : U \to \CC$ è continua. Per costruzione, la mappa $u \mapsto \opn{Arg}(u) = y$ è l'inversa di

        $$
        \begin{array}{rccc}
            g : & \left] -\frac{\pi}{2}, \frac{\pi}{2} \right[ & \longrightarrow & U \\
            & y & \longmapsto & e^{iy}
        \end{array}
        $$

        ed osserviamo che $g$ è continua e beigettiva. $g$ si estende a $\tilde g$ continua e bigettiva da $[-\frac{\pi}{2}, \frac{\pi}{2}]$ (intervallo compatto) a $\{ u \in \CC \mid |u| = 1 , \Re(u) \geq 0 \}$ (spazio di Hausdorff) $\implies \tilde f$ è un omeomorfismo quindi la sua inversa è continua, quindi l'inversa di $g$ è continua, quindi $\opn{Arg}(\hole) : U \to \CC$ è continua.
    \end{proof}
\end{prop}

\begin{prop}
    La serie di potenze $\sum_{n \geq 0} (-1)^{n-1} \dfrac{z^n}{n}$ converge per $|z| < 1$ ed è uguale alla branca principale di $\log(z + 1)$.
\end{prop}

\begin{prop}
    Se $f(z)$ è una branca di $\log(z)$ in un insieme aperto connesso $D$, la funzione $f$ ammette derivata

    $$
    \dfrac{\mathrm{d} f}{\mathrm{d}z}(z) = \frac{1}{z}
    $$
\end{prop}

\begin{definition}
    $\forall z, \alpha \in \CC, z \neq 0$. Poniamo

    $$
    z^\alpha := e^{\alpha\log(z)}
    $$    
\end{definition}

\section{Funzioni Analitiche}

\begin{definition}
    Sia $U \subset \CC$ un aperto. Una funzione $f : U \to \CC$ si dice \textbf{analitica} in $z_0 \in U$ se

    \begin{enumerate}
        \item $\exists$ una serie di potenze $\sum_{n \geq 0} a_n (z - z_0)^n$ che converge assolutamente per $|z - z_0| < r$ per qualche $r > 0$.
        \item $f(z) = \sum_{n \geq 0} a_n (z - z_0)^n$ per $|z - z_0| < r$.
    \end{enumerate}
    
    Diremo che $f$ è \textbf{analitica in $U$} se $f$ è \textit{analitica} in $z_0$, $\forall z_0 \in U$.
\end{definition}

\begin{examples}
    \begin{itemize}
        \item i polinomi
        \item $e^z, \cos(z), \sin(z), \dots$
    \end{itemize}
\end{examples}

\begin{prop}
    Sia $U \subset \CC$ un aperto e siano $f, g : U \to \CC$ funzioni analitiche in $U$. Allora

    \begin{itemize}
        \item $f + g$ è analitica in $U$        
        \item $fg$ è analitica in $U$
        \item $\dfrac{f}{g}$ è definita e analitica in qualche aperto contenuto in $\{ z \in U \mid g(z) \neq 0 \}$
    \end{itemize}
\end{prop}

\begin{prop}
    Siano $U, V$ aperti di $\CC$. Siano $f: U \to V$ e $g : V \to \CC$ funzioni analitiche. Allora $g \compose f$ è analitica in $U$.
\end{prop}

\begin{prop}
    Sia $U \subset \CC$ aperto e $f : U \to \CC$ una funzione. Se $f$ è analitica in $U$, allora $f$ è continua in $U$.

    \begin{proof}
        Sia $z_0 \in U$. Assumiamo che $f(z) = \sum_{n \geq 0} a_n (z - z_0)^n$ per $|z - z_0| < r$ per qualche $r > 0$.

        Senza perdita di generalità, possiamo assumere che $z_0 = 0$ e $f(z_0) = f(0) = 0 \implies a_0 = f(z_0) = f(0) = 0$.

        $$
        f(z) = \sum_{n \geq 0} a_n (z - z_0)^n = \sum_{n \geq 0} a_n z^n = \sum_{n \geq 1} a_n z^n = z \sum_{n \geq 1} a_n z^{n-1}
        $$

        Se $0 < \rho < r$ e $|z| < \rho$ abbiamo che

        $$
        |f(z)| \leq \sum_{n \geq 1} |a_n| |z^n| \leq |z| \sum_{n \geq 1} |a_n| |z|^{n-1} \leq |z| \underbrace{\sum_{n \geq 1} |a_n| \rho^{n-1}}_{\not\propto |z|}
        $$

        dunque per $|z| \to 0 \implies |f(z)| \to 0$.
    \end{proof}
\end{prop}

\begin{prop}
    Sia $z_0 \in \CC$. Sia $\sum_{n \geq 0} a_n (z - z_0)^n$ una serie di potenze assolutamente convergente nel disco aperto $D_r(z_0) = \{ z \mid |z - z_0| < r \}$ per qualche $r > 0$. Allora la funzione

    $$
    \begin{array}{rccc}
        f : & D_r(z_0) & \longrightarrow & \CC \\
        & z & \longmapsto & \displaystyle \sum_{n \geq 0} a_n (z - z_0)^n
    \end{array}
    $$

    è analitica in $D_r(z_0)$.

    \begin{proof}
        Non è restrittivo assumere che $z_0 = 0$. Allora $f(z) = \sum_{n \geq 0} a_n z^n$. Sia $a \in D_r(0)$ e sia $s$ tale che $|a| + s < r$. Notiamo che

        $$
        z^n = ((z - a) + a)^n = \sum_{0 \leq k \leq n} \binom{n}{k} (z - a)^k a^{n - k}
        $$

        quindi $f(z) = \sum_{n \geq 0} a_n z^n = \sum_{n \geq 0} a_n \lrpa{ \sum_{0 \leq k \leq n} \binom{n}{k} (z - a)^k a^{n - k}}$. Se $|z - a| < s \implies |a| + |z - a| < r$, quindi la serie $\sum_{n \geq 0} |a_n| [|a| + |z - a|]^n$ converge. Scambiando l'ordine della sommatoria, scrivendo $f(z) = \sum_{n \geq 0} b_n (z - a)^n$ per $|z - a| < s$

        $$
        f(z) = \sum_{n \geq 0} \lrpa{\sum_{k \geq n} a_k \binom{k}{n} a^{k - n}} \cdot (z - a)^n
        $$

        \textbf{Osservazione.} Il termine $\sum_{k \geq n} a_k \binom{k}{n} a^{k - n}$ converge poiché $\sum_{l \geq 0} b_l z^l$ assolutamente convergente $\implies \sum_{l \geq 0} l b_l z^{l - 1}$ assolutamente convergente con lo stesso raggio di convergenza (visto in precedenza) $\implies$ iterando arriviamo a

        $$
        \sum_{l \geq s} l (l - 1) \cdots (l - s + 1) b_l z^{l - s} = s! \sum_{l \geq s} \binom{l}{s} b_l z^{l - s}
        $$

        assolutamente convergente con stesso raggio di convergenza di $\sum_{l \geq 0} b_l z^l$. Nel nostro caso, visto che $\sum_{n \geq 0} a_n z^n$ era assolutamente convergente per $|z| < r$ abbiamo che

        $$
        \sum_{k \geq n} a_k \binom{k}{n} a^{k - n}
        $$

        è convergente visto che $|a| < r$.
    \end{proof}
\end{prop}

\begin{theorem}
    Sia $U \subset \CC$ aperto. Se $f : U \to \CC$ è analitica in $U$, allora $f$ è olomorfa in $U$ e la sua derivata $f'$ è una funzione analitica in $U$.

    \begin{proof}
        Sia $z_0 \in U$. Per ipotesi $\exists$ una serie $\sum_{n \geq 0} a_n (z - z_0)^n$ che converge assolutamente per $|z - z_0| < r$ per qualche $r > 0$. sia $\delta > 0$ tale che $|z| + \delta < r$. Per ogni $h \in \CC, h \neq 0, |h| < \delta$ abbiamo

        $$
        f(z + h) = \sum_{n \geq 0} a_n (z + h)^n = \sum_{n \geq 0} a_n (z^n + n h z^{n-1} + h^2 P_n(z, h))
        $$

        dove $P_n(z, h)$ è un polinomio in $z$ e $h$ a coeficienti interi positivi

        $$
        P_n(z, h) = \sum_{k=2}^n \binom{n}{k} h^{k-2} z^{n-k} \implies 
        |P_n(z, h)| \leq \sum_{k = 0}^{n-2} \binom{n}{k} \delta^{k-2} |z|^{n-k} = \underbrace{P_n(|z|, \delta)}_{\not\propto h}
        $$

        Allora $\displaystyle f(z + h) = \sum_{n \geq 0} a_n (z^n + nhz^{n-1} + h^2 P_n(z, h)) = f(z) + \sum_{n \geq 1} a_n n h z^{n-1} + h^2 \sum_{n \geq 2} a_n P_n(z, h)$

        $$
        \underbrace{f(z + h) - f(z) - \sum_{n \geq 1} a_n n h z^{n-1}}_{(*)}
         = \underbrace{h^2 \sum_{n \geq 2} a_n P_n(z, h)}_{(**)}
        $$

        Per ipotesi abbiamo che $(*)$ è assolutamente convergente $\implies$ $(**)$ è assolutamente convergente e quidni dividendo per $h$ otteniamo che per $|h| < \delta$

        $$
        \frac{f(z + h) - f(z)}{h} - \sum_{n \geq 1} a_n n z^{n-1} = \underbrace{h \sum_{n \geq 2} a_n P_n(z, h)}_{\text{da stimare}}
        $$

        $$
        \bigg| \sum_{n \geq 2} a_n P_n(z, h) \bigg| \leq \sum_{n \geq 2} |a_n| P_n(|z|, \delta)
        $$

        non dipende da $h$ e converge quindi infine

        $$
        \begin{tikzcd}[column sep=tiny]
            \dfrac{f(z + h) - f(z)}{h} \ar{d}{h \to 0} & - & \displaystyle\sum_{n \geq 1} a_n n z^{n-1} \ar{d}{} 
            &=& h \displaystyle\sum_{n \geq 2} a_n P_n(z, h) \ar{d}{} \\
            f'(z) & - & \displaystyle\sum_{n \geq 1} a_n n z^{n-1} &=& 0 \\
        \end{tikzcd}
        $$

        quindi $f$ è olomorfa in $z$, inoltre $f'(z) = \sum_{n \geq 1} a_n n z^{n-1} \implies f'$ è analitica.
    \end{proof}
\end{theorem}

\begin{prop}
    Sia $\log : D \to \CC$ una branca del logaritmo, $D \subseteq \CC$ aperto connesso. Allora $\log$ è olomorfa e $\log'(z) = \frac{1}{z}$ (ovviamente $0 \notin D$)

    \begin{proof}  
        Infatti $\log(\exp(z)) = z + c$, $c$ costante implica $\log'(\exp(z)) \cdot \exp'(z) = 1$, da cui, essendo $\exp' = \exp$, $\log'(\exp(z)) = \dfrac{1}{\exp(z)}$, cioè $\log'(y) = \dfrac{1}{y}$. (Detto bene, fissato $z_0 \in D, z_0 \neq 0 \; \exists y_0 \in \CC$ con $z_0 = \exp(y_0)$ per continuità di $\exp$, dato un intorno $V$ di $z_0$ in $D$, $\exists$ intorno $W$ di $y_0$ in $\CC$ tale che $\exp(W) \subseteq V$, e le uguaglianze sopra sono verificate per $y \in W, z \in V$)
    \end{proof}
\end{prop}

Abbiamo usato:

\begin{prop}
    Se $f : D \to D'$ è olomorfa e bigettiva, $g : D' \to D$ è l'inversa di $f$. Se $f'(z_0) \neq 0$ allora $g$ è olomorfa in $f(z_0) \neq 0$, allora $g$ è olomorfa in $f(z_0)$ e vale l'usuale formula

    $$
    g'(f(z_0)) = \frac{1}{f'(z_0)}
    $$

    \begin{proof}
        Da $g \compose f = \Id$ e dal teorema della funzione inversa (Analisi 2), poiché $f'(z_0) = a + ib \neq 0$, $\mathrm{d} f_{z_0} = \begin{pmatrix} a & -b \\ b & a \end{pmatrix}$ è invertibile, per cui $g$ è differenziabile in $f(z_0)$ e $\dd g_{f(z_0)} = (\dd f_{z_0})^{-1}$. 

        Infine, si verifica che se $\dd f_{z_0} = A = \begin{pmatrix} a & -b \\ b & a \end{pmatrix}$, $a^2 + b^2 \neq 0$ (cioè $a + ib \neq 0$), $\dd g_{f(z_0)} = A^{-1} = \begin{pmatrix} c & -d \\ d & c \end{pmatrix}$ con $c + id = \dfrac{1}{a + ib}$ su $\CC$.
    \end{proof}
\end{prop}

\begin{prop}
    Siano $D = B(0, 1)$, sia $\log : \{ \Re z > 0 \} \to \CC$ la branca principale del logaritmo. Allora $\forall z \in D$

    $$
    \log(1 + z) = \sum_{n \geq 1} (-1)^n \frac{z^n}{n}
    $$

    \begin{proof}
        Siano $f : D \to \CC, f(z) = \log(1 + z)$ e $g : D \to \CC, g(z) = \sum_{n \geq 1} (-1)^n \frac{z^n}{n}$. Poiché il raggio di convergenza della serie è $1$, $g$ è ben definita, analitica, dunque olomorfa e 

        $$
        \begin{aligned}
            g'(z) &= \sum_{n \geq 1} (-1)^{n+1} n \frac{z^{n-1}}{n} \\
            &= \sum_{n \geq 1} (-1)^{n+1} z^{n-1} = \sum_{n \geq 1} (-1)^n z^n = \sum_{n \geq 1} (-z)^n = \\
            &= \frac{1}{1 - (-z)} = \frac{1}{1 + z} \\ 
        \end{aligned}
        $$

        Perciò se $h = f - g$, $\forall z \in D \quad h'(z) = f'(z) - g'(z) = \dfrac{1}{1 + z} - \dfrac{1}{1 + z} = 0$ dunque $h' \equiv 0$ su $D$, e $h$ è costante su $D$ (funzione con differenziale nullo ovunque è costante su un connesso). 

        Dunque $\forall z \in D \quad h(z) = c$. Per trovare $c$ osservo che $h(0) = f(0) - g(0) = \log(1) - \sum_{n \geq 1} 0 = 0$. Dunque $h \equiv 0$, cioè $f \equiv g$.
    \end{proof}
\end{prop}

\subsection{Prolungamento Analitico}

D'ora in poi, $D$ sarà  un \textit{aperto connesso} di $\CC$.

\begin{prop}
    Sia $f : D \to \CC$ analitica, sono fatti equivalenti

    \begin{enumerate}
        \item $\exists z_0 \in D$ con $\forall n \in \N \quad f^{(n)}(z_0) = 0$
        \item $\exists$ aperto in $U \subseteq D$ con $f \equiv 0$ su $U$
        \item $f \equiv 0$ su $D$
    \end{enumerate}

    \begin{proof} \ 
        \begin{itemize}
            \item[$\boxed{\text{iii)} \Rightarrow \text{i)}}$] Ovvio $\forall z_0 \in D$
            \item[$\boxed{\text{i)} \Rightarrow \text{ii)}}$] Per analiticità, $\forall z \in B(z_0, R) \quad f(z) = \sum_{n \geq 0} a_n (z - z_0)^n$ per qualche $R > 0$. Inoltre, $a_n = \frac{f^{(n)}(z_0)}{n!}$ (segue da una semplice induzione, vedi più avanti). Dunque $f^{(n)}(z_0) = 0 \implies \forall n \in \N \quad a_n = 0$, perciò $f \equiv 0$ su $B(z_0, R)$.

            \item[$\boxed{\text{ii)} \Rightarrow \text{iii)}}$] Sia $\Omega \subseteq D, \Omega = \{ z \in D \mid \exists \text{ intorno aperto } U \ni z, f{|}_U \equiv 0 \}$. Per ii), $\Omega \neq \varnothing$. Poiché $D$ è connesso, basta vedere che $\Omega$ è aperto e chiso per concludere che $\Omega = D$ che è la tesi.

            $\Omega$ chiuso: Sia $z \in D$, con $z = \lim_n z_n$, $z_n \in \Omega$. Allora $\forall n, k \in \N \;\, f^{(k)}(z_n) = 0$ ($z_n \in \Omega \implies f \equiv 0$ su un intorno di $z_n \implies \forall k \in \N \;\, f^{(k)}(z_n) = 0$) Ma $\forall k \in \N \;\, f^{(k)}$ è continua, e dunque $f^{(k)}(z) = \lim_n f^{(k)}(z_n) = 0$, perciò tutte le derivate di $f$ si annullano in $z$. Come nella dimostrazione di i) $\Rightarrow$ ii), ciè implica che $\exists$ intorno di $z$ su cui $f$ è identificamente nulla perciò $z \in \Omega$, che pertanto è chiuso.
        \end{itemize}
    \end{proof}
\end{prop}

Abbiamo usato che, se $f : D \to \CC$ è analitica, $f' : D \to \CC$ è essa stessa analitica, e se

$$
f(z) = \sum_{n \geq 0} a_n (z - z_0)^n \implies f'(z) = \sum_{n \geq 1} n a_n (z - z_0)^{n-1}
$$

da cui $f'$ è essa stessa derivabile. Iterando otteniamo $f$ è derivabile infinite volte, e $f^{(n)}(z_0) = a_n \cdot n!$ cioè $a_n = \dfrac{f^{(n)}(z_0)}{n!}$.

\textbf{Attenzione.} L'enunciato appena dato è falso per funzioni $C^\infty$, se consideriamo $f : \CC \to \CC$ data 

$$
f(a + ib) =
\begin{cases}
    e^{-\frac{1}{a}} & a > 0 \\
    0 & a \leq 0 \\
\end{cases}
$$

allora $f$ è $C^\infty$, si annulla su $\{ \Re z \leq 0 \}$, dunque anche su un aperto di $\CC$, ma non è nulla su $\CC$. In questo esempio l'insieme $\Omega$ definito sopra è aperto ma non è chiuso.

[TODO: Plot]

\begin{corollary}
    Date $f, g : D \to \CC$ analitiche. Se $f = g$ su un aperto $U \subseteq D$, allora $f = g$ su $D$. Se $\exists z_0 \in D$ con $\forall n \in \N \;\, f^{(n)}(z_0) = g^{(n)}(z_0)$, allora $f = g$ su $D$.

    \begin{proof}
        Basta applicare il teorem appena visto a $f - g$.
    \end{proof}
\end{corollary}

\begin{corollary}
    L'annello delle funzioni analitiche su $D$ è un dominio di integrità.

    \begin{proof}
        Se $f, g : D \to \CC$ sono tali che $f \cdot g = 0$ allora, se $A = \{ z \mid f(z) = 0 \}, B = \{ z \mid g(z) = 0 \}$, allora $D = A \cup B$. $A, B$ sono chiusi $\implies$ almeno uno di essi ha parte interna non vuota (Esercizio). Se $A^\circ \neq \varnothing, f \neq 0$ su un aperto $\implies f \equiv 0$ su $D$.

        Dunque l'anello in questione non ha divisori dello zero.
    \end{proof}
\end{corollary}

\subsection{Zeri di funzioni analitiche}

\begin{definition}
    Sia $f : D \to \CC$ una funzione analitica non identicamente nulla. Allora $\forall z_0 \in D, \ord_{z_0}(f) = \min\{ n \in \N \mid f^{(n)}(z_0) \neq 0 \}$.
\end{definition}
    
Per il teorema visto sopra, poiché $f$ non è identicamente nulla $\{ n \in \N \mid f^{(n)}(z_0) \neq 0 \} \neq \varnothing$ dunque $\ord_{z_0}(f)$ è ben definito

Inoltre abbiamo che $f(z_0) = 0 \iff \ord_{z_0}(f) \geq 1$

\begin{definition}
    Uno zero $z_0$ di $f$ si dice \textbf{semplice} se $\ord_{z_0}(f) = 1$ altrimenti si dice \textbf{multiplo}.
\end{definition}

Se $f(z) = \sum_{n \geq 0} a_n (z - z_0)^n$, poiché $f^{(n)}(z_0) = n! \cdot a_n$, $\ord_{z_0}(f) = \min \{ n \in \N \mid a_n \neq 0 \}$

\begin{prop}
    $f : D \to \CC$ analitica. Allora

    $$
    f(z) = (z - z_0)^{\ord_{z_0}(f)} \cdot g(z)
    $$

    con $g : D \to \CC$ analitica e $g(z) \neq 0$ in un intorno di $z_0$.

    \begin{proof}
        Sia $k = \ord_{z_0}(f)$. In un intorno di $z_0$

        $$
        \begin{aligned}
            f(z) &= \sum_{n \geq 0} a_n (z - z_0)^n = \sum_{n \geq k} a_n (z - z_0)^n = \\
            &= (z - z_0)^k \sum_{n = k}^\infty a_n (z - z_0)^{n - k} = (z - z_0)^k \underbrace{\sum_{n \geq 0} a_{n+k} (z - z_0)^n}_g
        \end{aligned}
        $$

        la serie di potenze che definisce $g$ ha lo stesso raggio di convergenza di quella di $f$ ($\limsup_n \sqrt[n]{|a_{n+k}|} = \limsup_n \sqrt[n]{|a_n|}$). In particolare, $\exists$ intorno $U \ni z_0$ tale che, posto $g(z) = \sum_{n \geq 0} a_{n+k} (z - z_0)^n$, si ha che $\forall z \in U \;\, f(z) = (z - z_0)^k g(z)$, $g$ è analitica e $g(z_0) = a_k \neq 0$ (per definizione di $k = \ord_{z_0}(f)$). Questo mi dà la tesi su $U$. Fuori da $U$ (anzi, su $D \setminus \{ z_0 \}$) pongo $g(z) = \dfrac{f(z)}{(z - z_0)^k}$.

        Questa definizione estende $g(z) = \sum_{n \geq 0} a_{n+k}(z - z_0)^n$ a una funzione analitica su tutto $D$. Infine, poiché $g(z_0) \neq 0$ e $g$ è continua in quanto analitica, $g(z) \neq 0$ in un intorno di $z_0$.
    \end{proof}
\end{prop}

\begin{corollary}
    $f : D \to \CC$ analitica non identicamente nulla, $C = \{ z \in D \mid f(z) = 0 \} \subseteq D$ è \textbf{discreto} (e chiuso in $D$, ma non necessariamente in $\CC$).

    \begin{proof}
        Se $z_0 \in C, f(z_0) = 0$ e $k = \ord_{z_0} (f)$, allora in un intorno $U$ di $z_0$ si ha $f(z) = (z - z_0)^k g(z), \forall z \in U \;\, g(z) \neq 0$. Se $z \in U \setminus \{ z_0 \}, (z - z_0)^k \neq 0, g(z) \neq 0 \implies f(z) \neq 0$. Perciò $C \cap U = \{ z_0 \}$, dunque $C$ è discreto ed è chiuso perché preimmagine di $0$ che è chiuso.
    \end{proof}
\end{corollary}

\section{1-forme differenziali complesse}

$\CC$ è un $\R$-spazio vettoriale di dimensione $2$, con base $\{ 1, i \}$. Fissata questa base 

$$
\begin{array}{ccc}
    \opn{End}_\R (\CC) & \simeq & \mathfrak{M}(2, 2, \R) \\
    \varphi & \simeq & 
    \begin{pmatrix}
        \Re(\varphi(1)) & \Re(\varphi(i)) \\
        \Imm(\varphi(1)) & \Imm(\varphi(i)) \\
    \end{pmatrix}
\end{array}
$$

\begin{definition}
    Sia $D \subseteq \CC$ un dominio aperto. Una $1$-forma complessa su $D$ è una funzione $\omega : D \to \opn{End}_\R (\CC)$ continua (rispetto all'usale topologia su $\mathfrak{M} (2, 2, \R) \simeq \R^4$). 
\end{definition}

Notiamo che i differenziali associati alla base canonica sono

$$
\begin{aligned}
    \dd x : \CC \to \CC & \quad & \dd x (a + ib) = a \\
    \dd y : \CC \to \CC & \quad & \dd y (a + ib) = b \\
\end{aligned}
$$

e sono una base di $\opn{End}_\R(\CC)$ inteso come $\CC$-spazio vettoriale (se $\lambda \in \CC, \varphi : \CC \to \CC$ è $\R$-lineare, anche $\lambda \cdot \varphi$ dato da $(\lambda \cdot \varphi)(z) = \lambda \cdot \varphi(z)$ è $\R$-lineare, dunque $\opn{End}_\R(\CC)$ ha una struttura di $\CC$-spazio vettoriale).

Più concretamente, una $1$-forma differenziale complessa $\omega$ su $D$ corrisponde a due funzioni $P, Q : D \to \CC$ tali che

$$
\omega(z) = P(z) \dd x + Q(z) \dd y = P \dd x + Q \dd x
$$

\begin{prop}
    La continuità di $\omega$ equivale esattamente alla continuità di $P$ e $Q$.

    \begin{proof}
        Infatti se $P(z) = a(z) + i b(z)$ e $Q(z) = c(z) + i d(z)$ abbiamo che

        $$
        \begin{aligned}
            \omega(z)(1) &= P(z) \dd x(1) + Q(z) \dd y(1) = P(z) = a(z) + i b(z) \\
            \omega(z)(i) &= \dots = Q(z) = c(z) + i d(z) \\
        \end{aligned}
        $$

        perciò $\ds \omega(z) = \begin{pmatrix} a(z) & c(z) \\ b(z) & d(z) \end{pmatrix}$.

        ed è continua $\iff a, b, c, d$ lo sono $\iff P, Q$ lo sono.
    \end{proof}
\end{prop}

\textbf{Esempio fondamentale.} Se $f : D \to \CC$ è $C^1$, cioè differenziabile con derivate parziali continue, $z = u + iv$, allora $\dd f$ è una $1$-forma complessa, data da

$$
\dd f = 
\begin{pmatrix}
    \dfrac{\pd \Re(f)}{\pd u} & \dfrac{\pd \Re(f)}{\pd v} \\
    \dfrac{\pd \Imm(f)}{\pd u} & \dfrac{\pd \Imm(f)}{\pd v} \\
\end{pmatrix}
$$

(sto usando la base $\{ 1, i \}$ per identificare $\CC$ con $\R^2$).

Un'altra base utilie di $\opn{End}_\R(\CC)$ è data da 

$$
\dd z = \dd x + i \dd y \quad \text{ e } \quad \dd \bar z = \dd x - i \dd y
$$

che è una base poiché si ottiene da $\dd x, \dd y$ tramite la matrice invertibile $\ds \begin{pmatrix} 1 & 1 \\ i & -i \end{pmatrix}$.

Abbiamo che $\dd x = \dfrac{\dd z + \dd \bar z}{2}$ e $\dd y = \dfrac{\dd z - \dd \bar z}{2i} = -i \dfrac{\dd z - \dd \bar z}{2}$. Inoltre, se $f : D \to \CC$ è differenziabile,

$$
\begin{aligned}
    \dd f &= \frac{\pd f}{\pd x} \dd x + \frac{\pd f}{\pd y} \dd y \\
    &= \frac{\pd f}{\pd x} \lrpa{\frac{\dd z + \dd \bar z}{2}} + \frac{\pd f}{\pd y} \lrpa{-i\frac{\dd z - \dd \bar z}{2}} \\
    &= \frac{1}{2}\lrpa{\frac{\pd f}{\pd x} - i \frac{\pd f}{\pd y}} \dd z + \frac{1}{2}\lrpa{\frac{\pd f}{\pd x} + i \frac{\pd f}{\pd y}} \dd \bar z \\
    &= \frac{\pd f}{\pd z} \dd z + \frac{\pd f}{\pd \bar z} \dd \bar z
\end{aligned}
$$

osserviamo che $f$ è olomorfa $\iff \dd f$ è $\CC$-lineare, cioè $\dfrac{\pd f}{\pd y} = \dd f (i) = i \dd f(1) = i \dfrac{\pd f}{\pd x} \iff \dfrac{\pd f}{\pd x} = -i \dfrac{\pd f}{\pd y} \iff \dfrac{\pd f}{\pd \bar z} = 0$.

\begin{prop}
    $f : D \to \CC$ differenziabile. Allora $f$ è olomorfa $\iff \dfrac{\pd f}{\pd \bar z} = 0$ e in tal caso $\dd f = \dfrac{\pd f}{\pd z} \dd z$
\end{prop}

\end{document}
