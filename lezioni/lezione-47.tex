\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 26 Marzo 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

Abbiamo visto che

$$
\dd f = \frac{\pd f}{\pd x} \dd x + \frac{\pd f}{\pd y} \dd y 
\qquad
\dd f = \frac{\pd f}{\pd z} \dd z + \frac{\pd f}{\pd \bar z} \dd \bar z 
$$

Se $f$ è olomorfa (cioè $\forall z \in D \quad \dd f_z$ è $\CC$-lineare ed è perciò la moltiplicazione per un elemento di $\CC$, detto $f'(z)$) si ha $\dfrac{\pd f}{\pd \bar z} = 0$.

Inoltre, sempre assumendo $f$ olomorfa, 

$$
\begin{aligned}
    \frac{\pd f}{\pd x}(z) &= \dd f_z (1) = f'(z) \cdot 1 = f'(z) \\
    \frac{\pd f}{\pd y}(z) &= \dd f_z (i) = i \cdot \dd f_z (1) = i \cdot f'(z)
\end{aligned}
$$

Perciò $\dfrac{\pd f}{\pd z} = \dfrac{1}{2} \lrpa{ \dfrac{\pd f}{\pd x} - i \dfrac{\pd f}{\pd y} } = \dfrac{1}{2}\lrpa{f'(z) - i \cdot i f'(z)} = f'(z)$.

\begin{corollary}
    Se $f$ è olomorfa, $\dd f = f' \dd z$

    \begin{proof}
        Infatti $\dd f = \frac{\pd f}{\pd z} \dd z + \frac{\pd f}{\pd \bar z} \dd \bar z = f'(z) \dd z + 0$
    \end{proof}
\end{corollary}

\begin{definition}
    Sia $\omega$ una $1$-forma differenziale complessa su $D$. Allora $\omega$ è \textbf{esatta} se $\exists F : D \to \CC$ tale che $\omega = \dd F$. Inoltre, $\omega$ è \textbf{chiusa} se è localmente esatta, cioè $\forall p \in D \; \exists$ aperto $U \subseteq D$ con $p \in U$ e $F_U : U \to \CC$ con $\dd F_U = \omega{|}_U$ (In particolare, $F$ e $F_U$ sono differenziabili). $F$ si chiama \textbf{primitiva} di $\omega$, metre $F_U$ è una \textbf{primitiva locale}. 
\end{definition}

Ovviamente $\omega$ esatta $\implies \omega$ chiusa.

Ricordiamo che $\dd F = \frac{\pd f}{\pd x} \dd x + \frac{\pd f}{\pd y} \dd y$ per cui $\omega = P \dd x + Q \dd y$ è esatta se $\exists F$ tale che $P = \frac{\pd F}{\pd x}, Q = \frac{\pd F}{\pd y}$, cioè se il campo vettoriale $z \mapsto (P(z), Q(z))$ è gradiente di una funzione (si dice che "ammette un potenziale"). Le forme chiuse corrispondono ai campi che ammettono potenziali locali. Vedremo che questa condizione si può controllare locale ed è equivalente ad essere \textqt{irrotazionale}.

\begin{itemize}
    \item Chiusura di $\omega \rightsquigarrow$ Proprietà locale
    \item Esattezza di $\omega \rightsquigarrow$ Proprietà globale che dipende dalla topologia di $D$
\end{itemize}

\subsection{Integrali curvilinei}

Se $f : [a, b] \to \CC$ è continua, possiamo definire

$$
\int_a^b f(t) \dd t = \int_a^b \Re(f(t)) \dd t + i \int_a^b \Imm(f(t)) \dd t
$$

Sia ora $D$ un dominio aperto connesso di $\CC$ e fissiamo $\omega$ una $1$-forma su $D$. Se $\gamma : [a, b] \to D$ è un cammino $C^1$ (ovvero una curva $\gamma(t) = x(t) + i y(t)$ con $x, y \in C^1$), definiamo

$$
\int_\gamma \omega := \int_a^b \underbrace{\omega_{\gamma(t)}}_{= \omega(\gamma(t))}(\gamma'(t)) \dd t
$$

dove se $\gamma(t) = (x(t), y(t)) = x(t) + i y(t) \rightsquigarrow \gamma'(t) = (x'(t), y'(t)) = x'(t) + i y'(t)$.

Se $\omega = P \dd x + Q \dd y$ e $P, Q : D \to \CC$ continue abbiamo che

$$
\begin{aligned}
    \omega_{\gamma(t)}(\gamma'(t)) &= P(\gamma(t)) \dd x (\gamma'(t)) + Q(\gamma(t)) \dd y (\gamma'(t)) \\
    &= P(\gamma(t)) x'(t) + Q(\gamma(t)) y'(t) \\
\end{aligned}
$$

che è una funzione continua di $t$ poiché $\gamma$ è $C^1$ quindi è integrabile.

\textbf{Esempio fondamentale.} Siano $D = \CC^* = \CC \setminus \{ 0 \}, \omega = \dfrac{1}{z} \dd z, \gamma(t) = e^{2\pi i t} = \cos(2\pi t) + i \sin(2\pi t)$. $\gamma$ è un loop con punto iniziale e finale $1 \in \CC$.

[TODO: Plot]

Vogliamo calcolare $\int_\gamma \omega$. Ricordiamo che $\dd z = \dd x + i \dd y$, cioè è l'identità di $\CC$. Infatti

$$
\dd z (a + i b) = (\dd x + i \dd y) (a + i b) = \dd x(a + ib) + i \dd y (a + ib) = a + ib
$$

Inoltre $\gamma'(t) = 2 \pi i e^{2\pi i t} = 2 \pi i \gamma t$. Dunque $\omega_{\gamma(t)}(\gamma'(t)) = \dfrac{\dd z}{\gamma(t)}(2\pi i \gamma (t)) = \dfrac{2 \pi i \gamma(t)}{\gamma(t)} = 2 \pi i$

Perciò $\ds \int_\gamma \omega = \int_0^1 2 \pi i \dd t = 2 \pi i$, in coordinate (cioè usando $\dd x$ e $\dd y$)

$$
\begin{aligned}
    \frac{\dd z}{z} &= \frac{1}{x + i y}(\dd x + i \dd y) = \frac{x - iy}{x^2 + y^2} (\dd x + i \dd y) \\
    &= \frac{x - i y}{x^2 + y^2} \dd x + \frac{ix + y}{x^2 + y^2} \dd y \\
    &= \frac{x \dd x + y \dd y}{x^2 + y^2} + i \frac{x \dd y - y \dd x}{x^2 + y^2}
\end{aligned}
$$

$\gamma(t) = \cos(2 \pi t) + i \sin(2 \pi t) = x(t) + i y(t)$, con $x(t) = \cos 2 \pi t, y(t) = \sin 2 \pi t$. Perciò

$$
\begin{aligned}
    \dd x(\gamma'(t)) &= x'(t) = - 2\pi \sin 2\pi t \\ 
    \dd y(\gamma'(t)) &= y'(t) = 2\pi \cos 2\pi t \\ 
\end{aligned}
$$

$$
\begin{gathered}
    \implies (x \dd x + y \dd y)_{\gamma(t)}(\gamma'(t)) = i \frac{\cos(2\pi t) \cdot 2\pi \cdot \cos(2 \pi t) - \sin (2 \pi t) \cdot (- 2 \pi) \cdot \sin (2 \pi t)}{\cos^2 2\pi t + \sin^2 2\pi t} \\
    = i \frac{2 \pi (\cos^2 + \sin^2)}{\cos^2 + \sin^2} = 2 \pi i
\end{gathered}
$$

Abbiamo ritrovato che $\ds \int_\gamma \frac{\dd z}{z} = \int_\gamma \frac{x \dd x + y \dd y}{x^2 y^2} + i \int_\gamma \frac{x \dd y - y \dd x}{x^2 + y^2} = 0 + \int_0^1 2 \pi i = 0 + 2 \pi i$ come sopra.

Un fatto utile appena visto è che $\dd z = \Id_\CC$, perciò $\forall a \in \CC \quad \dd z(a) = a$ e $\dd \bar z (a) = \bar a$.

\textbf{Proprietà dell'integrale curvilineo.}

\begin{itemize}
    \item Se $\gamma = \gamma_1 * \gamma_2$ (giunzione $C^1$), allora

        $$
        \int_\gamma = \int_{\gamma_1} \omega + \int_{\gamma_2} \omega
        $$

        (la dimostrazione segue subito dalla successiva).

    \item Sia $\gamma : [a, b] \to D$. Se $\psi : [c, d] \to [a, b]$ è $C^1$ con $\psi(c) = e, \psi(d) = b$, allora

        $$
        \int_\gamma \omega = \int_{\gamma \compose \psi} \omega
        $$

        ovvero $\int_\gamma \omega$ è indipendente da riparametrizzazioni che preservano il verso.

        \begin{proof}
            Infatti, $\ds \int_{\gamma \compose \psi} \omega = \int_c^d \omega_{\gamma(\psi(t))}((\gamma \compose \psi)'(t)) \dd t = \int_c^d \omega(\gamma(\psi(t))) \cdot (\gamma'(\psi(t)) \cdot \psi'(t)) \dd t = \int_c^d \psi'(t) \cdot \omega(\gamma(\psi(t))) (\gamma'(\psi(t))) \dd t = \int_{\psi(c)}^{\psi(d)} \omega(\gamma(s)) (\gamma'(s)) \dd s = \int_a^b \omega(\gamma(t)) \cdot \gamma'(t) = \int_\gamma \omega$
        \end{proof}

    \item Se $\gamma : [a, b] \to D$ è $C^1$ a tratti (cioè continua e tale che $\exists a = t_0 < \cdots < t_n = b$ tale che $\gamma{|}_{[t_i, t_{i+1}]}$ sia $C^1$ per $i = 0, \dots, n-1$) allora poniamo

        $$
        \int_\gamma \omega = \sum_{i=0}^{n-1} \int_{\gamma{|}_{[t_i, t_{i+1}]}} \omega
        $$

        la definizione non dipende dalla partizione per le proprietà appena viste.
\end{itemize}

\begin{lemma}
    $D \subseteq \CC$ aperto connesso. Allora $D$ è connesso per archi $C^1$ (ci basterà per archi e $C^1$ a tratti).

    \begin{proof}
        Quasi identica a \textqt{localmente connesso per archi $+$ connesso $\implies$ connesso per archi}. Basta osservare che ogni punto di $D$ ha un intorno connesso per archi $C^1$ (una piccola palla), scelgiere $x_0 \in D$, e mostrare che l'insieme dei punti di $D$ connessi a $x_0$ da un arco $C^1$ a tratti è aperto e chiuso. 
    \end{proof}
\end{lemma}

\begin{lemma}
    Sia $\omega$ una $1$-forma esatta su $D$, con $\omega = \dd F$. Allora $\forall \gamma : [a, b] \to D$ curva $C^1$ a tratti vale

    $$
    \int_\gamma \omega = F(\gamma(b)) - F(\gamma(a))
    $$

    In particolare, $\int_\gamma \omega$ dipende solo dai punti iniziale e finale di $\gamma$.

    \begin{proof}
        Sia $a = t_0 < \cdots < t_n = b$ una partizione tale che $\forall i \; \gamma{|}_{[t_i, t_{i+1}]}$ sia $C^1$. Se mostriamo che

        $$
        \int_{\gamma{|}_{[t_i, t_{i+1}]}} \omega = F(\gamma(t_{i+1})) - F(\gamma(t_i))
        $$

        abbiamo finito perché avremo $\ds \int_\gamma \omega = \sum_{i=0}^{n-1} \int_{\gamma{|}_{[t_i, t_{i+1}]}} \omega = \sum_{i=0}^{n-1} F(\gamma(t_{i+1})) - F(\gamma(t_i)) = F(\gamma(t_n)) - F(\gamma(t_0)) = F(\gamma(b)) - F(\gamma(a))$.

        Ma $\ds \int_{\gamma{|}_{[t_i, t_{i+1}]}} \omega = \int_{\gamma{|}_{[t_i, t_{i+1})}} \dd F = \int_{t_i}^{t_{i+1}} \dd F_{\gamma(t)} (\gamma'(t)) \dd t = \int_{t_i}^{t_{i+1}} (F \compose \gamma)'(t) \dd t = F(\gamma(t_{i+1})) - F(\gamma(t_i))$.
    \end{proof}
\end{lemma}

\begin{corollary}
    Sia $D \subseteq \CC$ aperto connesso. Se $F$ è una primitiva di $\omega$, tutte e sole le primitive di $\omega$ si ottengono sommando una costante a $F$.

    \begin{proof}
        Se $G$ è un'altra primitiva, $\dd G = \dd F \implies \dd(G - F) = 0 \implies G = F + \text{cost.}$. 

        Il viceversa è ovvio infatti, $\dd(F + \text{cost.}) = \dd F = \omega$.
    \end{proof}
\end{corollary}

\begin{theorem}
    $D \subseteq \CC$ aperto connesso, $\omega$ una $1$-forma su $D$. Allora 

    $$
    \omega \text{ è esatta }\iff \forall \gamma \text{ curva }C^1 \text{ a tratti a valori in }\ds D \text{ vale }\int_\gamma \omega = 0
    $$

    \begin{proof} \ 
        \begin{itemize}
            \itemRarr Se $\gamma : [a, b] \to D$ è un loop, e $\omega = \dd F$ è esatta, abbiamo visto che $\int\limits_\gamma \omega = F(\gamma(b)) - F(\gamma(a)) = 0$ poiché $\gamma(b) = \gamma(a)$.

            \itemLarr Costruiamo una primitiva di $\omega$ come segue. Fissato $x_0 \in D, \forall p \in D$ scelgo un cammino $\gamma_p : [0, 1] \to D$ $C^1$ a tratti con $\gamma(0) = x_0, \gamma(1) = p$ e pongo $\ds F(p) = \int\limits_{\gamma_p} \omega$.

                Mostro che $F$ è ben definita, cioè che non dipende dalla scelta di $\gamma_p$. Sia $\alpha_p$ un altro cammino che congiunge $x_0$ a $p$. Allora $\gamma_p * \bar\alpha_p$ è un loop, per cui per ipotesi

                $$
                0 = \int\limits_{\gamma_p * \bar\alpha_p} \omega = \int\limits_{\gamma_p} \omega + \int\limits_{\bar\alpha_p} \omega = \int\limits_{\gamma_p} \omega - \int\limits_{\alpha_p} \omega
                $$

                cioè $\ds\int\limits_{\gamma_p} \omega = \int\limits_{\alpha_p} \omega$. Dunque $F$ è ben definita. 

                Devo vedere che $F$ è differenziabile e $\dd F = \omega$. Se $\omega = P \dd x + Q \dd y$, basta vedere che $\dfrac{\pd F}{\pd x} = P, \dfrac{\pd F}{\pd y} = Q$ (perché $P, Q$ sono continue per ipotesi, dunque per il teorema del differenziale totale, $F$ ammetterebbe derivate parziali continue e sarebbe differenziabile con $\dd F = \dfrac{\pd F}{\pd x} \dd x + \dfrac{\pd F}{\pd y} \dd y = \omega$)

                Mostriamo che $\dfrac{\pd F}{\pd x} = P$ (per l'altra derivata parziale segue analogamente). Per definizione $\ds F(z_0) = \int\limits_{\gamma_{z_0}} \omega, F(z_0 + h) = \int\limits_{\gamma_{z_0} * \gamma_h} \omega$ per cui 

                $$
                F(z_0 + h) - F(z_0) = \int\limits_{\gamma_{z_0} * \gamma_h} \omega - \int\limits_{\gamma_{z_0}} \omega = \int\limits_{\gamma_{z_0}} \omega + \int\limits_{\gamma_h} \omega - \int\limits_{\gamma_{z_0}} \omega = \int\limits_{\gamma_h} \omega
                $$

                $\gamma_h : [0, h] \to D, \gamma_h(t) = z_0 + t$, per cui $\omega_{\gamma_h(t)} (\gamma_h'(t)) = \omega_{\gamma_h(t)}(1) = P(z_0 + t) \dd x (1) + Q(z_0 + t) \dd y (1) = P(z_0 + t)$. 

                Dunque $\ds \frac{F(z_0 + h) - F(z_0)}{h} = \frac{1}{h} \int_0^h P(z_0 + t) \dd t = P(z_0 + \xi(h))$ con $0 \leq \xi(h) \leq h$ per il teorema della media integrale.

                Passando al limite per $h \to 0$ e usando la continuità di $P$ otteniamo

                $$
                \frac{\pd F}{\pd x}(z_0) = \lim_{h \to 0} \frac{F(z_0 + h) - F(z_0)}{h} = \lim_{h \to 0} P(z_0 + \xi(h)) = P(z_0)
                $$
        \end{itemize}
    \end{proof}
\end{theorem}

\end{document}
