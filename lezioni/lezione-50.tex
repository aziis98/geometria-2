\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del ???}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\subsection{Serie di potenze}

\textbf{Esercizio.} Siano $r \in \R, 0 \leq r \leq 1$. Mostrare che $\sum_{n \geq 0} r^n e^{in\theta}$ e $\sum_{n=-\infty}^\infty$ e $r^{|n|} e^{in\theta}$ convergono per $\theta$ reale.

\begin{proof}
    $$
    \abs[\bigg]{\sum_{n \geq 0} r^n e^{in\theta}} \leq \sum_{n \geq 0} \abs{r^n e^{in\theta}} = \sum_{n \geq 0} \abs{r}^n \cdot \abs{e^{in\theta}} < \infty
    $$

    poiché $|r| < 1$. Possiamo anche calcolare la somma

    $$
    \sum_{n \geq 0} r^n e^{in\theta} = \sum_{n \geq 0} \lrpa{re^{i\theta}}^n = \frac{1}{1 - re^{i\theta}}
    $$

    Invece per l'altra

    $$
    \sum_{n=-\infty}^\infty r^{|n|} e^{in\theta} = \sum_{n \geq 1} r^n e^{in\theta} + \sum_{n \geq 0} r^n e^{-in\theta} = \frac{re^{i\theta}}{1 - r e^{i\theta}} + \frac{1}{1 - r e^{-i\theta}} 
    $$
\end{proof}

\begin{exercise}
    Consideriamo la serie

    $$
    \sum_{n \geq 1} \frac{z^{n-1}}{(1 - z^n) (1 - z^{n+1})}
    $$

    Provare che converge a $\dfrac{1}{(1 - z)^2}$ per $|z| < 1$ e a $\dfrac{1}{z} \dfrac{1}{(1 - z)^2}$ per $|z| > 1$. 

    \begin{proof}
        Poniamo $a_n(z) = a_n := \frac{z^{n-1}}{(1 - z^n)(1 - z^{n+1})}$, dobbiamo calcolare $\lim_n S_n(z)$ dove

        $$
        S_n(z) = \sum_{k=1}^n a_n
        $$

        Consideriamo $b_n = z a_n = \frac{z^n}{(1-z^n) (1 - z^{n+1})}$ e $D(z) := (1 - z^n) (1 - z^{n+1})$.

        $$
        \begin{aligned}
            b_n &= \frac{z^n}{D(z)} \frac{1 - z}{1 - z}
            = \frac{1}{1 - z} \frac{z^n - z^{n+1}}{D(z)} 
            = \frac{1}{1 - z} \frac{z^n - 1 + 1 - z^{n+1}}{D(z)} \\
            &= \frac{1}{1 - z} \lrbt{-\frac{1}{1 - z^{n+1}} + \frac{1}{1-z^n}} = b_n \\
        \end{aligned}
        $$
        
        $$
        \implies S_n = \sum_{k=1}^n a_k = \frac{1}{z} \sum_{k=1}^n b_k = \frac{1}{z (1 - z)} \lrbt{\frac{1}{1 - z} - \frac{1}{1 - z^{n+1}}}
        $$

        Per $|z| < 1$ abbiamo

        $$
        \begin{aligned}
            \lim_n S_n &= \frac{1}{z(1-z)} \lrbt{\frac{1}{1-z} - 1} \\
            &= \frac{1}{z(1 - z)}\frac{z}{1 - z} 
            = \frac{1}{(1 - z)^2} \\
        \end{aligned}
        $$

        Per $\ds |z| > 1, \lim_n S_n = \frac{1}{z(1 - z)}\frac{1}{1 - z}$.
    \end{proof}
\end{exercise}

\begin{exercise}
    Definiamo i \textbf{numeri di Bernoulli} tramite la serie di potenze

    $$
    \sum_{n \geq 0} B_n \frac{z^n}{n!} = \frac{z}{e^z - 1}
    $$

    Mostrare che

    $$
    \frac{B_0}{n!0!} + \frac{B_1}{(n-1)! 1!} + \dots + \frac{B_{n-1}}{1! (n-1)!} = \sum_{k=0}^{n-1} \frac{B_k}{(n-k)! k!} =
    \begin{cases}
        1 & \text{ se } n = 1 \\
        0 & \text{ se } n > 1 \\
    \end{cases}
    \implies B_0 = 1
    $$

    \begin{proof}
        $\ds e^z = \sum_{n \geq 0} \frac{z^n}{n!}$. Allora $\ds \frac{z}{e^z - 1} = \frac{z}{\sum_{n \geq 0} \frac{z^n}{n!} - 1} = \frac{z}{\sum_{n \geq 1} \frac{z^n}{n!}} = \frac{1}{\sum_{k \geq 0} \frac{z^k}{(k+1)!}}$

        $$
        1 = \lrpa{\sum_{k \geq 0} \frac{z^k}{(k+1)!}} \lrpa{\sum_{n \geq 0} B_n \frac{z^n}{n!}} = \sum_{m \geq 0} c_m z^m \implies
        \begin{cases}
            c_m = 1 & \text{ per } m = 0 \\
            c_m = 0 & \text{ per } m > 0 \\
        \end{cases}
        $$

        La tesi seguirà da un calcolo esplicito di $c_m$.

        $$
        c_m = \sum_{l=0}^m B_l \frac{1}{l! (m - l + 1)!}
        $$

        Adesso proviamo che $B_n = 0$ per $n$ dispari, $n \neq 1$

        $$
        \begin{gathered}
            f(z) = 1 + \sum_{n \geq 2} \frac{B_n}{n!} z^n \\
            f(z) - f(-z) = 1 + \sum_{n \geq 2} \frac{B_n}{n!} z^n - 1 - \sum_{n \geq 2} \frac{B_n}{n!} (-z)^n = 2 \sum_{\substack{n \geq 2 \\ n \text{ dispari}}} B_n \frac{z^n}{n!}
        \end{gathered}
        $$

        Vogliamo provare che $\forall n \neq 1$ dispari $B_n = 0$. E' sufficiente provare che $f(z) - f(-z) = 0$. E' facile vedere che $\ds f(z) = \frac{z}{e^z - 1} - 1 + \frac{1}{2}z$ e che $f(z) - f(-z) = 0$ [TODO: Esercizi per casa].
    \end{proof}
\end{exercise}

\textbf{Esercizio.} Sia $\ds f(z) = \sum_{n \geq 0} \frac{z^{2n}}{(2n)!}$. Calcolare il raggio di convergenza e provare che $f''(z) = f(z)$.

\subsection{Integrazione}

\begin{examples} \ 
    \begin{enumerate}[label={\arabic*)}]
        \item Sia $n \in \Z, \omega = (z - z_0)^n \dd z$ per un qualche $z_0 \in \CC$. Dire per quali valori di $n, \omega$ è esatta.
        
        \item Sia $\ds f(z) = \sum_{n \geq 0} a_n (z - z_0)^n$ una serie assolutamente convergente per $|z - z_0| < r$. Sia $\gamma$ una circonferenza centrata in $z_0$ e raggio $R < r$. Calcolare $\ds \int_{\gamma} \frac{1}{(z - z_0)^m} f(z) \dd z$ per $m \geq 1$
    \end{enumerate}

    \begin{proof} \ 
        \begin{enumerate}[label={\arabic*)}]
            \item Per $n \neq 1$ definiamo

                $$
                f(z) = \frac{1}{n + 1} (z - z_0)^{n+1}
                $$

                Allora $\dd f = \omega \implies \omega$ è esatta.

                Per $n = - 1, \gamma : [0, 2\pi] \to \CC, t \mapsto r e^{it} + z_0$ allora

                $$
                \int_{\gamma} \omega = 2 \pi i
                $$

            \item Sia $\gamma : [0, 2\pi] \to \CC, t \mapsto R e^{it} + z_0$. $f(z) = \lim_n S_n(z) \quad |z - z_0| < r$. $S_n(z) = \sum_{k=0}^n a_k (z - z_0)^k$. Calcoliamo l'integrale 

            $$
            \int_{\gamma} \frac{1}{(z - z_0)^m} f(z) \dd z = \int_{\gamma} \frac{1}{(z - z_0)^m} \lrpa{\lim_n} S_n(z) \dd z = \lim_n \int_{\gamma} \frac{1}{(z - z_0)^m} S_n(z) \dd z
            $$
        \end{enumerate}
    \end{proof}
\end{examples}

\begin{definition}
    Sai $S$ un insieme, $f : S \to \CC$ una funzione limitata, definiamo

    $$
    \norm f := \sup \{ |f(s)| \mid s \in S \}
    $$
\end{definition}

\textbf{Esercizio.} Mostrare che quella appena definita è una norma su $\CC$

\begin{definition}
    Sia $\{ f_n : S \to \CC \}$ una successione di funzioni limitate. Diremo che $\{ f_n \}$ converge uniformemente se $\exists f : S \to \CC$ limitata tale che 

    $$
    \forall \varepsilon \; \exists N > 0 \text{ tale che } \forall n \geq N, \norm{f_n - f} < \varepsilon
    $$
\end{definition}

\begin{lemma}
    (Esercizio) Sia $S \subseteq \CC$ e sia $\{ f_n \}$ una successione di funzioni continue e limitate su $S$. Se $f_n \to f$ uniformemente $\implies f$ è continua.
\end{lemma}

\begin{lemma}
    Sia $\{ f_n \}$ una successione di funzioni continue su un aperto $D \subseteq \CC$ tale che $f_n \to f$ uniformemente. Sia $\gamma : [a, b] \to \CC$ una curva differenziabile in $D$, allora

    $$
    \lim_n \int_{\gamma} f_n \dd z = \int_{\gamma} (\lim_n f_n) \dd z = \int_{\gamma} f(z) \dd z 
    $$

    \begin{proof}
        $$
        \abs[\bigg]{\int_{\gamma} f_n - \int_{\gamma} f} \leq \int_{\gamma} |f_n - f| = \int_{a}^{b} \abs{(f_n - f)\gamma(t) \gamma'(t)} \dd t \leq \norm{f_n - f} \int_{a}^{b} |\gamma'(t)| \dd t
        $$

        per $n \to \infty, \norm{f_n - f} \to 0 \implies \ds \int_{\gamma} f_n \xrightarrow{n} \int_{\gamma} f$
    \end{proof}
\end{lemma}

\begin{observation}
    $f(z) = \sum_{n \geq 0} a_n(z - z_0)^n$ converge assolutamente per $|z - z_0| < r \implies$ la successione delle somme parziali converge uniformemente su $|z - z_0| \leq \rho$ per $0 < \rho < r$.
\end{observation}

Ritornando all'esercizio, siamo autorizzati a scrivere

$$
\int_{\gamma} \frac{1}{(z - z_0)^m} f(z) \dd z = \lim_n \int_{\gamma} \frac{1}{(z - z_0)^m} \sum_{k=0}^n a_k (z - z_0)^k \dd z
$$


Non ci rimane che calcolare

$$
\int_{\gamma} \frac{1}{(z-z_0)^m} \sum_{k=0}^n a_k (z - z_0)^k \dd z =
\begin{cases}
    a_{m-1} 2 \pi i & \text{ per } n \geq m - 1, k = m - 1 \\
    0 & \text{ altrimenti }
\end{cases}
$$

$$
\implies \int_{\gamma} \frac{1}{(z - z_0)^m} f(z) \dd z = a_{m-1} 2 \pi i
$$

\textbf{Esercizio.} Calcolare l'integrale $\ds \int_{\gamma} z e^{z^2}$ con $\gamma$: (a) il segmento che unisce i punti $i$ e $2-i$ (b) da $0$ a $1+i$ lungo la parabola $y = x^2$.

\subsection{Relazione tra funzioni armoniche e olomorfe}

Sia $\ds \Delta := \frac{\pd^2}{\pd x^2} + \frac{\pd^2}{\pd y^2}$ allora diamo le seguenti definizioni

\begin{definition}
    Sia $D \subseteq \R^2$ un aperto, una funzione $u \in C^2(D)$ si dice \textbf{armonica} se $\Delta u = 0$.
\end{definition}

\begin{theorem}
    Sia $f$ olomorfa su $D$, tale che $f = u + i v$ con $u,v \in C^2(D)$. Allora $u, v$ sono armoniche.

    \begin{proof}
        Bisogna provare che $\Delta u = 0$, cioè $\ds \frac{\pd^2 u}{\pd x^2} + \frac{\pd^2 u}{\pd y^2} = 0$. D'altro canto abbiamo

        $$
        \frac{\pd^2 u}{\pd x^2} = \frac{\pd}{\pd x} \lrpa{\frac{\pd u}{\pd x}} = \frac{\pd}{\pd x} \lrpa{\frac{\pd v}{\pd y}} = \frac{\pd^2 v}{\pd x \pd y}
        $$

        per ipotesi $v \in C^2(D)$ dunque

        $$
        = \frac{\pd^2 v}{\pd y \pd x} = \frac{\pd}{\pd y} \lrpa{\frac{\pd v}{\pd x}} = \frac{\pd}{\pd y} \lrpa{-\frac{\pd u}{\pd y}} = -\frac{\pd^2 u}{\pd y^2}
        $$

        ed applicando il teorema di Schwartz abbiamo $\implies \Delta u = 0$. Sia fa in modo simile per $v$.
    \end{proof}
\end{theorem}

\subsubsection{Problema di definire funzioni olomorfe a partire da funzioni armoniche}
    
\begin{theorem}
    Sia $D \subseteq \CC$ un aperto semplicemente connesso. Sia $u \in C^2(D)$ una funzione armonica. Allora $\exists f : D \to \CC$ olomorfa con $\Re f = u$ e tale funzione è definita univocamente a meno di una costante puramente immaginaria.

    \begin{proof}
        Vorremmo trovare $v \in C^2(D)$ tale che

        $$
        \frac{\pd v}{\pd x} = -\frac{\pd u}{\pd y}, \frac{\pd v}{\pd y} = \frac{\pd u}{\pd x} 
        $$

        (in modo da ottenere $f$ come $u + iv$), abbiamo che $\exists v \in C^2(D)$ che soddisfa le condizioni $\iff \omega := \ds -\frac{\pd u}{\pd y} \dd x + \frac{\pd u}{\pd x} \dd y$ è esatta in $D$.

        Utilizziamo che $D$ è semplicemente connesso: $\omega$ esatta $\iff \omega$ chiusa $\iff \dd \omega = 0$

        $$
        \dd \omega = \lrpa{\frac{\pd Q}{\pd x} - \frac{\pd P}{\pd y}} \dd x \dd y = \lrpa{\frac{\pd^2 u}{\pd x^2} + \frac{\pd^2 u}{\pd y^2}} \dd x \dd y
        $$

        usando che $\omega = P \dd x + Q \dd y$ e poi che $\ds P = -\frac{\pd u}{\pd y}, Q = \frac{\pd u}{\pd x}$.

        $$
        = \Delta u \, \dd x \dd y = 0 \implies \dd \omega = 0
        $$

        che implica $\omega$ chiusa e quindi esatta. E si conclude notando che $\omega = \dd v$ da che $v$ è definita a meno di una costante. 
    \end{proof}
\end{theorem}

    









\end{document}
