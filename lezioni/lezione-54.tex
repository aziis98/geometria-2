\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 20 Aprile 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

[Fact Check: Generalizziamo ora i risultati a funzioni solamente continue]

\begin{definition}
    Sia $D \subseteq \CC$ un aperto e $f : D \to \CC$ una funzione. Diciamo che $f$ ha la \textbf{proprietà del valor medio} se $\forall a \in D \, \exists r_0$ tale che $\{ z \mid |z - a| < r_0 \} \subseteq D$ e 

    $$
    \forall r \in [0, r_0) \quad f(a) = \frac{1}{2\pi} \int_{0}^{2R} f(a + r e^{i\theta}) \dd \theta 
    $$
\end{definition}

\begin{observation}
    Se $f$ è olomorfa $\implies f$ ha la \textit{proprietà del valor medio}
\end{observation}

\begin{theorem}
    \textbf{Principio del massimo modulo.}

    Sia $D \subseteq \CC$ un aperto e $f : D \to \CC$ una funzione continua che ha la proprietà del valor medio. Se $|f|$ ha un massimo relativ in $a \in D$, allora $f$ è costante in un intorno di $a$.    
\end{theorem}

\begin{corollary}
    Sia $D \subseteq \CC$ un aperto connesso e limitato. Sia $f$ una funzione continua su $\overline D$ che ha la proprietà del valor medio in $D$. Se

    $$
    M := \sup \{ |f(z)| \mid z \in \pd D \}
    $$

    Allora 

    \begin{itemize}
        \item $|f(z)| \leq M \; \forall z \in D$
        \item Se $\exists a \in D$ tale che $|f(z)| = M$, allora $f$ è costante.
    \end{itemize}

    \begin{proof}
        Definiamo $M' = \sup \{ |f(z)| \mid z \in \overline D \}$ allora $M \leq M' < \infty$ e $\exists$ almeno un punto $a \in \overline D$ tale che $|f(z)| = M'$. Adesso dobbiamo considerare due casi: $a \in D$ oppure $a \notin D$.

        \begin{itemize}
            \item \textbf{Caso 1.} Se $a \in D$, allora per il teorema (principio del massimo modulo) $\implies f$ è costante in un intorno di $a$.

                Definiamo $D' := \{ z \in D \mid |f(z)| = |f(a)| = M' \}$, vorremmo provare che $D' = D$.

                Abbiamo che $D'$ è chiuso per definizione e $a \in D' \implies D' \neq \varnothing$. Adesso utilizzando gli argomenti della dimostrazione del teorema, possiamo osservare che se $\exists a'$ tale che $|f(a')| = M' = |f(a)| \implies \forall z$ con $|z - a'| < r_0'$ si ha $f(z) = f(a')$ per un certo $r_0' \implies D'$ è aperto $\implies D' = D$.

                Visto che $f$ è continua in $\overline D \implies f(z) = f(a)$ in tutto $\overline D \implies f(z) = f(a)$ in tutto $\overline D \implies M = M'$ e otteniamo le tesi.

            \item \textbf{Caso 2.} Se $a \notin D \implies a \in \pd D \implies M \geq |f(a)| = M'$ e per costruzione, $M \leq M' \implies M = M'$ da cui segue la prima tesi. 
        \end{itemize}
    \end{proof}
\end{corollary}

\begin{corollary}
    \textbf{Principio del massimo modulo per funzioni olomorfe.}

    Sia $f$ una funzione olomorfa su un aperto connesso $D \subseteq \CC$. Se $f$ non è costante, allora $|f|$ non ha massimo relativo in $D$. Inoltre se $D$ è limitato e $F$ è continua sul bordo di $D$, allora $|f|$ assume massimo nel bordo di $D$.

    \begin{proof}
        Vogliamo provare che $|f|$ ha massimo relativo in $D \implies f$ è costante. Sia $f$ olomorfa in $D \implies f$ ha la proprietà del valor medio. Per il principio del massimo modulo, $|f|$ ha massimo relativo in $D \implies \exists a \in D$ tale che $f$ è costante in un intorno di $a \implies f$ è costante in tutto $D$. (Abbiamo usato il principio di continuazione analitica che richiede la connessione di $D$)

        Per il secondo enunciato applichiamo il corollario (possiamo farlo perché adesso $D$ è anche limitato) e così otteniamo

        $$
        \forall z \quad |f(z)| \leq M = \sup \{ |f(w)| \mid w \in \pd D \}
        $$ 
    \end{proof}
\end{corollary}

\begin{theorem}
    \textbf{Lemma di Schwarz.}

    Sia $f(z)$ una funzione olomorfa nel disco aperto $|z| < 1$ e assumiamo che $f(0) = 0$ e $\forall |z| < 1 \; |f(z)| < 1$, allora

    \begin{itemize}
        \item $|f(z)| \leq |z|$ per $|z| < 1, |f'(0)| \leq 1$
        \item Se $\exists z_0 \neq 0$ tale che $|f(z_0)| = |z_0|$ oppure $|f'(0)| = 1 \implies \exists \lambda \in \CC$ tale che $f(z) = \lambda z$ (con $|\lambda| = 1$)
    \end{itemize}

    \begin{proof}
        $f$ è olomorfa quindi analitica per $|z| < 1 \implies$ sia $f(z) = \sum_{n \geq 0} a_n z^n$ l'espansione di Taylor nell'origine (con raggio di convergenza $\rho \geq 1$). Definiamo la funzione

        $$
        g(z) = 
        \begin{cases}
            \dfrac{f(z)}{z} & 0 < |z| < 1 \\
            a_1 = f'(0) & z = 0 \\
        \end{cases}
        $$

        Allora $g(z) = \sum_{n \geq 1} a_n z^{n-1}$ per $|z| < 1 \implies g$ è analitica $\implies$ olomorfa per $|z| < 1$. Prendiamo $r \in (0, 1)$ e $|z| = r$. Abbiamo $|g(z)| = \abs[\bigg]{\frac{f(z)}{z}} \leq \dfrac{1}{r}$.

        A questo punto, abbiamo che $g$ è olomorfa e $\forall |z| < r \; |g(z)| \leq \dfrac{1}{r} \implies |g(z)| \leq 1$.

        Abbiamo $\forall |z| < 1 \; |g(z)| \leq 1 \implies \abs[\bigg]{\dfrac{f(z)}{z}} \leq 1 \implies \forall |z| < 1 \; |f(z)| \leq |z|$. $|g(z)| \leq 1 \implies |g(0)| \leq 1 \implies |f'(0)| \leq 1$.

        Se $\exists a$ tale che $|a| < 1$ e $|g(a)| = 1$

        $$
        \lrpa{\iff \exists z_0 \neq 0 \text{ tale che } |f(z_0)| = |z_0| \text{ oppure } |f'(0)| = 1}
        $$

        per il corollario al principio del massimo modulo $\implies g$ è costante (usando il secondo punto) cioè $\exists \lambda \in \CC$ tale che $g(z) = \lambda$ ($1 = |g(a)| = |\lambda|$)

        $$
        g(z) = \frac{f(z)}{z} \implies f(z) = \lambda z
        $$
    \end{proof}
\end{theorem}

\textbf{Esercizio.} Cosa successione se assumiamo $|f(z)| \leq 1$ al posto di $|f(z)| < 1$?

\section{Serie di Laurent}

L'obbiettivo è capire il comportamento di una funzione olomorfa in un corona circolare $\{ z \in \CC \mid \rho_2 < |z| < \rho_1 \}$. Vedremo che il ruolo dell'espansione di Taylor per funzioni olomorfe in $|z| < \rho$ verrà preso dall'espansioni di Laurent per funzioni olomorfe in $\rho_2 < |z| < \rho_1$.

Chiamiamo \textbf{serie di Laurent} un'espressione della forma

$$
\sum_{n \in \Z} a_n z^n 
$$ 

Ad essa possiamo associar edue serie di potenze

$$
\sum_{n \geq 0} a_n z^n \qquad \sum_{n < 0} a_n z^{-n} 
$$

Assumiamo che esse siano assolutamente convergenti con raggi di convergenza $\neq 0, \infty$. Definiamo così i raggi di convergenza

$$
\begin{aligned}
    \rho_1 &= \text{ raggio di convergenza di } \sum_{n \geq 0} a_n z^n \\
    \rho_2 &= \dfrac{1}{\text{raggio di convergenza di } \sum_{n < 0} a_n z^{-n}}\\
\end{aligned}
$$

Consideriamo 

\begin{itemize}
    \item $f_1(z) := \sum_{n \geq 0} a_n z^n$ che converge assolutamente per $|z| < \rho_1 \implies f_1$ è analitica $\implies f_1$ è olomorfa. 
    \item $f_2(z) := \sum_{n < 0} a_n z^n$ che converge assolutamente per $|z| > \rho_2$
\end{itemize}  

Mostriamo che $f_2(z)$ è olomorfa in $|z| > \rho_2$, poniamo $\ds z = \dfrac{1}{n}, g(u) := f_2\lrpa{\dfrac{1}{u}} = \sum_{n < 0} a_n \lrpa{\dfrac{1}{u}}^n = \sum_{n < 0} a_n u^{-n} = \sum_{k \geq 0} a_{-k} u^k$ e tale serie converge assolutamente per $|u| < \dfrac{1}{\rho_2} \implies g'(u) = \sum_{k \geq 0} k a_{-k} u^{k-1}$.

Adesso, osserviamo che $f_2(z) = g\left(\dfrac{1}{z}\right)$ e per concludere usiamo la formula di derivazione di funzioni composte e otteniamo

$$
f_2'(z) = g'\lrpa{\frac{1}{z}}\lrpa{-\frac{1}{z^2}} = -\frac{1}{z^2} \lrpa{\sum_{k \geq 0} k a_{-k} \lrpa{\frac{1}{z}}^{k-1}} = \sum_{n < 0} n a_n z^{n-1} 
$$

$\implies f_2'$ esiste $\implies f_2$ è olomorfa.

\begin{prop}
    Sia $\sum_{n \in \Z} a_n z^n$ una serie di Laurent ed assumiamo che

    \begin{itemize}
        \item $\ds \sum_{n \geq 0} a_n z^n$ e $\sum_{n < 0} a_n z^{-n}$ sono assolutamente convergenti.
        \item I raggi di convergenza $\rho_1$ e $\rho_2$ definiti prima soddisfano $\rho_2 < \rho_1$.
    \end{itemize}

    Allora la somma $f(z)$ della serie di Laurent è olomorfa per $\rho_2 < |z| < \rho_1$ e la serie converge in norma in $r_2 \leq |z| \leq r_1$ con $\rho_2 < r_2 < r_1 < \rho_1$.
\end{prop}

\begin{definition}
    Diciamo che una funzione $f(z)$ definita sulla corona circolare $\rho_2 < |z| < \rho_1$ ha un'espansione di Laurent se $\exists$ una serie di Laurent che converge in questa corona e di $f(z)$ è la somma per ogni punto $z$ della corona.
\end{definition}

\begin{observation}
    Se $f(z)$ ammette un'espansione di Laurent $\implies f$ è olomorfa in tale corona.
\end{observation}

\begin{theorem}
    Sia $a \in \CC$ e sia $f$ una funzione olomorfa nella corona circolare $A = \{ z \in \CC \mid 0 < \rho_2 < |z - a| < \rho_1 < \infty \}$, allora $f$ ha un'espansione di Laurent cioè esplicitamente $\forall z \in A$

    $$
    f(z) = \sum_{n \in \Z} a_n (z - a)^n \text{ con } \forall n \; a_n := \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{(w - a)^{n+1}} \dd w  
    $$ 

    \begin{proof}
        Senza prendere di generalità prendiamo $a = 0$, scegliamo $r_1, r_2, r_1', r_2'$ tali che

        $$
        \rho_1 < r_2' < r_2 < r_1 < r_1' < \rho_2
        $$

        siano $\gamma_1 = t \mapsto r_1' e^{2 \pi i t}$, $\gamma_2 = t \mapsto r_2' e^{2 \pi i t}$.

        $B := \{ z \in \CC \mid r_2' \leq |z| \leq r_1' \}$, sia $z \in \CC$ tale che $r_2' < r_2 \leq |z| \leq r_1 < r_1'$. Sia $r > 0$ tale che

        $$
        \{ w \in \CC \mid |w - z| \leq r \} \subset \{ w \in \CC \mid r_2' < |w| < r_1' \}
        $$

        $\alpha$ è una parametrizzazione del bordo di $\overline{B(z, r)}$, col verso in senso antiorario.

        [TODO: \textit{Addendum} e disegnino]

        \begin{observation}
            $\forall \omega$ forma chiusa in $A \setminus B(z, r)$

            $$
            \int_{\gamma_1} \omega - \int_{\gamma_2} \omega - \int_{\alpha} \omega = 0
            $$
        \end{observation}

        In particolare $\dfrac{f(w)}{w - z}$ è olomorfa in $A \setminus B(z, r) \implies \omega = \dfrac{f(w)}{w - z} \dd w$ è chiusa, sostituendo ad $\omega$ sopra l'espressione

        $$
        \frac{1}{2 \pi i} \int_{\gamma_1} \frac{f(w)}{w - z} \dd w - \frac{1}{2 \pi i} \int_{\gamma_2} \frac{f(w)}{w - z} \dd w = \frac{1}{2 \pi i} \int_{\alpha} \frac{f(w)}{w - z} \dd w = \underbrace{I(\alpha, z)}_{=1} f(z)  
        $$
    \end{proof}
\end{theorem}

\end{document}
