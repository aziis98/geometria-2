\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 9 Marzo 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\begin{facts} \ 
    \begin{itemize}
        
        \item Sia $R \subseteq F(S)$, sia $H$ un gruppo arbitrario, sia $\psi : F(S) \to H$ omomorfismo, se $\forall r \in R \quad \psi(r) = e \implies \exists \bar\psi : \langle\, S \mid \,\rangle \to H$ omomorfismo di gruppi tale che $\forall s \in S \quad \bar\psi([s]) = \psi(s)$

        \item $G_0 = \langle\, S_0 \mid R_0 \,\rangle, G_1 = \langle\, S_1 \mid R_1 \,\rangle, G_2 = \langle\, S_2 \mid R_2 \,\rangle$ e $\phi_1 : G_0 \to G_1, \phi_2 : G_0 \to G_2$ omomorfismi e

            $$
            G := \snfrac{G_1 * G_2}{N} \text{ con } N = N(\{ \phi_1(g) \phi_2(g^{-1}) \mid g \in G_0 \})
            $$

            allora $\tilde\phi_i : F(S) \to F(S_i)$ è un omomorfismo tale che $[\tilde\phi_i(s)] = \phi_i([s])$

            $$
            \xymatrix{
                F(S_0) \ar[r]^{\tilde\phi_i} \ar[d] & F(S_i) \ar[d] \\
                G_0 \ar[r]_{\phi_i} & G_i \\
            }
            $$
    \end{itemize}
\end{facts}

\subsection{Teorema di Van Kampen}

\textbf{Teorema.} Sia $X$ spazio topologico, assumiamo $X = A \cup B$ con $A, B$ aperti connessi per archi e $A \cap B$ connesso per archi e $x_0 \in A \cap B$ (d'ora in poi tutti i gruppi fondamentali saranno puntati in $x_0$). Siano $\alpha : A \cap B \hookrightarrow A$ e $\beta : A \cap B \hookrightarrow B$ inclusioni dell'intersezione e $f : A \hookrightarrow X$ e $g : B \hookrightarrow X$ inclusioni degli aperti nello spazio. Consideriamo il seguente diagramma

$$
\xymatrixcolsep{2ex}
\xymatrixrowsep{3ex}
\xymatrix{
    & \pi_1(A) \ar[rd]^{f_*} \ar@/^1.5pc/[rrrd]^{h} &&& \\
    \pi_1(A \cap B) \ar[ru]^{\alpha_*} \ar[rd]_{\beta_*} && \pi_1(X) \ar@{-->}[rr]^{\exists! \phi} & \qquad & G \\
    & \pi_1(B) \ar[ru]_{g_*} \ar@/_1.5pc/[rrru]_{k} &&& \\
}
$$

sia $G$ un gruppo, se abbiamo delle mappe $h : \pi_1(A) \to G$ e $k : \pi_1(B) \to G$ omomorfismo tale che $h \compose \alpha_* = k \compose \beta_* \implies \exists! \phi : \pi_1(X) \to G$ tale che $\phi \compose f_* = h$ e $\phi \compose g_* = k$.

Per prima cosa vediamo un corollario che rappresenta la conseguenza principale del teorema di Van Kampen.

\begin{corollary}
    $\pi_1(X) \simeq \snfrac{\pi_1(A) * \pi_1(B)}{N}$ con $N = N(\{ \alpha_*(g) \beta_*(g)^{-1} \mid g \in \pi_1(A \cap B) \})$

    \textit{Dimostrazione del Corollario.}
    Per prima cosa consideriamo le immersioni naturali dei gruppi fondamentali degli aperti nel loro prodotto libero.

    $$
    i : \pi_1(A) \to \pi_1(A) * \pi_1(B) \qquad j : \pi_1(B) \to \pi_1(A) * \pi_1(B)
    $$

    Se considero il diagramma

    $$
    \xymatrixcolsep{2ex}
    \xymatrixrowsep{3ex}
    \xymatrix{
        \pi_1(A) \ar[rd]^{i} \ar@/^1.5pc/[rrrd]^{f_*} &&& \\
        & \pi_1(A) * \pi_1(B) \ar@{-->}[rr]^{\quad\exists! \psi} & \qquad & \pi_1(X) \\
        \pi_1(B) \ar[ru]_{j} \ar@/_1.5pc/[rrru]_{h_*} &&& \\
    }
    $$

    per la proprietà universale dei prodotti liberi esiste $\exists! \psi : \pi_1(A) * \pi_1(B) \to \pi_1(X)$ tale che $\psi \compose i = f_*$ e $\psi \compose j = g_*$. Sia ora $\pi$ la proiezione al quoziente

    $$
    \xymatrix{
        \pi_1(A) * \pi_1(B) \ar[r]^{\quad\psi} \ar[d]_{\pi} & \pi_1(X) \\
        \snfrac{\pi_1(A) * \pi_1(B)}{N} \ar@{-->}[ru]_{\exists\bar\psi} & \\
    }
    $$

    abbiamo che $\exists \bar\psi : \snfrac{\pi_1(A) * \pi_1(B)}{N} \to \pi_1(X) \iff \psi(N) = \{ e \}$.

    $$
    \begin{aligned}
        & \psi( \,i(\alpha_*(g)) j(\beta_*(g))^{-1}\, ) = \\
        &= \psi( \,i(\alpha_*(g))\, ) \psi( \,j(\beta_*(g^{-1}))\, ) \\
        &= f_*( \alpha_*(g) ) g_*( \beta_*(g^{-1}) ) \\
        &= (f \compose \alpha)_*(g) (g \compose \beta)_*(g^{-1}) \\
        &= (f \compose \alpha)_*(g) (g \compose \beta)_*(g^{-1}) \\
        &= (f \compose \alpha)_* (g g^{-1}) = \{ e \}\\
    \end{aligned}
    $$

    usando il fatto che $f \compose \alpha = g \compose \beta$. Ho ora che $\bar\psi : \sfrac{\pi_1(A) * \pi_1(B)}{N} \to \pi_1(X)$, voglio ora $\phi : \pi_1(X) \to \sfrac{\pi_1(A) * \pi_1(B)}{N}$.

    $$
    \xymatrixcolsep{2ex}
    \xymatrixrowsep{3ex}
    \xymatrix{
        & \pi_1(A) \ar[rd]^{f_*} \ar@/^1.5pc/[rrrd]^{i \compose \pi} &&& \\
        \pi_1(A \cap B) \ar[ru]^{\alpha_*} \ar[rd]_{\beta_*} && \pi_1(X) \ar@{-->}[rr]^{\exists! \phi} & \qquad & \snfrac{\pi_1(A) * \pi_1(B)}{N} \\
        & \pi_1(B) \ar[ru]_{g_*} \ar@/_1.5pc/[rrru]_{j \compose \pi} &&& \\
    }
    $$

    con $\bar \psi \compose \phi = \Id_{\pi_1(X)}$ e $\phi \compose \bar\psi = \Id_{\snfrac{\pi_1(A) * \pi_1(B)}{N}}$. \hfill$\square$
\end{corollary}

\textbf{Corollario/Proposizione.}
Se abbiamo che $\pi_1(A) = \prerel{S_1}{R_1}, \pi_1(B) = \prerel{S_2}{R_2}$ e $\pi_1(A \cap B) = \prerel{S_0}{R_0}$ allora

$$
\pi_1(X) \simeq \sfrac{\pi_1(A) * \pi_1(B)}{N} \implies \pi_1(X) \simeq \prerel{S_1 \cup S_2}{R_1 \cup R_2 \cup R_3}
$$

Vediamo ora la dimostrazione del teorema di Van Kampen, per prima cosa costruiamo il morfismo richiesto e poi vediamo che è ben definito.

\textit{Dimostrazione del Teorema.} 

\textbf{Costruzione.} Vorremmo costruire $\phi$ per far commutare il diagramma, sia $x \in X$ e denoto con $\alpha_x \in \Omega(x_0, x)$ cammino tale che

\begin{itemize}
    \item Se $x = x_0$: $\alpha_x = c_{x_0}$
    \item Se $x \in A$: $\alpha_x([0, 1]) \subseteq A$
    \item Se $x \in B$: $\alpha_x([0, 1]) \subseteq B$
\end{itemize}

Dato $\gamma \in \pi_1(X)$ con $\gamma$ rappresentante di $[\gamma]$ vogliamo definire esplicitamente $\phi([\gamma])$. Poiché il ricoprimento aperto $\{ \gamma^{-1}(A), \gamma^{-1}(B) \}$ ha numero di Lebesgue $\implies \exists$ suddivisione $0 = t_0 < t_1 < \cdots < t_n = 1$ tale che $\gamma([t_{i - 1}, t_i]) \subseteq A$ oppure $\gamma([t_{i - 1}, t_i]) \subseteq B$. 

Ora definiamo induttivamente dei pezzi $\gamma_i$ (per brevità indichiamo con $\alpha_i := \alpha_{\gamma(t_i)}$), definiamo $\gamma_i = \alpha_{i - 1} * \gamma {|}_{[t_{i - 1}, t_i]} * \bar\alpha_i$ con $\gamma_1 = \gamma{|}_{[0, t_1]} * \bar\alpha_1$ e $\gamma_n = \alpha_{n - 1} * \gamma{|}_{[t_{n - 1}, 1]}$ dunque segue subito che $\gamma \sim \gamma_1 * \cdots * \gamma_n$ in quanto i termini in mezzo si cancellano.

Allora $\phi([\gamma]) = g_1 \cdots g_n$ con

$$
g_i =
\begin{cases}
    h([\gamma_i]) & \text{se } \opn{Im} \gamma_i \subseteq A \\
    k([\gamma_i]) & \text{se } \opn{Im} \gamma_i \subseteq B \\
\end{cases}
$$

inoltre per costruzione $\phi$ è unica.

\textbf{Buona definizione.} Dobbiamo mostrare che i $\phi$ non dipende dai $t_i$ e $\phi$ non dipende dal rappresentante $\gamma$ di $[\gamma]$

\begin{itemize}
    
    \item $\phi$ non dipende dai $t_i$:

        Bisgona mostrare che $\phi$ per $0 = t_0 < \cdots < t_n = 1$ è uguale a $\phi$ per $0 = t_0 < \cdots < t_i < \bar t < t_{i+1} < \cdots < t_n = 1$. Sia $\beta = \alpha_{\gamma(t)}$ e siano $\delta_1 = \alpha_i * \gamma_{[t_i, \bar t]} * \bar\beta$ e $\delta_2 = \beta * \gamma_{[t_i, t_{i+1}]} * \bar \alpha_{i + 1}$ ora

        \begin{itemize}
            \item[1)] $\gamma_{i+1} \sim \delta_1 * \delta_2$ in $A$ se $\gamma([t_i, t_{i+1}]) \subseteq A \implies h([\gamma_{i+1}]) = h([\delta_1]) h([\delta_2])$
            \item[2)] $\gamma_{i+1} \sim \delta_1 * \delta_2$ in $B$ se $\gamma([t_i, t_{i+1}]) \subseteq B \implies k([\gamma_{i+1}]) = k([\delta_1]) k([\delta_2])$
        \end{itemize}

        e

        $$
        \rho_j =
        \begin{cases}
            h(\delta_j) & \text{Se 1)} \\
            k(\delta_j) & \text{Se 2)} \\
        \end{cases}
        $$

        se aggiungiamo $t_i < \bar t < t_{i + 1} \implies$ caso 1) o 2)

        $$
        \phi([\gamma]) = g_1 \cdots g_{i+1} \cdots g_n = g_1 \cdots g_i (\rho_1 \rho_2) g_{i+2} \cdots g_n
        $$

    \item $\phi$ non dipende dal rappresentante di $[\gamma]$ scelto: 

        Sia $[\gamma] \in \pi_1(X)$ e $\beta \sim \gamma$, sia $H : [0, 1] \times [0, 1] \to X$ omotopia tra $\gamma$ e $\beta$, poiché $\{ H^{-1}(A), H^{-1}(B) \}$ è un ricoprimento aperto in $[0, 1] \times [0, 1]$, $\exists n$ tale che

        $$
        \forall h, k \in \{ 0, \dots, n - 1 \}
        \qquad
        H\lrpa{\lrbt{\frac{k}{n}, \frac{k + 1}{n}} \times \lrbt{\frac{h}{n}, \frac{h + 1}{n}}} \subseteq A \text{ o } B
        $$

        \begin{center}
            \def\svgwidth{0.50\textwidth}
            \input{images/thvankampen-2.pdf_tex}
        \end{center}

        siano ora

        $$
        \begin{array}{ccc}
            \delta_{1,2} 
            = \alpha_{H(v_1)} * (H \compose l_{1,2}) * \overline{\alpha_{H(v_2)}} & \qquad &
            \delta_{1,2} 
            = \alpha_{H(v_1)} * (H \compose l_{1,4}) * \overline{\alpha_{H(v_4)}} \\
            \delta_{2,3} 
            = \alpha_{H(v_2)} * (H \compose l_{2,3}) * \overline{\alpha_{H(v_3)}} & \qquad &
            \delta_{4,3} 
            = \alpha_{H(v_4)} * (H \compose l_{4,3}) * \overline{\alpha_{H(v_3)}} \\
        \end{array}
        $$

        perciò $\delta_{1,2} * \delta_{2,3} \sim \delta_{1,4} * \delta_{4,3}$ in $A \implies h([\delta_{1,2}] [\delta_{2,3}]) = h([\delta_{1,4}] [\delta_{1,3}])$. Abbiamo che $\gamma \sim \gamma_1 * \cdots * \gamma_n \sim \gamma_1 * \cdots * \gamma_n * \delta$ (con $\delta$ come nella figura successiva) da cui segue che $\phi([\gamma]) = \phi([\gamma_1]) \dots \phi([\gamma_n]) = \phi([\gamma_1]) \dots \phi([\gamma_n]) \phi([\delta]) = \phi([\gamma_1]) \dots \phi([\gamma_{n - 1}]) \phi([\delta'])$.

        \begin{center}
            \def\svgwidth{0.9\textwidth}
            \input{images/thvankampen-3.pdf_tex}
        \end{center}

        Quindi ripetendo iterativamente quanto descritto otteniamo $\phi([\gamma]) = \cdots = \phi([\beta])$.

    \item $\phi$ è un omomorfismo:

        Siano $[\gamma_1], [\gamma_2] \in \pi_1(X)$, date suddivisioni per $\gamma_1$ e $\gamma_2$ possiamo costruire una suddivisione per $\gamma_1 * \gamma_2$. [TODO: Eplicitare il conto per esercizio]

    \item $\phi$ rende i due triangoli commutativi ovvero valgono $\phi \compose f_* = h, \phi \compose g_* = k$:

        Basta considerare

        \begin{itemize}
            \item Sia $\gamma$ con $\Im \gamma \subseteq A \implies t_0 = 0, t_1 = 1 \implies \phi([\gamma]) = h([\gamma])$
            \item Sia $\gamma$ con $\Im \gamma \subseteq B \implies t_0 = 0, t_1 = 1 \implies \phi([\gamma]) = k([\gamma])$
        \end{itemize}

    \item $\phi$ unica:

        sia $\phi' : \pi_1(X) \to G$ che soddisfa

        $$
        \phi' \compose f_* = h \qquad \phi' \compose g_* = k
        $$

        $$
        \implies \phi([\gamma]) = \phi([\gamma_1] \dots [\gamma_n]) = \phi([\gamma_1]) \dots \phi([\gamma_n]) = \phi'([\gamma_1]) \dots \phi'([\gamma_n])
        $$

        usando il punto precedente.
\end{itemize}
\hfill $\square$

\begin{corollary}
    Assumiamo $X$ con le ipotesi del teorema di Van Kampen. Se $A$ e $B$ sono semplicemente connessi $\implies X$ è semplicemente connesso.
\end{corollary}

\begin{example}
    $\forall n \geq 2 \quad \pi_1(S^n) \simeq \{ e \}$

    \begin{proof}
        Scegliamo opportunamente $A$ e $B$, consideriamo

        $$
        A = S^n \setminus \{ (1, 0, \dots, 0) \} \qquad B = S^n \setminus \{ (-1, 0, \dots, 0) \}
        $$

        da cui per la proiezione stereografica segue che $A, B \simeq \R^n$ dunque la tesi segue per il corollario precedente.
    \end{proof}
\end{example}

\begin{example}
    $\forall n \geq 2 \quad \pi_1(\P^n(\R)) \simeq \Z_2$

    \begin{proof}
        $S^n$ è un rivestimento universale di $\P^n(\R) \implies |\pi_1(\P^n(\R))| = 2$ che è il grado del rivestimento dunque la tesi segue poiché $\Z_2$ è l'unico gruppo di ordine $2$.
    \end{proof}
\end{example}

\begin{example}
    $\forall n \quad \pi_1(\P^n(\CC)) \simeq \{ e \}$

    \begin{proof}
        Vediamo per induzione su $n$

        \begin{enumerate}
            
            \item $n = 0$: $\P^n(\CC) = \{ \text{pt.} \}$ è banale.
            
            \item $(n - 1) \Rightarrow (n)$: Consideriamo l'iperpiano $H = \{ [x_0, \dots, x_n] \in \P^n(\CC) \mid x_0 = 0 \}$ e come aperti prendiamo

                $$
                A = \P^n(\CC) \setminus H \qquad B = \P^n(\CC) \setminus \{ [1, 0, \dots, 0] \}
                $$

                in particolare $A$ è semplicemente connesso in quanto $A \omeom \CC^n$ attraverso la mappa

                $$
                \begin{array}{ccc}
                    \P^n(\CC) \setminus H & \longrightarrow & \CC^n \\
                    {[} x_0, \dots, x_n] & \longmapsto & \left[ \frac{x_1}{x_0}, \dots, \frac{x_n}{x_0} \right]
                \end{array}
                $$

                quindi $\pi_1(A) = \{ e \}$. $A \cap B \omeom \CC^n \setminus \{ (0, \dots, 0) \} \implies$ connesso per archi.

                Mostriamo ora che $B \sim \P^{n-1}(\CC)$, notiamo che $H \subseteq B$ e mostriamo che esiste una retrazione per deformazione di $B$ su $H$. Sia $r \subseteq \CC^{n+1}$ la retta complessa generata da $(1, 0, \dots, 0)$ e definiamo

                $$
                \begin{array}{cccc}
                    h : & (\CC^{n+1} \setminus r) \times [0, 1] & \longrightarrow & \CC^{n+1} \setminus r \\
                    & ((x_0, \dots, x_n), t) & \longmapsto & (t x_0, \dots, x_n) \\
                \end{array}
                $$

                che passa al quoziente e $k : B \times [0, 1] \to B$ è la retrazione per deformazione cercata. Usando l'ipotesi induttiva $\pi_1(B) \simeq \{ e \} \implies \pi_1(\P^n(\CC)) \simeq \{ e \}$.
        \end{enumerate}
    \end{proof}
\end{example}

\subsection{Wedge di circonferenze}

Consideriamo $X$ come il sottospazio di $\R^2$ dato da $X = C_1 \cup C_2$ con

$$
C_1 = \{ (x, y) \in \R^2 \mid (x - 1)^2 + y^2 = 1 \}
\qquad
C_2 = \{ (x, y) \in \R^2 \mid (x + 1)^2 + y^2 = 1 \}
$$

consideriamo ora i due aperti $A = X \setminus \{ P \}$ e $B = X \setminus \{ Q \}$ con $P = (-2, 0)$ e $Q = (+2, 0)$.

\textbf{Esercizio.} Mostrare che esite una retrazione per deformazione di $A$ in $C_1$ e di $B$ in $C_2$. $A \cap B$ è omotopo a $S^1 \setminus \{ \text{pt.} \} \omeom \R \implies$ semplicemente connesso.

$$
\pi_1(X) \simeq \snfrac{\pi_1(A) * \pi_1(B)}{N} \simeq \pi_1(C_1) * \pi_1(C_2) \simeq \Z * \Z
$$

dove in particolare $N$ è banale per quanto visto prima.

\begin{definition}
    Sia $X$ uno spazio topologico allora $X$ si dice \textbf{wedge di circonferenze} se $\exists S_\alpha, \alpha \in I$ tale che

    \begin{enumerate}
        \item $\forall \alpha \in I \quad S_\alpha \subseteq X$ ed $\exists P \in X$ tale che $\forall \alpha \neq \beta \quad S_\alpha \cap S_\beta = \{ \text{pt.} \}$
        \item $\forall \alpha \in I \quad S_\alpha \omeom S^1$
        \item $U \subset X$ aperto $\iff \forall \alpha \quad U \cap S_\alpha$ è aperto in $S_\alpha$
    \end{enumerate}

    in questo caso si scrive $\displaystyle X = \bigvee_{\alpha \in I} S_\alpha$
\end{definition}

\begin{observation}
    La condizione iii) è automaticamente soddisfatta se $I$ è finito [TODO: esercizio]
\end{observation}

\begin{prop}
    Sia $\displaystyle X = \bigvee_{\alpha \in I} S_\alpha$ come sopra, allora

    \begin{itemize}
        \item $\pi_1(X, P)$ è un gruppo libero.
        \item Se $[\gamma_\alpha]$ è un generatore di $\pi_1(S_\alpha, P) \implies \{ [\gamma_\alpha] \mid \alpha \in I \}$ è un sistema di generatori liberi cioè $\pi_1(X, P) \simeq F(\{ [\gamma_\alpha] \mid \alpha \in I \})$
    \end{itemize}
\end{prop}

\begin{corollary}
    $\displaystyle X = \bigvee_{i = 1}^n S_i \implies \pi_1(X, P) \simeq F_n$ con $F_n := F(\{ a_1, \dots, a_n \})$.
\end{corollary}

\textbf{Esercizio.} Sia $S = \{ x_1, \dots, x_n \} \subset \R^2$ con $\forall i \neq j \quad x_i \neq x_j$, provare allora che $\pi_1(\R^2 \setminus S) \simeq F_n$.

\begin{example}
    Siano $r_1, \dots, r_n \subset \R^3$ delle rette distinte passanti per l'origine, allora $\displaystyle \R^3 \setminus \bigcup_{i = 1}^n$ si retrae per deformazione a 

    $$
    S^2 \setminus \left( S^2 \cap \bigcup_{i = 1}^n r_i \right) \omeom \R^2 \setminus \{ P_1, \dots, P_{2n-1} \} \implies \pi_1 \left(\R^3 \setminus \bigcup_{i = 1}^n r_i \right) \simeq F_{2n - 1}
    $$
\end{example}

\begin{example}
    Proviamo ora a vedere da un punto di vista geometrico che posto $\sigma_1 = S^1 \times S^1$ si ha

    $$
    \pi_1(\Sigma_1) = \pi_1(S^1 \times S^1) \simeq \pi_1(S^1) \times \pi_1(S^1) \simeq \Z \oplus \Z = \prerel{X, Y}{ X Y X^{-1} Y^{-1} }
    $$

    proviamo ad usare il teorema di Van Kampen, consideriamo il toro quoziente del quadrato in cui identifichiamo i lati opposti (presi nello stesso verso). Sia $T = [0, 1] \times [0, 1]$ e sia $y \in T$ ed indichiamo con $\pi$ la proiezione al quoziente

    $$
    \begin{gathered}
        \Sigma_1 = \snfrac{T}{\text{\textqt{lati opposti}}} \\
        A = \pi(T \setminus \{ y \}) \qquad B = \pi(T \setminus \partial T)
    \end{gathered}
    $$

    allora abbiamo che

    \begin{itemize}
        \item $A \sim S^1 \lor S^1$
        \item $B \sim \text{pt.}$
        \item $A \cap B \sim S^1$
    \end{itemize}

    da cui segue $\pi_1(\Sigma_1) \simeq \snfrac{\pi_1(A) * \pi_1(B)}{N} \simeq \snfrac{\Z * \Z}{N}$ [...]
\end{example}

\end{document}
