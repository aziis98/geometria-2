\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezioni 51-52-53 \& aggiornamenti del 10 Aprile 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\section{Indice di avvolgimento}

Sia  $a \in \CC$. Allora $\CC \setminus \{ a \}$ si retrae per deformazione su un cerchio di centro $a$ e raggio unitario.

Quindi $\pi_1(\CC \setminus \{ a \}, 1 + a) \simeq \Z$ ($1 + a \in \pd B(a, 1)$). Fissiamo un isomorfismo canonico $f : \pi_1(\CC \setminus \{ a \}, 1 + a) \to \Z$ che manda $[\gamma]$ in $1$, dove $\gamma$ è una parametrizzazione di $\pd B(a, 1)$ che percorre la circonferenza in senso antiorario.

$$
[S1, \CC \setminus \{ a \}] = \{ \text{ classi di omotopia libera di mappe continue } S^1 \to \CC \setminus \{ a \} \}
$$

Avete già visto che $\exists$ una bigezione naturale tra $\Omega(1 + a, 1 + a) \to \Omega(S^1, 1 + a)$ tale che $\gamma \mapsto \hat\gamma$.

[...]

Questa bigezione induce un omomorfismo

$$
\begin{array}{rccc}
    \pd : & \pi_1(\CC \setminus \{ a \}) & \longrightarrow & [S^1, \CC \setminus \{ a \}] \\
    & [\gamma] & \longmapsto & [\hat\gamma]
\end{array}
$$

dove $x_0 = 1 + a$. Inoltre

\begin{enumerate}
    \item $\pd$ è surgettiva
    \item $\pd([\gamma]) = \pd([\gamma']) \iff [\gamma]$ e $[\gamma']$ sono coniugati in $\pi_1(\CC \setminus \{ a \}, x_0)$. Dato che $\pi_1(\CC \setminus \{ a \}, x_0)$ è abeliano $\implies \pd$ è iniettiva. Definiamo la seguente composizione

    $$
    \psi : [S^1, \CC \setminus \{ a \}] \xrightarrow{\pd^{-1}} \pi_1(\CC \setminus \{ a \}, x_0) \xrightarrow{f} \Z
    $$ 
\end{enumerate}

\begin{definition}
    Sia $\gamma : [0, 1] \to \CC \setminus \{ a \}$ un cammino chiuso in $x_0$. Chiamiamo indice di $\gamma$ rispetto ad $a$ l'intero $\psi([\hat\gamma])$

    $$
    I(\gamma, a) := \psi([\hat\gamma])
    $$

    dove $\hat\gamma : S^1 \to \CC \setminus \{ a \}$ è la mappa indotta da $\gamma$.
\end{definition}

\begin{theorem}
    Sia $\gamma : [0, 1] \to \CC \setminus \{ a \}$ un cammino chiuso, allora

    $$
    I(\gamma, a) = \frac{1}{2\pi i} \int_{\gamma} \frac{1}{z - a} \dd z
    $$

    \begin{proof}
        
        Strategia: 1) Definiamo una mappa $\ds \gamma \xmapsto{\varphi} \frac{1}{2\pi i} \int_{\gamma} \frac{1}{z - a} \dd z$ e 2) mostreremo che $\varphi = \psi$

        Abbiamo già visto che $\dfrac{1}{z - a}$ è olomorfa in $\CC \setminus \{ a \} \implies \omega = \frac{1}{z - a} \dd z$ è chiusa $\implies$ possiamo integrare $\omega$ lungo curve $\gamma$ continue.

        Inoltre, avete già visto che se $\gamma'$ è liberamente omotopo a $\gamma$ allora $\ds \int_{\gamma} \omega = \int_{\gamma'} \omega$.

        Quindi la seguente mappa è ben definita

        $$
        \begin{array}{rccc}
            \varphi : & [S^1, \CC \setminus \{ a \}] & \longrightarrow & \CC \\
            & [\hat\gamma] & \longmapsto & \frac{1}{2 \pi i} \int_{\gamma} \omega 
        \end{array}
        $$

        Ci resta da dimostrare che $\varphi = \psi$, cioè  $\forall [\hat\gamma] \in [S^1, \CC \setminus \{ a \}] \quad \varphi([\hat\gamma]) = \psi([\hat\gamma])$. Notiamo che 

        $$
        [S^1, \CC \setminus \{ a \}] = \{ [\hat\gamma_n] \mid \gamma_n : [0, 1] \to \CC \setminus \{ a \}, \gamma_n : t \mapsto a + e^{2 \pi i n t} \} \implies \psi([\hat\gamma_n]) = n
        $$

        Abbiamo già visto d'altro canto che $\ds \int_{\gamma_n} \frac{1}{z - a} \dd z = 2 \pi i n$.
    \end{proof}
\end{theorem}

Abbiamo ottenuto che $\forall$ cammino chiuso $C^0, \gamma$ e $\ds a \notin \Im \gamma, \Sigma(\gamma, a) = \frac{1}{2\pi i} \int_{\gamma} \frac{1}{z - a} \dd z$

\begin{observation}
    Per definizione, $\omega = \dfrac{1}{z - a} \dd z$ è chiusa, cioè localmente esatta. Le primitive locali di $\dfrac{1}{z - a} \dd z$ sono date dalle branche di $\log(z - a)$. Una primitiva locale di $\dfrac{1}{z - a} \dd z$ lungo $\gamma$ è una funzione continua $f : [0, 1] \to \CC$ tale che $e^{f(t)} = \gamma(t) - a$

    $$
    \implies I(\gamma, a) = \frac{1}{2 \pi i} (f(1) - f(0))
    $$
\end{observation}

\textbf{Proprietà.}

\begin{itemize}
    \item $\gamma, \gamma'$ due curve $C^0$ chiuse in $\CC \setminus \{ a \}, \gamma$ è liberamente omotopo a $\gamma'$, $I(\gamma, a) = I(\gamma', a)$.

    \item Sia $\gamma : [0, 1] \to \CC$ una curva chiusa $C^0$. Allora la funzione

        $$
        \begin{array}{ccc}
            \CC \setminus \Im \gamma & \longrightarrow & \CC \\
            z & \longmapsto & I(\gamma, z)
        \end{array}
        $$

        è localmente costante (in particolare, è costante su ogni componente connessa di $\CC \setminus \Im \gamma$)

        \begin{proof}
            Fissiamo $z \in \CC \setminus \Im \gamma$. Mostriamo che $\forall h \in \CC, |h|$ sufficientemente piccolo, $\Sigma(\gamma, z + h) = I(\gamma, z)$.

            Sia $0 < \delta_0 < \min \{ |\gamma(t) - z| \mid t \in [0, 1] \}$, $\forall h \in \CC$ tale che $|h| < \delta_0$, abbiamo 

            $$
            \Sigma(\gamma, z+ h) = \frac{1}{2\pi i} \int_{\gamma} \frac{1}{z - (a + h)} \dd z = \frac{1}{2 \pi i} \int_{\gamma} \frac{\dd z}{(z - h) - a}
            $$

            Eseguendo il seguente cambio di variabili $z' = z - h$, otteniamo che

            $$
            I(\gamma, z + h) = \frac{1}{2 \pi i} \int_{\gamma'} \frac{1}{z' - a} \dd z' 
            $$

            dove $\gamma'(t) = \gamma(t) - h$ per $t \in [0, 1]$. Per conclude, mostreremo che l'espressione precedente

            $$
            = \frac{1}{2 \pi i} \int_{\gamma} \frac{1}{z - a} \dd z
            $$

            (mostreremo che $\gamma$ e $\gamma'$ sono liberamente omotope) $\gamma \sim \gamma'$ tramite l'omotopia $F(t, s) = \gamma(t) - sh$ per $t, s \in [0, 1] \implies I(\gamma, z + h) = I(\gamma', z) = I(\gamma, z)$
        \end{proof}
    \item Sia $a \in \CC$. Sia $\gamma$ una curva chiusa $C^0$ tale che $\Im \gamma \subset D \subset \CC \setminus \{ a \}, D$ è un aperto \textit{semplicemente connesso} $\implies I(\gamma, a) = 0$

        \begin{proof}
            $\omega = \dfrac{1}{z - a} \dd z$ è chiusa in $D \implies \omega$ è esatta $\ds \implies \int_{\gamma} \omega = 0 \implies I(\gamma, a) = 0$     
        \end{proof}

    \item Sia $\gamma$ una curva chiusa $C^0$. Sia $a \in \CC \setminus \Im \gamma$. Allora $I(\gamma, a) = 0$ per tutti gli $a$ in una componente connessa illimitata di $\CC \setminus \Im(\gamma)$ 
\end{itemize}

\begin{example}
    Sia $\gamma : t \mapsto R e^{it}$ con $R > 0$ e $t \in [0, 2\pi]$

    \begin{itemize}
        \item $|z| < R, I(\gamma, z) = 1$
        \item $|z| > R, I(\gamma, z) = 0$
    \end{itemize}
\end{example}

\begin{prop}
    Sia $f : \{ z \in \CC \mid |z| \leq R \} \to \CC$ una mappa continua, sia $\gamma(t) := f(R e^{2 \pi i t})$ per $t \in [0, 1]$. 

    Se $a \notin \Im \gamma$ e $I(\gamma, a) \neq 0$ allora $\exists z \in \CC, |z| < R$ tale che $f(z) = a$.

    \begin{proof}
        \textit{Per assurdo,} assumiamo che $\forall |z| < R \quad f(z) \neq a$, allora $\forall |z| \leq R \ f(z) \neq a$ visto che $a \notin \Im \gamma$. 

        Definiamo $F(t, s) := f(t R e^{2\pi i s}) \quad \forall t, s \in [0, 1]$. $F$ ci dà un'omotopia tra $\gamma$ e il cammino costante $f(0)$. Infatti $F$ è continua e $F(1, \curry) = \gamma(\curry), F(0, \curry) = F(0), F(\curry, 0) = F(\curry, 1)$ e $\Im F \subseteq \CC \setminus \{ a \}$

        Dunque $\gamma \sim$ cammino costante in $\ds f(0) \implies \int_{\gamma} \frac{1}{z - a} \dd z = 0$ ma $I(\gamma, a) \neq 0$ $\lightning$
    \end{proof}
\end{prop}

\begin{definition}
    Siano $\gamma_1, \gamma_2$ due curve $C^0$. Allora definiamo somma e prodotto di curve

    $$
    \begin{gathered}
        \gamma_1 + \gamma_2 : t \mapsto \gamma_1(t) + \gamma_2(t) \\
        \gamma_1 \gamma_2 : t \mapsto \gamma_1(t) \cdot \gamma_2(t)
    \end{gathered}
    $$
\end{definition}

\begin{theorem}
    Siano $\gamma_1, \gamma_2$ due curve $C^0$ chiuse tali che $0 \notin \Im \gamma_1, \Im \gamma_2$, allora $I(\gamma_1 \gamma_2, 0) = I(\gamma_1, 0) + I(\gamma_2, 0)$

    \begin{proof}
        Sia $\omega = \dfrac{\dd z}{z}$ e $\gamma_i : [0,  1] \to \CC^2 \setminus \{ 0 \}$ per $i = 1, 2$. Sia $f_i : [0, 1] \to \CC$ una funzione continua tale che $\forall t \in [0, 1], i = 1, 2 \quad e^{f_i(t)} = \gamma_i(t)$. Allora $\gamma_1(t) \gamma_2(t) = e^{f_1(t)} e^{f_2(t)} = e^{f_1(t) + f_2(t)} \implies f = f_1 + f_2$ è una primitiva di $\omega$ lungo $\gamma_1\gamma_2$.

        $$
        I(\gamma_1 \gamma_2, 0) = \frac{f_1(1) + f_2(1) - f_1(0) - f_2(0)}{2\pi i} = I(\gamma_1, 0) + I(\gamma_2, 0)
        $$
    \end{proof}
\end{theorem}

\begin{theorem}
    Siano $\gamma_1, \gamma_2$ curve chiuse $C^0$ tali che $0 \notin \Im \gamma_1, \Im \gamma_2$. Assumiamo che $\forall t \in [0, 1] \quad 0 < |\gamma_1(t)| < |\gamma(t)|$ allora

    $$
    I(\gamma_1 + \gamma, 0) = I(\gamma, 0)
    $$

    \begin{proof}
        $\ds \gamma(t) + \gamma_1(t) = \gamma(t) \underbrace{\lrpa{1 + \frac{\gamma_1(t)}{\gamma(t)}}}_{\beta(t)} = \gamma(t) \beta(t) \implies I(\gamma_1 + \gamma, 0) = I(\gamma \beta, 0) = I(\gamma, 0) + I(\beta, 0)$

        Notiamo che $I(\beta, 0) = 0$, infatti 
        
        $$
        |\beta(t) - 1| 
        = \abs[\bigg]{1 + \frac{\gamma_1(t)}{\gamma(t)} - 1}
        = \abs[\bigg]{\frac{\gamma_1(t)}{\gamma(t)}} < 1
        $$

        $\implies \Im \beta \subset D(1, 1) \implies \Im \beta$ che è contenuta in un aperto semplicemente connesso $\implies I(\beta, 0) = 0$
    \end{proof}
\end{theorem}

\begin{theorem}
    \textbf{Formula Integrale di Cauchy.}

    Sai $D \subset \CC$ un aperto, $a \in D$, sia $\gamma : [0, 1] \to D$ un cammino chiuso omotopicamente banale, con $a \notin \Im \gamma$. Sia $f : D \to \CC$ una funzione olomorfa. Allora

    $$
    \frac{1}{2\pi i} \int_{\gamma} \frac{f(z)}{z - a} \dd z = I(\gamma, a) f(a)
    $$

    \begin{proof}
        Per ogni $z \in D$ definiamo

        $$
        g(z) =
        \begin{cases}
            \dfrac{f(z) - f(a)}{z - a} & \text{ se } z \neq a \\
            f'(a) & \text{ se } z = a
        \end{cases}
        $$

        $g$ è continua in $D$. Consideriamo ora la retta orizzontale $r$ che passa per $a$ allora $g$ è anche olomorfa in $D \setminus r$ quindi per un teorema visto in precedenza, $g(z) \dd z$ è chiusa in $D$. Visto che $\gamma$ è omotopicamente banale in $D$ e $g(z) \dd z$ è chiusa in $D$, abbiamo che $\int_{\gamma} g(z) \dd z = 0$.

        $$
        0 = \int_{\gamma} g(z) \dd z = \int_{\gamma} \frac{f(z) - f(a)}{z - a} \dd z = \int_{\gamma} \frac{f(z)}{z - a} \dd z - f(a) \int_{\gamma} \frac{1}{z - a} \dd z
        $$

        $\implies$ dividendo per $2 \pi i$, otteniamo la tesi.
    \end{proof}
\end{theorem}

\begin{example}
    Sia $R > 0$ tale che

    $$
    B(a, R) = \{ z \in \CC \mid |z - a| \leq \R \} \subseteq D
    $$

    e sia $\gamma$ una parametrizzazione di $\pd B(a, R)$ in senso antiorario. Applicando la formula integrale di Cauchy, otteniamo che

    $$
    \frac{1}{2\pi i} \int_{\gamma} \frac{f(z)}{z - a} \dd z = f(a)
    $$

    per qualche funzione olomorfa $f : D \to \CC$.
\end{example}

\begin{observation}
    Formula integrale di Cauchy $\implies$ il teorema di Cauchy.

    \begin{proof}
        Sia $f$ una funzioni olomorfa su un aperto $D \subseteq \CC$. Vogliamo provare che $\omega = f(z) \dd z$ è chiusa in $D$. Sia $\gamma$ omotopicamente banale in $D$ e $a \notin \Im \gamma$.

        $F(z) = (z - a) f(z)$. Applichiamo la formula integrale di Cauchy a $F(z)$:

        $$
        0 = F(a) \Sigma(\gamma, a) = \frac{1}{2\pi i} \int_{\gamma} \frac{F(z)}{z - a} \dd z = \frac{1}{2\pi i} \int_{\gamma} f(z) \dd z 
        $$

        Abbiamo provato che $\forall \gamma$ omotopicamente banale $\int_{\gamma} \omega = 0$, $\Im \gamma \subseteq D \implies \omega$ è chiusa.
    \end{proof}
\end{observation}

\begin{theorem}
    Sia $D \subseteq \CC$ un aperto e $\omega$ 1-forma differenziabile, allora sono equivalenti

    \begin{itemize}
        \item $\omega$ è chiusa
        \item $\ds \forall R \subseteq D$ rettangolo $\int_{\pd R} \omega = 0$ 
        \item $\forall$ omotopicamente banale, $\Im \gamma \subseteq D \ds \int_{\gamma} \omega = 0$
    \end{itemize}
\end{theorem}

\begin{theorem}
    $f$ olomorfa $\implies f$ analitica

    Sia $f$ una funzione olomorfa nel disco aperto $\{ z \in \CC \mid |z| < R \}$ per $0 < R \leq +\infty$. Allora $f$ è analitica in $\{ z \in \CC \mid |z| < R \}$. In particolare $\exists$ una serie di potenze $\sum_{n \geq 0} a_n z^n$ con raggio di convergenza $\rho \geq R$ tale che

    $$
    f(z) = \sum_{n \geq 0} a_n z^n
    $$

    per $|z| < R$.

    \begin{proof}
        Sia $0 < r_0 < R$. Definiamo $\forall t \in [0, 1], \gamma : t \mapsto r_0 e^{2\pi i t}$. Sia $z \in \CC$ tale che $|z| < r_0$ e poniamo $r = |z|$. Allora $I(\gamma, z) = 1$ e la formula integrale di Cauchy ci dice che

        $$
        f(z) = \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w - z} \dd w
        $$

        \textbf{Strategia.} Riscrivere l'integrando come una serie di potenze e poi scambiare l'ordine tra la somma e l'integrazione. 

        Notiamo che $\forall t \in [0, 1] \; |z| = r < r_0 = |\gamma(t)|$, in particolare:

        $$
        \frac{1}{w - z} = \frac{1}{w} \frac{1}{1 - \frac{z}{w}} = \frac{1}{w} \sum_{n \geq 0} \lrpa{\frac{z}{w}}^n
        $$
        $$
        \frac{1}{w - z} - \frac{1}{w}\frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} = \frac{1}{w}\frac{1 - \lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} = \frac{1}{w} \sum_{k=0}^n \lrpa{\frac{z}{w}}^k
        $$
        $$
        \frac{1}{w - z} = \frac{1}{w} \sum_{k=0}^n \lrpa{\frac{z}{w}}^k + \frac{1}{w} \frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}}
        $$

        ed adesso sostituendoci la formula integrale di Cauchy otteniamo

        $$
        f(z) = \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w - z} \dd z = \frac{1}{2 \pi i} \lrbt{\int_{\gamma} \frac{f(w)}{w} \sum_{k=0}^n \lrpa{\frac{z}{w}}^k \dd w + \int_{\gamma} \frac{f(w)}{w} \frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} \dd w }
        $$
        $$
        = \frac{1}{2 \pi i} \lrbt{\sum_{k=0}^n z^k \lrpa{\int_{\gamma} \frac{f(w)}{w^{k+1}} \dd w } + \int_{\gamma} \frac{f(w)}{w} \frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} \dd w }
        $$

        Definiamo ora $\ds a_k := \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w^{k+1}} \dd w$

        $$
        R_n = \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w} \frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} \dd w 
        $$

        Abbiamo così ottenuto $f(z) = \ds \sum_{k=0}^n a_k z^k + R_n$, vogliamo ora studiare $|R_n|$ e provare a darne una stima. Per fare questo definiamo

        $$
        M(r_0) := \sup \{ |f(\gamma(t))| \mid t \in [0, 1] \}
        $$

        che è $< +\infty$ perché $|f(\gamma(\curry))|$ è una funzione continua su un compatto.

        $$
        R_n = \frac{1}{2\pi i} \int_{\gamma} \frac{f(w)}{w} \frac{\lrpa{\frac{z}{w}}^{n+1}}{1 - \frac{z}{w}} \dd w = \frac{1}{2 \pi i} \int_{0}^{1} \frac{f(\gamma(t))}{\gamma(t)} \frac{\lrpa{\frac{z}{\gamma(t)}}^{n+1}}{1 - \frac{z}{\gamma(t)}} r_0 2 \pi i e^{2 \pi i t} \dd t
        $$

        $$
        |R_n| \leq \frac{1}{2\pi} \int_{0}^{1} \frac{|f(\gamma(t))|}{\gamma(t)} \frac{\abs{\frac{z}{\gamma(t)}}^{n+1}}{\abs{1 - \frac{z}{\gamma(t)}}} 2 \pi r_0 \dd t
        $$

        Usiamo che $|z| = r < r_0 = |\gamma(t)| \implies \ds \abs{1 - \frac{z}{\gamma(t)}} \geq 1 - \frac{r}{r_0}$

        $$
        |R_n| \leq M(r_0) \frac{\lrpa{\frac{r}{r_0}}^{n+1}}{1 - \frac{r}{r_0}}
        $$

        Quindi $|R_n| \leq M(r_0) \frac{\lrpa{\frac{r}{r_0}}^{n+1}}{1 - \frac{r}{r_0}}$ e per $n \to \infty \implies |R_n| \to 0 \implies f(z) = \sum_{k \geq 0} a_k z^k$ per $|z| < R$.

        Osserviamo che $\forall r_0 \in (0, R) \quad a_k = \ds\frac{1}{2 \pi i} \int_{\gamma} \frac{f(w)}{w^{k+1}} \dd w$ è indipendente da $r_0$. Ora usando lo stesso ragionamento di prima otteniamo

        $$
        |a_k| \leq \frac{1}{2 \pi} \int_{0}^{1} \frac{|f(\gamma(t))|}{|\gamma(t)|^{k+1}} r_0 2 \pi \dd t \leq \frac{M(r_0)}{r_0^k} 
        $$
        $$
        \implies \forall k \geq 0 \quad |a_k| \leq \frac{M(r_0)}{r_0^k}
        $$

        Adesso usiamo il lemma di Abel, rienunciamolo

        \textbf{Lemma di Abel.} Siano $r, r_0$ numeri reali tali che $0 < r < r_0$. Se esiste $0 < M < +\infty$ tale che $\forall k \geq 0 \quad |a_k| r_0^k \leq M$. Allora la serie $\ds \sum_{k \geq 0} a_k z^k$ converge \textit{normalmente} per $|z| \leq r$ (cioè $\sum_{k \geq 0} \norm{a_k z^k} < \infty$)

        Lemma di Abel $\Rightarrow \ds\sum_{k \geq 0} a_k z^k$ converge uniformemente per $|z| \leq r$ e converge assolutamente per $|z| < r_0$.

        Dunque applicando questo lemma nella al nostro caso otteniamo che $\forall r_0 \in (0, R)$ la serie $\ds \sum_{k \geq 0} a_k z^k$ converge assolutamente in $|z| < r_0$ e dunque che il raggio di convergenza $\rho$ della serie è $\rho \geq R$.
    \end{proof}
\end{theorem}

\begin{observation}
    $\ds f(z) = \sum_{k \geq 0} a_k z^k \implies 2 \pi i f(z) = \ds \sum_{k \geq 0} \lrpa{\int_{\gamma} \frac{f(w)}{w^{k+1}} \dd w} z^k = \int_{\gamma} f(w) \lrpa{\sum_{k \geq 0} \frac{z^k}{w^{k+1}}} \dd w$.
\end{observation}

\begin{corollary}
    Sia $D \subseteq \CC$ un aperto. Una funzione $f$ è olomorfa in $D \iff f$ è analitica in $D$. In particolare, la serie di Taylor di $f$ in $a \in D$ ha raggio di convergenza $\rho \geq \sup \{ r > 0 \mid \{ z \in \CC \mid |z - a| < r \} \subseteq D \}$ 
\end{corollary}

\begin{corollary}
    Se $f$ è una funzione olomorfa su un aperto $D$, allora $f$ è $C^\infty$ in $D$ e $\forall n \; f^{(n)}$ è olomorfa in $D$.

    \begin{proof}
        $f$ olomorfa $\implies f$ analitica $\implies f \text{ è } C^\infty \land f^{(n)} \text{ è analitica} \implies f^{(n)} \text{ è olomorfa}$
    \end{proof}
\end{corollary}

\begin{corollary}
    Sia $f$ una funzione olomorfa in un aperto $D$. Sia $R > 0$ tale che $B(a, R) \subset D$. Sia $\gamma$ la curva che parametrizza $\pd B(a, R)$ in senso antiorario. Allora $\ds f^{(n)}(a) = \frac{n!}{2 \pi i} \int_{\gamma} \frac{f(z)}{(z - a)^{n + 1}} \dd z$

    \begin{proof}
        $\ds\frac{f^{(n)(a)}}{n!} = a_n = \frac{1}{2\pi i} \int_{\gamma} \frac{f(z)}{(z - a)^{n+1}} \dd z$
    \end{proof}
\end{corollary}

\textbf{Teorema di Morera (implicazione opposta al teorema di Cauchy).}
Sia $f$ una funzione continua in un aperto $D \subseteq \CC$. Se $\omega = f(z) \dd z$ è chiusa in $D \implies f$ è olomorfa in $D$.

\begin{proof}
    $\omega$ è chiusa in $D$. Per definizione $\forall a \in D \; \exists$ un intorno aperto $U$ di $a$ tale che $\omega = \dd F$ in $U, F$ di classe $C^1$ in $U$. ($F$ primitiva locale di $\omega$)

    $$
    f(z) \dd z = \omega = \dd F = \frac{\pd F}{\pd z} \dd z + \frac{\pd F}{\pd \bar z} \dd \bar z \iff f(z) = \frac{\pd F}{\pd z} \text{ e } \frac{\pd F}{\pd \bar z} = 0
    $$

    $\dfrac{\pd F}{\pd \bar z} = 0 \iff F$ soddisfa le condizioni di Cauchy-Riemann $\implies F$ è olomorfa $\implies F'$ è olomorfa (per il corollario precedente), ma $F' = f \implies f$ è olomorfa.
\end{proof}

Utilizzando il teorema di Morera insieme alla proposizione \textqt{$f : D \to \CC, D \subset \CC$ aperto, $f \in C^0(D), f$ olomorfa in $D \setminus r$ retta orizzontale $\implies f(z) \dd z$ chiusa.}

\begin{corollary}
    Sia $f : D \to \CC$ continua in un aperto $D$ e olomorfa in $D \setminus r$ con $r$ retta orizzontale $\implies f$ è olomorfa.
\end{corollary}

\begin{theorem}
    Sia $f : D \to \CC$ una funzione su un aperto $D \subset \CC$. Le seguenti affermazioni sono equivalenti

    \begin{itemize}
        \item $f$ olomorfa in $D$
        \item $f$ è analitica in $D$
        \item $f$ è di classe $C^1$ e soddisfa le condizioni di Cauchy-Riemann in $D$
        \item $f$ è di classe $C^0$ in $D$ e olomorfa in $D \setminus r$ con $r$ retta orizzontale.
        \item $f$ è di classe $C^0$ e $\omega = f(z) \dd z$ è chiusa in $D$. 
    \end{itemize}
\end{theorem}

Una disuguaglianza vista in precedenza implica

\begin{corollary} \textbf{Disuguaglianza di Cauchy.}
    Sia $a \in \CC$ e assumiamo che $f(z) = \sum_{n \geq 0} a_n (z - a)^n$ ha raggio di convergenza $\rho$, allora $\forall r \in (0, \rho)$ si ha $\ds a_n = \frac{f^{(n)}}{n!}$ e $|a_n| r^n \leq M(r)$ dove $M(r) = \sup \{ |f(z)| \mid |z - a| = r \}$.
\end{corollary}

\begin{corollary}
    Sia $a \in \CC$ e assumiamo che $\ds f(z) = \sum_{n \geq 0} a_n (a - a)^a$ ha raggio di convergenza $\rho > 0$. Allora $\ds a_n = \frac{f^{(n)}(a)}{n!}$ e $|a_n| r^n \leq M(r)$ per $r \in (0, \rho)$.
\end{corollary}

\textbf{Teorema di Liouville.}
Una funzione intera limitata è costante. (ovvero $f : \CC \to \CC$ olomorfa su tutto il dominio)

\begin{proof}
    Visto che $f$ è intera, $\ds f(z) = \sum_{n \geq 0} a_n z^n$ con raggio di convergenza $\rho = \infty$ e $\forall r > 0, \forall n \geq 0 \quad |a_n| r^n \leq M(r)$.

    Quindi possiamo ulteriormente maggiorare la disuguaglianza e ottenere

    $$
    \forall r > 0, \forall n \geq 0 \qquad |a_n| \leq \frac{M}{r^n}
    $$

    per $r \to \infty \implies \forall n \geq 1 \; |a_n| \to 0 \implies f(z) = a_0 = \text{cost.}$
\end{proof}

\subsection{Applicazione del Teorema di Liouville}

\textbf{Teorema fondamentale dell'Algebra.}
Sia $p(z) \in \CC[z]$ un polinomio non costante. Allora esso ammette almeno uno zero.

\begin{proof}
    \textit{Per assurdo.} $\ds \forall z \in \CC, p(z) \neq 0 \implies \frac{1}{p(z)}$ è una funzione intera.

    Vorremmo applicare il teorema di Liouville per giungere ad una contraddizione e per fare ciò vorremmo provare che $\dfrac{1}{p(z)}$ sia limitata.

    Adesso assumiamo che $p(z)$ sia della forma

    $$
    p(z) = a_n z^n + \cdots + a_1 z + a_0
    $$

    con $a_n \neq 0$, quindi possiamo riscrivere $p(z)$ come

    $$
    p(z) = z^n \lrpa{a_n + \frac{a_{n-1}}{z} + \frac{a_{n-1}}{z^2} + \dots + \frac{a_0}{z}}
    $$

    per $|z| \to \infty \implies |p(z)| \to \infty$, questo usando la disuguaglianza $|p(z)| = |z|^n \cdot \abs[\big]{a_n + \frac{a_{n+1}}{z} + \dots + \frac{a_0}{z^n}}$. Quindi visto che $|P(z)| \to \infty \implies \abs[\bigg]{\dfrac{1}{p(z)}} \to 0$. 

    Questo implica che $\exists R > 0$ tale che $\abs[\bigg]{\dfrac{1}{p(z)}}$ è una funzione limitata $\forall |z| > R$. D'altro canto, anche la funzione $\abs[\bigg]{\dfrac{1}{p(z)}}$ definita su $\{ z \mid |z| \leq R \}$ è limitata $\implies \abs[\bigg]{\dfrac{1}{p(z)}}$ è una funzione limitata, quindi $\dfrac{1}{p(z)}$ è limitata.

    Applicando il teorema di Liouville alla funzione $\dfrac{1}{p(z)}$ otteniamo così che essa è costante $\implies p(z)$ è costante, in contraddizione con l'ipotesi che $p(z)$ fosse non costante.
\end{proof}

\begin{definition}
    Sia $D \subseteq \CC$ aperto e $f : D \to \CC$ una funzione continua, allora diciamo che $f$ ha la \textbf{proprietà del valor medio} se $\forall a \in D \; \exists r_0 > 0$ tale che $\forall r \in [0, r_0)$ valgano le seguenti

    \begin{enumerate}
        \item $\{ z \mid |z - a| < r_0 \} \subseteq D$
        \item $\ds f(a) = \frac{1}{2 \pi} \int_{0}^{2\pi} f(a + re^{i\theta}) \dd \theta$
    \end{enumerate}
\end{definition}

\begin{exercise}
    Se $f$ verifica la proprietà del valor medio allora anche $\Re f, \Imm f$ la hanno.
\end{exercise}

\begin{observation}
    Se $f$ olomorfa $\implies f$ ha la proprietà del valor medio.

    \begin{proof}
        Sia $f$ olomorfa in $D$ e $a \in D$. Abbiamo visto che

        $$
        f^{(n)}(a) = \frac{n!}{2\pi i} \int_{\gamma} \frac{f(z)}{(z - a)^{n+1}} \dd z
        $$

        per $\gamma : \theta \mapsto a + r_0 e^{i \theta}$ per $\theta \in [0, 2\pi]$ per $r_0$ tale che $\{ z \mid |z - a| \leq r_0 \} \subset D$.

        Per $n = 0$ svolgendo l'integrale, otteniamo

        $$
        f(a) = \frac{1}{2 \pi i} \int_{\gamma} \frac{f(z)}{z - a} \dd z
         = \frac{1}{2 \pi i} \int_{0}^{2\pi} \frac{f(a + r_0 e^{i \theta})}{a + r_0 r^{i \theta} - a} r_0 i e^{i \theta} \dd \theta 
        $$
    \end{proof}
\end{observation}

\begin{theorem}
    \textbf{Principio del massimo modulo.}
    Sia $D \subset \CC$ un aperto e sia $f : D \to \CC$ una funzione continua che ha la proprietà del valor medio. Se $|f|$ ha massimo relativo in un punto $a \in D$, allora $f$ è costante in un intorno di $a$.

    \begin{proof}
        Se $f(a) = 0$ e $|f(z)| \leq |f(a)| = 0$ $\forall z$ sufficientemente vicino ad $a \implies f(z) = 0$ $\forall z$ sufficientemente vicino ad $a \implies f$ è costante in un intorno di $a$.

        Adesso bisogna considerare il caso $f(a)$ diverso da zero. Se assumiamo che $f(a)$ è della forma $f(a) = \alpha e^{i \beta} \implies e^{-i \beta} f(a) \in R$ e $>0$. Quindi a meno di rimpiazzare $f$ con $e^{-i\beta} f$, posso assumere che $f(a) \in \R, f(a) > 0$.

        Da adesso in poi assumiamo che $f(a) \in \R, f(a) > 0$. Per notazione $u := \Re(f), v := \Imm(f)$

        Adesso scegliamo $r_0 > 0$ tale che le seguenti 3 condizioni sono soddisfatte

        \begin{itemize}
            \item $B(a, r_0) = \{ z \in \CC \mid |z - a| \leq r_0 \} \subset D$
            \item $\forall r \in [0, r_0) \;\ds f(a) = \frac{1}{2\pi} \int_{0}^{2\pi} f(a + re^{i \theta}) \dd \theta$
            \item $\forall z \in B(a, r_0) \quad |f(z)| \leq |f(a)|$
        \end{itemize}

        Definiamo $M(r) := \sup \{ |f(z)| \mid |z - a| = r \}$ che per ogni $r \in [0, r_0)$ è $< \infty$. Dall'ultima condizione segue che

        $$
        \forall r \in [0, r_0) \quad M(r) \leq |f(a)| = f(a)
        $$

        per la seconda proprietà.

        $$
        \forall 0 \leq r < r_0 \quad f(a) = \frac{1}{2\pi} \int_{0}^{2\pi} f(a + r e^{i\theta}) 
        = |f(a)| \leq \frac{1}{2\pi} \int_{0}^{2\pi} |f(a + re^{i \theta})| \dd \theta \leq \frac{1}{2 \pi} \int_{0}^{2\pi} M(r) \dd \theta = M(r) 
        $$

        $\implies \forall r \in [0, r_0) \quad f(a) = M(r)$

        $$
        \frac{1}{2\pi} \int_{0}^{2\pi} M(r) \dd \theta = M(r) = f(a) = \frac{1}{2\pi} \int_{0}^{2\pi} u(a + r e^{i \theta}) \dd \theta 
        $$

        $$
        \int_{0}^{2\pi} [M(r) - u(a + re^{i\theta})] \dd \theta = 0
        $$

        Sia $g(\theta) := M(r) - u(a + re^{i\theta})$ tale che $\ds \int_{0}^{2\pi} g(\theta) \dd \theta = 0$ ma $g(\theta) \geq 0$

        Cioè abbiamo definito una funzione $g$ non negativa su $[0, 2\pi]$ tale che il suo integrale su $[0, 2\pi]$ è nullo $\implies \forall \theta \; g = 0$.

        quindi $\forall \theta \in [0, 2\pi] \; M(r) = u(a + r e^{i \theta})$. Usando ancora una volta la definizione di $M(r)$ abbiamo che

        $$
        M(r) \geq |f(a + r e^{i\theta})| = (u(a + re^{i\theta})^2 + v(a + re^{i\theta})^2)^{\frac{1}{2}} = (M(r) ^ 2 + v(a + e^{i\theta})^2)^{\frac{1}{2}}
        $$

        $\implies \forall \theta \in [0, 2\pi], \forall r \in [0, r_0), v(a + r e^{i\theta}) = 0$. Concludendo $\forall z$ tale che $|z - a| < r_0$ abbiamo $f(z) = u(z) = M(|z|) = f(a)$.
    \end{proof}
\end{theorem}

\begin{corollary}
    Sia $D \subset \CC$ un aperto connesso e limitato. Sia $f$ una funzione continua su $\overline D$ che abbia la proprietà del valor medio su $D$. Se $M = \sup \{ |f(z)| \mid z \text{ nel bordo di } D \}$ allora

    \begin{itemize}
        \item $\forall z \in D \; |f(z)| \leq M$
        \item Se $\exists a \in D$ tale che $|f(a)| = M$, allora $f$ è costante.
    \end{itemize}
\end{corollary}

\begin{corollary}
    \textbf{Principio del massimo modulo per funzioni olomorfe.}
    Sia $f$ una funzione olomorfa su un aperto connesso $D \subseteq \CC$. Se $f$ non è costante, allora $|f|$ non ha massimo relativo in $D$. Inoltre, se $D$ è limitato e $f$ è continua in $\overline D$, allora $|f|$ assume massimo nel bordo di $D$.

    \begin{proof}
        Sia $f$ olomorfa su $D$. Allora $f$ verifica la proprietà del valor medio. Assumiamo che $|f|$ ha un massimo relativo in $D \implies$ per il principio del massimo modulo $\exists a \in D$ tale che $f$ è costante in un intorno di $a \implies$ per il principio di continuazione analitica, $f$ è costante in $D$. 

        Assumiamo che $D$ è limitato e applichiamo il corollario. 
    \end{proof}
\end{corollary}

\begin{observation}
    Sia $f$ una funzione olomorfa in $\{ z \in \CC \mid |z| < r \}$ e continua in $\{ |z| \leq r \}$, allora $\forall z$ tale che $|z| \leq r \quad |f(z)| \leq M(r) = \sup \{ |f(z)| \mid |z| = r \}$
\end{observation}

\end{document}
