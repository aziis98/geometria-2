\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{
 a4paper,
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm
}

\input{prelude}

\title{Geometria 2}
\author{Antonio De Lucreziis}
\date{\small Lezione \& aggiornamenti del 1 Aprile 2020}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,      
    urlcolor=cyan,
}
 
\urlstyle{same}
 
\begin{document}

\maketitle

\parskip 1ex
\setlength{\parindent}{0pt}

\textbf{Esercizio.} Ad esempio se $D$ è semplicemente connesso, $\exists$ una branca del logaritmo definita su tutto $D$: basta prendere un'opportuna primitiva di $\dfrac{\dd z}{z}$

\begin{theorem}
    Sia $D \subseteq \CC$ aperto ed $\omega$ una 1-forma su $D$ allora i seguenti sono fatti equivalenti:

    \begin{enumerate}
        \item $\omega$ è chiusa
        \item $\forall R$ rettangolo con $R \subseteq D, \ds\int_{\pd R} \omega = 0$
        \item $\forall \gamma$ lacci continuo omotopo al laccio costante, $\ds \int_{\gamma} \omega = 0$
    \end{enumerate}

    \begin{proof} \ 
        \begin{itemize}
            \item[$\boxed{\text{i)} \Rightarrow \text{iii)}}$] Se $\gamma \sim c$ a estremi fissi dove $c$ è un cammino costante, allora $\ds \int_{\gamma} \omega = \int_{c} \omega = 0$

            \item[$\boxed{\text{iii)} \Rightarrow \text{ii)}}$] Ovvio, poiché $\pd R$ è un cammino omotopicamente banale.

            \item[$\boxed{\text{ii)} \Rightarrow \text{i)}}$] Visto in precedenza.
        \end{itemize}
    \end{proof}
\end{theorem}

Ricordiamo che ogni cammino chiuso $\gamma : [0, 1] \to D$ definisce un loop $\hat\gamma : S^1 \to D$, e $\gamma$ si dice \textbf{liberamente omotopo} ad $\alpha$ se $\exists H : [0, 1] \times [0, 1] \to D$ continua con

$$
\forall t \in [0, 1] \quad H(t, 0) = \gamma(t), H(t, 1) = \alpha(t) \\
\forall s \in [0, 1] \quad H(0, s) = H(1, s)
$$

(ovvero $\forall s \; H(\hole, s)$ è un cammino chiuso) Però non chiedo che $H(0, s), H(1, s)$ siano costanti in $s$. Ricordiamo che $\gamma$ è liberamente omotopo ad $\alpha \iff \hat \gamma$ è omotopo ad $\hat\alpha \iff \gamma \sim \beta * \alpha * \bar\beta$ ad estermi fissi per qualche cammino $\beta$ che congiunge $\gamma(0)$ ad $\alpha(0)$.

\textbf{Esempio.} In $\CC \setminus \{ 0 \}$ siano $\gamma(t) = e^{2\pi i t}, \alpha(t) = s \cdot e^{2\pi i t}$ per $t \in [0, 1]$, allora $\gamma$ ed $\alpha$ sono liberamente omotopi.

[TODO: Disegno]

\begin{fact}
    Se $\omega$ è una 1-forma chiusa su $D$ e $\gamma$ e $\alpha$ sono cammini chiusi in $D$ liberamente omotopi, allora

    $$
    \int_{\gamma} \omega = \int_{\alpha} \omega
    $$

    \begin{proof}
        Infatti, $\gamma$ è omotopo a estremi fissi a $\beta * \alpha * \bar\beta$ per cui $\ds \int_{\gamma} \omega = \int_{\beta * \alpha * \bar\beta} \omega = \int_{\beta} \omega + \int_{\alpha} \omega + \int_{\bar\beta} \omega = \int_{\alpha} \omega$
    \end{proof}
\end{fact}

\subsection{Forme chiuse di classe $C^1$}

Sia $D \subseteq \CC$ un aperto ed $\omega = P \dd x + Q \dd y$ una 1-forma su $D$, allora si dice $C^1$ se $P, Q : D \to \CC$ sono di classe $C^1$. In questo caso poniamo \textit{formalmente}

$$
\dd \omega = \lrpa{\frac{\pd Q}{\pd x} - \frac{\pd P}{\pd y}} \underbrace{\dd x \dd y}_{\mathrlap{\rotatebox[origin=c]{180}{$\Lsh$} \text{ simbolo formale}}}
$$

\textbf{Formula di Green, o Green-Riemann, Stokes.} 
Sia $\omega$ una 1-forma $C^1$ su $D, R \subseteq D$ rettangolo. Allora

$$
\int_{\pd R} \omega = \int_{R} \dd \omega
$$

\textit{Digressione.} Se $f : R \to \CC$ è continua, allora ha senso $\int_{R} f(x, y) \dd x \dd y \in \CC$ (dove $f(x, y) = f(x + iy)$). Inoltre se $R$ ha vertici $\{ a_1, a_2 \} \times \{ b_1, b_2 \}$ allora

$$
\int_{R} f(x, y) \dd x \dd y = \int_{[a_1, a_2] \times [b_1, b_2]} f(x, y) \dd x \dd y 
= \int_{a_1}^{a_2} \lrpa{\int_{b_1}^{b_2} f(x, y) \dd y} \dd x 
= \int_{b_1}^{b_2} \lrpa{\int_{a_1}^{a_2} f(x, y) \dd x} \dd y
$$ 

\begin{proof}
    Con le notazioni appena introdotte abbiamo

    $$
    \begin{aligned}
        \int_{R} \dd \omega 
        &= \int_{R} \lrpa{\frac{\pd Q}{\pd x} - \frac{\pd P}{\pd y}} \dd x \dd y \\
        &= \int_{R} \frac{\pd Q}{\pd y} \dd x \dd y - \int_{R} \frac{\pd P}{\pd y} \dd x \dd y \\
        &= \int_{b_1}^{b_2} \lrpa{\int_{a_1}^{a_2} \frac{\pd Q}{\pd x}(x, y) \dd x} \dd y 
        - \int_{a_1}^{a_2} \lrpa{\int_{b_1}^{b_2} \frac{\pd P}{\pd y} \dd y} \dd x \\
        &= \int_{b_1}^{b_2} (Q(a_2, y) - Q(a_1, y)) \dd y - \int_{a_1}^{a_2} (P(x, b_2) - P(x, b_1)) \dd x \\
        &= \int_{b_1}^{b_2} Q(a_2, y) \dd y - \int_{b_1}^{b_2} Q(a_1, y) \dd y - \int_{a_1}^{a_2} P(x, b_2) \dd x + \int_{a_1}^{a_2} P(x, b_1) \dd x \\
        &= \int_{\gamma_2} \omega - \int_{\bar\gamma_4} \omega - \int_{\bar\gamma_3} \omega + \int_{\gamma_1} \omega \\
        &= \int_{\gamma_1} \omega + \int_{\gamma_2} \omega + \int_{\gamma_3} \omega + \int_{\gamma_4} \omega \\
        &= \int_{\pd R} \omega 
    \end{aligned}
    $$
\end{proof}

\begin{theorem}
    Sia $\omega$ una 1-forma $C^1$ su $D$. Allora $\omega$ è chiusa $\iff \dd \omega = 0$

    \begin{proof}
        \begin{itemize}
            \itemRarr Se $\omega$ è chiusa, $\forall p \in D \; \exists U \subseteq D$ aperto, $P \in U$ con $\ds \omega = \dd F = \frac{\pd F}{\pd x} \dd x + \frac{\pd F}{\pd y} \dd y$ su $U$.

                $F$ è $C^2$ perché $\omega$ è $C^1$. Per il teorema di Schwartz $\ds\frac{\pd^2 F}{\pd x \pd y} = \frac{\pd^2 F}{\pd y \pd x}$ dunque

                $$
                \dd \omega = \lrpa{\frac{\pd}{\pd x}\lrpa{\frac{\pd F}{\pd y}} - \frac{\pd}{\pd y}\lrpa{\frac{\pd F}{\pd x}}} \dd x \dd y = 0
                $$

            \itemLarr Sia $R \subseteq D$ un rettangolo. Allora $\ds \int_{\pd R} \omega = \int_{R} \dd \omega = 0$ quindi $\omega$ è chiusa per un teorema precedente.
        \end{itemize}
    \end{proof}
\end{theorem}

In precedenza abbiamo visto che \textqt{analitica $\Rightarrow$ olomorfa}. Quanto segue è di interesse indipendente e serivrà per dimostrare l'implicazione opposta, cioè \textqt{olomorfa $\Rightarrow$ analitica} (in particolare, l'esistenza della derivata prima complessa implica l'esistenza delle derivate di qualsiasi ordine).

\begin{theorem}
    Sia $f : D \to \CC$ olomorfa. Allora $f(z) \dd z$ è una 1-forma chiusa su $D$.

    \begin{proof}
        La dimostrazione è (molto) più semplice se si assume $f$ di classe $C^1$, cioè non solo derivabile in senso complesso, ma anche con derivata continua. Infatti in tal caso $\omega = f(z) \dd z = f(z) \dd x + i f(z) \dd y$ è $C^1$ e 

        $$
        \dd \omega 
        = \lrpa{\frac{\pd i f}{\pd x} - \frac{\pd f}{\pd y}} \dd x \dd y 
        = \lrpa{i \frac{\pd f}{\pd x} - \frac{\pd f}{\pd y}} \dd x \dd y
        $$

        Poiché $f$ è olomorfa

        $$
        \begin{aligned}
            & \frac{\pd f}{\pd x} = \dd f(1) = f' \\
            & \frac{\pd f}{\pd y} = \dd f(i) = i \cdot \dd f' = i f' \\
        \end{aligned}
        $$

        dunque $\ds \dd \omega = i\frac{\pd f}{\pd x} - \frac{\pd f}{\pd y} = i f' - i f' = 0$.

        Dunque, se $\omega$ non è $C^1$ bisogna sfruttare un argomento diverso. Sia $f$ olomorfa, e supponiamo per assurdo $f(z) \dd z$ non sia chiusa. Allora $\exists$ rettangolo $R$ tale che $\ds \int_{\pd R} \omega = \alpha(R) \neq 0$ (d'ora in poi, se $\bar R$ è un qualsiasi rettangolo, poniamo $\ds \alpha(\bar R) = \int_{\pd \bar R} \omega$)

        $$
        \int_{\pd R} \omega = \int_{\pd A_1} \omega + \int_{\pd A_2} \omega + \int_{\pd A_3} \omega + \int_{\pd A_4} \omega 
        $$

        in quanto nel membro a destra i contributi relativi ai segmenti interni si elidono. Dunque $\alpha(R) = \alpha(A_1) + \alpha(A_2) + \alpha(A_3) + \alpha(A_4)$ ed esiste perciò un $A_i$ tale che $|\alpha(A_i)| \geq \dfrac{\alpha(R)}{4}$.

        Pongo $R_1$ uguale a questo punto $A_i$. Ora itero la costruzione applicando ad $R_1$, ottenendo $R_2 \subseteq \R_1$ con $|\alpha(R_2)| \geq \dfrac{\alpha(R_1)}{4} \geq \dfrac{|\alpha(R)|}{4^2}$. Iterando, otteniamo una successione di rettangoli inscatolati $R \subseteq R_1 \subseteq R_2 \subseteq \cdots \subseteq R_n \subseteq \cdots$ con $|\alpha(R_n)| \geq \dfrac{\alpha(R)}{4^n}$. Ora prendiamo $z_0 \in \bigcap_{n \in \N} R_n \neq \varnothing$ in qunato intersezione descrescente di compatti chiusi non vuoti.

        [TODO: Disegno]

        Poiché $f$ è olomorfa, abbiamo $f(z) = f(z_0) + f'(z_0) (z - z_0) + \varepsilon(z) |z - z_0|$, dove $\lim_{z \to z_0} |\varepsilon(z)| = 0$, per cui

        $$
        \begin{aligned}
            \alpha(R_n) 
            &= \int_{\pd R_n} (f(z_0) + f'(z_0)(z - z_0) + \varepsilon(z) |z - z_0|) \dd z \\
            &= \underbrace{\int_{\pd R_n} f(z_0) \dd z}_{=0} + \underbrace{\int_{\pd R_n} f'(z_0) (z - z_0) \dd z}_{=0} + \int_{\pd R_n} \varepsilon(z) \cdot |z - z_0| \dd z
        \end{aligned}
        $$

        (in quanto $\dd z, (z - z_0) \dd z$ sono chiuse, sono $C^1$ ed hanno differenziale nullo, come mostra un semplice calcolo; oppure ancora più facilmente, ammettono rispettivamente primitive $z$ e $\frac{(z - z_0)^2}{2}$, dunque sono esatte)

        Perciò $\ds \alpha(R_n) = \int_{\pd R_n} \varepsilon(z) \cdot |z - z_0| \dd z$.

        Ora, se $n$ è sufficientemente alto, $R_n \subseteq B(z_0, \delta)$, dove $\delta$ può essere scelto piccolo a piacere. Ciò è ovvio perché $z_0 \in R_n$ e, se $a, b$ sono le lunghezze dei lati di $R$, $\opn{diam}(R_n) \leq \dfrac{a + b}{2^n}$.

        In particolare possiamo scegliere $n$ in modo che $\forall z \in R_n \quad |\varepsilon(z)| \leq \dfrac{|\alpha(R)|}{4(a + b)^2}$ (basta scegliere $\delta > 0$ tale che $\forall z \in B(z_0, \delta) \quad |\varepsilon(z)| \leq \dfrac{|\alpha(R)|}{4(a + b)^2}$, cosa possibile perché $\lim_{z \to z_0} \varepsilon(z) = 0$, e scegliere $n$ di conseguenza).

        Per tale $n$, se $z \in \pd R_n, |z - z_0| \leq \dfrac{a + b}{2^n} = \opn{diam}(R_n)$ per cui

        $$
        \begin{aligned}
            |\alpha(R_n)| 
            &= \left| \int_{\pd R_n} \varepsilon(z) \cdot |z - z_0| \right| 
            \leq \int_{0}^{\overbrace{\scriptstyle\frac{2(a + b)}{2^n}}^{
                \mathrlap{\textstyle\Rsh\raisebox{4pt}{$\scriptstyle\opn{Perimetro}(R_n)$}}
            }} |\varepsilon(\gamma(t))| \cdot |\gamma(t) - z_0| \dd t \\
            &\leq \frac{2(a + b)}{2^n} \cdot \frac{|\alpha(R)|}{4(a + b)^2} \cdot \frac{(a + b)}{2^n} = \frac{1}{2} \cdot \frac{|\alpha(R)|}{4^n}
        \end{aligned}
        $$

        il che contraddice $|\alpha(R_n)| \geq \dfrac{|\alpha(R)|}{4^n}$
    \end{proof}
\end{theorem}

\begin{corollary}
    $f : D \to \CC$ olomorfa $\implies \forall p \in D \; \exists$ aperto $U \subseteq D, p \in U$ e $F : U \to \CC$ olomorfa con $F' = f{|}_U$

    \begin{proof}
        $f(z) \dd z$ è chiusa $\implies \exists U$ come nell'enunciato, $F : U \to \CC$ con $\dd F = f(z) \dd z$ su $U$, cioè $F$ olomorfa e $F' = f$ su $U$.
    \end{proof}
\end{corollary}

\begin{corollary}
    $f : D \to \CC$ olomorfa $\implies \forall \gamma$ laccio in $D$ omotopicamente banle, $\ds \int_{\gamma} f(z) \dd z = 0$
\end{corollary}

\begin{prop}
    $f : D \to \CC$, $D$ aperto e sia $f$ continua su tutto $D$ e olomorfa su $D \setminus r$ con $r$ retta orizzontale, allora $f(z) \dd z$ è chiusa.

    \begin{proof}
        Sia $R \subseteq D$ rettangolo, voglio $\ds \int_{\pd R} \omega = 0$. Se $R \cap r = \varnothing$, segue da quanto già visto ($R \subseteq D \setminus r$, e $f$ è olomorfa su $D \setminus r$).

        Supponiamo che un lato orizzontale di $R$ giaccia su $r$. Allora possiamo costruire una successione di rettangoli $R_n$ come in figura che \textqt{tenda} a $R$. 

        [TODO: Disegno]

        Allora usando che $f$ è continua si vede facilmente che $\ds \int_{\pd R} f(z) \dd z = \lim \int_{\pd R_n} f(z) \dd z = 0$ in quanto $\forall n \; \int_{\pd R_n} f(z) \dd z = 0$.

        Infine se $R \cap r \neq \varnothing$ ma $r$ non contiene lati di $R$, si conclude così

        $$
        \int_{\pd R} f(z) \dd z = \int_{\pd R_1} f(z) \dd z + \int_{\pd R_2} f(z) \dd z = 0 + 0 = 0
        $$

        per quanto visto nel caso precedente.

        \textit{Vediamo meglio il primo caso.} Se $R$ ha vertici $\{ a_1, a_2 \} \times \{ b_1, b_2 \}$ e $R \cap r$ ha ordinata $b_1$ con $b_1 < b_2$ allora $R_n$ ha vertici

        $$
        \lrpa{a_1, b_1 + \frac{1}{n}} 
        , \qquad
        \lrpa{a_2, b_1 + \frac{1}{n}} 
        , \qquad
        \lrpa{a_1, b_2} 
        , \qquad
        \lrpa{a_2, b_2} 
        $$

        Gli integrali lungo i segmenti orizzontali di sopra sono identici, mentre gli altri hanno differenze che tendono a $0$ per $n \to +\infty$ (usando che $f$ è continua).
    \end{proof}
\end{prop}


\end{document}
