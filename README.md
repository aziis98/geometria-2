
# Geometria 2

Raccolta di appunti di Geometria 2 tratte dalle lezioni del corso dell'anno 2019/2020 del prof. Frigerio. Per ora che è in corso c'è una cartella con gli estratti delle varie lezioni.

## Organizzazione

- `main.tex`

	Contiene tutto il testo del libro, per ora l'approccio monolitico mi sta sembrando il migliore

- `prelude.tex`

	Import dei vari pacchetti e configurazione

- `main.pdf` / `geometria-2.pdf `

	PDF principale, il secondo è solo una copia della versione "stabile" più recente.

- `extract-lesson.sh <nome_senza_estensione>` e `lezioni/`

	Script magico che usa `git diff` per estrarre i cambiamenti rispetto all'ultimo commit e estrarre l'ultima lezione tex-ata.

- `.latexmkrc`

	Configurazione di `latexmk` 

- `altro/`

	Backup di cose vecchie, da pulire

- `images/` e `asymptote/`

	Cartelle con i disegnini autoestratti dal LaTeX da `latexmk` o fatti con Inkscape.

## Branches

- `notazoine-sollevamenti`: Branch sperimentale per provare la nuova notazione per i sollevamenti senza mille mila pedici. Della serie "curva [freccia-su] punto"

## Note

- Comando per compilare `latexmk -pdf -halt-on-error`

- Per i disegnini è consigliato usare `0.75pt` per lo stroke-width delle linee.















