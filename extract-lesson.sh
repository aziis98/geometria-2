#!/bin/bash

if [ "$#" -eq "0" ] ; then
	echo "Passa come primo parametro il nome del file in cui mettere la lezione!"
	exit 1
fi

FILE_NAME=$1

cat "./lezioni/template.tex" > "./lezioni/$FILE_NAME.tex"

# git diff -U0 main.tex                 --- Genera un diff del main rispetto all'ultimo commit
# tail +5                               --- Taglia il frammento iniziale di info generato da git diff
# sed -E 's/^\+//;s/^@@.+$//;s/^-.+$//' --- Cancella i '+' all'inizio di ogni riga e le righe con '-' o '@@' all'inizio 
# >> "./lezioni/$FILE_NAME.tex"         --- Infine appende tutto nel giusto file in cui è già presente l'header.
git diff -U0 main.tex | tail +5 | sed -E 's/^\+//;s/^@@.+$//;s/^-.+$//' >> "./lezioni/$FILE_NAME.tex"

# Appende il footer al file
echo -e "\n\\\end{document}" >> "./lezioni/$FILE_NAME.tex"

echo "Estratta l'ultima lezione dai local changes di git in './lezioni/$FILE_NAME.tex'"
echo "Compilo il pdf..."

# Fa partire nella cartella primaria la renderizzazione del latex nel terminale usare questo comando come base
echo latexmk -pdf -halt-on-error -outdir=lezioni \"lezioni/$FILE_NAME.tex\"

echo "Fatto!"