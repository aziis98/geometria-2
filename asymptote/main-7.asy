if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-7";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

//
// settings.render = 2;

import three;
import plain_pens;

currentprojection = orthographic((2, 3.5, 1), up=X);

unitsize(2.5cm);

draw(-1.5X -- 1.5X, arrow=Arrow3(TeXHead2), L=Label("$x$", position=EndPoint, align=W));
draw(-2Y -- 2Y, arrow=Arrow3(TeXHead2), L=Label("$y$", position=EndPoint));
draw(-2Z -- 3Z, arrow=Arrow3(TeXHead2), L=Label("$z$", position=EndPoint));

draw(unitsphere, surfacepen=white+opacity(0.5));
draw(circle(c=(0,0,0), r=1, normal=X), dashed);

draw(shift(0.5Z) * scale(3, 3, 4) * rotate(90, -Y, Y) * shift(-0.5, -0.5, 0) * unitplane, surfacepen=white+opacity(0.5));
label("$\{ x_0 = 0 \}$", (0.25,1.5,1.5), interaction(1, true));

draw((1.5, 0, -1) -- X -- (3/5, 0, 4/5) -- (0, 0, 2) -- (-0.5, 0, 3));
dot(X -- (3/5, 0, 4/5) -- (0, 0, 2), red);
