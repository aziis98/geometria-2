if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-1";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

import three;
import plain_pens;

settings.render = 2;

size(16cm, 5cm);

currentprojection = perspective((0,-10,2), up = Z);

surface ciotola = surface((0,0,0),
(0,0,-0.1) .. (0.25,0,-0.1) .. (1.0,0,0.3),
Z
);

surface vaso = surface((0,0,0),
(0,0,-0.5) .. (0.75,0,-0.5) .. (0.75,0,0.5) .. (0.5, 0, 0.75),
Z
);

draw(
shift(-6X) * unitdisk,
white
);

draw(shift(-6X) * circle((0,0,0), 1, Z), p = magenta + 2);

draw(
-4.5X+0.3Z .. -4X+0.4Z .. -3.5X+0.3Z,
arrow=Arrow3(TeXHead2)
);

draw(
shift(-2X) * ciotola,
white
);

draw(shift(-2X) * circle(0.3Z, 1, Z), p = magenta + 2);

draw(
-0.5X+0.3Z .. 0.4Z .. 0.5X+0.3Z,
arrow=Arrow3(TeXHead2)
);

draw(
shift(2X) * vaso,
white
);

draw(shift(2X) * circle(0.75Z, 0.5, Z), p = magenta + 2);

draw(
+3.5X+0.3Z .. +4X+0.4Z .. +4.5X+0.3Z,
arrow=Arrow3(TeXHead2)
);

draw(shift(6X) * unitsphere,
surfacepen = white
);

dot(6X+Z, magenta + 3);
