if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-2";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(1cm);

usepackage("amssymb");
usepackage("amsmath");

// draw(box((-5,2.5), (+5,-2)), black);

// Left

label("$\mathbb{R}^2$", (-2.5, 1.75), N);
label("$A$", (-1.5, 0), N, magenta);

draw((-4,0) -- (-1, 0), magenta);
draw(box((-4,1.5), (-1,-1.5)));

// Right

label("$\raisebox{+0.3ex}{\scalebox{0.9}{$\mathbb{R}^2$}} / \raisebox{-0.3ex}{\scalebox{0.9}{$A$}}$", (2.5, 1.75), N);

draw((1,1.5) -- (4, 1.5));
draw((1,-1.5) -- (4, -1.5));
draw(
(1, 1.5) .. controls (1.25, 1) and (2, 0.25) .. (2.5, 0) &
(2.5, 0) .. controls (3, -0.25) and (3.75, -1) .. (4, -1.5)
);
draw(
(4, 1.5) .. controls (3.75, 1) and (3, 0.25) .. (2.5, 0) &
(2.5, 0) .. controls (2, -0.25) and (1.25, -1) .. (1, -1.5)
);

dot((2.5, 0), magenta);
label("$[A]$", (2.8, 0), E, magenta);

draw((-0.25, 0) -- (+0.5, 0), arrow=Arrow(TeXHead));
