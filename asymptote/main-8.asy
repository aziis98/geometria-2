if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-8";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(0.75cm); //

srand(3);

pair randpoint() {
return ((unitrand() - 0.5) * 2, (unitrand() - 0.5) * 2);
}

pair[] pts = sequence(new pair(int i) {
pair pt = randpoint() * 4;

dot(pt);
label("$P_" + string(i + 1) + "$", pt, NE);

return pt;
}, 5);

void drawKn(real m) {
path[] compact = circle((0, 0), m);

for (pair pt : pts) {
compact = compact ^^ reverse(circle(pt, 3 / m));
}

picture p1 = new picture;
fill(p1, compact, hsv(270, 0.6 - m / 20.0, 1.0));
clip(p1, circle((0,0), m - 0.01));

add(p1);
}

for (real i = 6; i >= 2; i -= 1) {
drawKn(i);
}

for (real i = 2; i <= 6; i += 1) {
label(
"$K_" + string(i - 1) + "$",
(-i + 0.5,-i + 0.5) / sqrt(2)
);
}

label("\dots", (-6.5,-6.5) / sqrt(2));
