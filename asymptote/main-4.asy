if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-4";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(1cm);

pair pt_x = (-1, 0);
pair pt_y = (0, -1);
pair pt_z = (1, 1);

draw(pt_x .. (-0.75,-0.45) .. (-0.25, -0.65) .. pt_y, L="$\alpha$");
draw(pt_y .. (0.5, -0.75) .. (0.5, 0.5) .. pt_z, L="$\beta$");

dot(pt_x, L="$x$", align=W);
dot(pt_y, L="$y$", align=S);
dot(pt_z, L="$z$", align=E);
