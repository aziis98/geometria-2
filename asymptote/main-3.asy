if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-3";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(1cm);

path pathfrompoints(pair[] points) {
path p;

for (pair pt : points) {
p = p{right}..{right}pt;
}

return p;
}

path pathfrompoints_straight(pair[] points) {
path p;

for (pair pt : points) {
p = p -- pt;
}

return p;
}

void draw_intorno(pair[] punti, pen p) {
path intorno = pathfrompoints(punti);
draw(intorno, p);
// draw(intorno, p+opacity(0.5));
// fill(intorno -- (12, 0) -- (0, 0) -- cycle, p+opacity(0.125));
}

pair[] punti_intorno_n = sequence(new pair(int i) { return (i, unitrand()*2.5 + 0.5); }, 13);
pair[] punti_intorno_m = sequence(new pair(int i) { return (i, unitrand()*2 + 0.4); }, 13);

// Intorni

draw_intorno(punti_intorno_n, blue);
label("$U_n$", (7,3), p=blue);

draw_intorno(punti_intorno_m, orange);
label("$U_m$", (5.5,2), p=orange);

// Sequenza di Epsion

real epsilon_n = min(map(new real(pair pt) { return pt.y; }, punti_intorno_n)) - 0.05;
real epsilon_m = min(map(new real(pair pt) { return pt.y; }, punti_intorno_m)) - 0.1;

draw((0, epsilon_n) -- (12, epsilon_n), blue+dotted);
draw((6, 0) -- (6, epsilon_n), blue+dotted);
label("$\varepsilon_n$", (0, epsilon_n), W, blue);

draw((0, epsilon_m) -- (12, epsilon_m), orange+dotted);
draw((9, 0) -- (9, epsilon_m), orange+dotted);
label("$\varepsilon_m$", (0, epsilon_m), W, orange);

// f - Funzione Diagonale

pair[] punti_f = sequence(new pair(int i) { return (i, unitrand()*1.5); }, 13);

punti_f[6] = (6, epsilon_n / 2);
punti_f[9] = (9, epsilon_m / 2);

path p_f = pathfrompoints_straight(punti_f);
draw(p_f, darkgreen);
// fill(p_f -- (12, 0) -- (0, 0) -- cycle, darkgreen+opacity(0.125));

label("$f$", (11.25, 1.3), E, darkgreen);

// Assi

draw((6,0.1) -- (6,-0.1));
label("$n$", (6,-0.1), S);

draw((9,0.1) -- (9,-0.1));
label("$m$", (9,-0.1), S);

draw((-.25,0) -- (12.5,0), arrow=Arrow(TeXHead));
draw((0,-.25) -- (0,4), arrow=Arrow(TeXHead));
