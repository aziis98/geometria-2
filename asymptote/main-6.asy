if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-6";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(0.5cm);

draw((-5,1) -- (5,-1));
draw((-1,-5) -- (1,5), Dotted);

dot((-5, 1));
dot((5, -1));

label("$x$", (-5,1), NW);
label("$y$", (5,-1), NE);
label("$r$", (0,-5));

srand(2);

for (int i : sequence(20)) {
dot((unitrand()*8-4, unitrand()*8-4), gray);
}

draw((-5,1) -- (0.5,2.5) -- (5,-1));

