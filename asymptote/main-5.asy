if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="main-5";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

unitsize(1cm);

for (real x : uniform(-30/9, 30/9, 6*10)) {
draw((x,-2 + unitrand() * 0.5 - 0.25) -- (x,2 + unitrand() * 0.5 - 0.25), gray);
}

for (real x : uniform(-3, 3, 6*3)) {
draw((x,-2 + unitrand() * 0.3 - 0.15) -- (x,2 + unitrand() * 0.3 - 0.15), black);
}

for (real x : uniform(-3, 3, 6)) {
draw((x,-2 + unitrand() * 0.2 - 0.1) -- (x,2 + unitrand() * 0.2 - 0.1), black+1);
}

draw(shift(-16 / 9, 1) * scale(0.3) * unitcircle, blue);
dot((-16 / 9, 1), blue);

draw((-3.5,0) -- (+3.5,0), black+1);

label("$X$", (3, 2), NE);
