
@default_files = ('main.tex');

# sub asy2 {
#   my $fn = shift;
#   my ($basename, $dirname) = fileparse($fn);
#   return system("asy -cd '$dirname' '$basename'");
# }

# sub asy {
#   return system("asy -o asymptote/ '$_'");
# }

# add_cus_dep("asy", "eps", 0, "asy");
# add_cus_dep("asy", "pdf", 0, "asy");
# add_cus_dep("asy", "tex", 0, "asy");

sub asy {return system("asy -f pdf -o asymptote/ '$_[0].asy'");}
add_cus_dep("asy","eps",0,"asy");
add_cus_dep("asy","pdf",0,"asy");
add_cus_dep("asy","tex",0,"asy");